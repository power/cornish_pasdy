# Road classes and its properties
#     category based on OSM definitions, see https://wiki.openstreetmap.org/wiki/DE:Key:highway
# Road properties as defined in Schrön et al. 2018, WRR <https://doi.org/10.1029/2017WR021719>
#     moisture is the water equivalent (g/g) of the road material
#     width is the total road width (all lanes, both directions) in meter
category		moisture	width
residential		0.10		3
track			0.12		2
service			0.07		3
motorway		0.03		10
trunk			0.03		8
primary			0.04		6
secondary		0.07		5
tertiary		0.06		5
unclassified	0.07		3
road			0.07		3
motorway_link	0.03		5
trunk_link		0.03		5
primary_link	0.04		3
secondary_link	0.06		3
tertiary_link	0.06		3