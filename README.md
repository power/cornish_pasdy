# CoRNy -- A Cosmic-Ray Neutron processing toolbox for python

![CoRNy](docs/corny-logo.png)

Corny aims at making CRNS **data analysis accessible for everyone**! CRNS stands for Cosmic Ray Neutron Sensing, a method used to measure snow and soil water content of the environment with neutrons from the natural cosmic background radiation.

This project has been initially developed by Martin Schrön, Erik Nixdorf, David Schäfer, Jannis Jakobi, Carmen Zengerle, Rafael Rosolem, and Steffen Zacharias. It is, however, open for contributions from an active user community! 

## Idea

 Corny wants to be understood as a *pre-processor*, i.e., it **reads original measurement files**, performs basic quality control, correction functions, and suggests a first-order soil water conversion. The generated PDF and KML output helps to have a **first glimpse on the data**, to identify potential issues or interesting features. However, Corny does *not* perform the full publication-ready analysis for you! **The output CSV is harmonized**, i.e., it wants to be imported into your individual analysis scripts -- may it be in Python, R, MatLab,  Excel, or whatever. This allows you to **do rocket science on the basis of soundly pre-processed data**.

 *Note:* CoRNy is the renamed version of the older *CORNish PASDy* -- COsmic-Ray Neutron flavored Processing and Analysis of Sensor Data in pYthon -- because we love cereals, and every corn contributed by the user community adds to universality and flavour. Full generalization of this toolbox also for non-cosmic-ray data is one of the many overachieving future milestones. Then, it may be renamed to PASDy again.
 
 *Important note:* This repository will be renamed to "corny" soon. Be warned to adapt your local copy.
 
## Contribution

- Did you find a bug or are you missing a feature? Please report your experience in the [project issues](https://git.ufz.de/CRNS/cornish_pasdy/issues).
- Have you resolved errors or added new features? Please push your contributions back to GitLab such that the whole community can benefit from your work. Contact [Martin](mailto:martin.schroen@ufz.de) to receive write permission.

## Usage

The CoRNy package aims at allowing various ways of usage depending on the level of detail and interaction required by individual users. It can be used in the following ways:

- **Mouse click** on the file `instantPASDy.py` processes the sensor data instantly. It requires a file `config.cfg` to be present in the same folder. Use this option if you have only one type of sensor for which the exact same analysis needs to be performed over and over again.
- In the **console**, anywhere on your disk, type

        python path/to/cornish_pasdy/instantPASDy.py configfile.cfg

    This way you can Corny on specific config files. Use this option if you have different config files in your research folder, and you don't like GUIs or you apply batch scripting. 
- **Execute** `cornyApp.py` to open the GUI (graphical user interface). Here, any config file can be selected and ammended directly through the interface. It allows to run Corny, log the output, and interactively look at plots and maps.
- **Shortcut double click** on `Corny.bat`. Copy this file from `app/Corny.bat` to your research folder, edit it with an editor and adapt the paths. Then just double-click on the file to directly open the GUI. 
- **Import Corny** from within your own scripts (e.g., using Jupyter notebooks) to directly access specific functions, e.g.:

        # Add the Corny folder to the recognizable paths
        import sys
        sys.path.insert(0, '/path/to/cornish_pasdy')
        # Import
        from corny import *


## Demo

Demonstration for a CRNS rover dataset (works similar for CRNS station data):
![Corny demonstration](docs/corny_demonstration.gif)

## Install

Here is a suggestion of how Corny could be installed. It has not been tested *everywhere*, so please contribute to this list if it also works (or doesn't) on your system :-)

### 0. TL;DR: Quick setup for basic users

If you only want to use Corny on Windows, don't intend to change its code, and don't mind the version numbers of python packages on your system, use this quick & dirty setup.

1. Install Python 3.10.*  
    - e.g., from [Python.org](https://www.python.org/downloads/) or [Miniconda](https://docs.conda.io/projects/conda/en/stable/user-guide/install/download.html).
    - Make sure to "add Python to PATH".
2. Download Corny
    - either as a [zip file](https://git.ufz.de/CRNS/cornish_pasdy/-/archive/master/cornish_pasdy-master.zip) and unpack it,
    - or using [Git](https://git-scm.com/downloads) to clone the repository.
3. Go to folder `install/` and double-click on... 
    - `Install.bat` if you use Python.org distributions, or
    - `Install-conda.bat` if you use Conda distributions.

After installation, the install script will also create a folder `example_work_folder`. Copy it to your favorite place where you usually conduct data analysis. The folder contains example data, configurations, and shortcuts to easify your daily work. For example, double-click on `Corny.bat` to quickly process your data in the console, or on `Corny-GUI.bat` to use the graphical user interface. There, press "Load", and when it is finished, click "Save & Run". 

Skip all other instructions below this item.

### 1. Download Corny

You can download the [latest version as a zip file](https://git.ufz.de/CRNS/cornish_pasdy/-/archive/master/cornish_pasdy-master.zip). Extract the archive and Corny is ready to go.

Alternatively, you can use *Git* (optional, but recommended). Git is a handy version control system, which would allow you to easily clone and update Corny with a single command (`git pull`). You can get Git from [git-scm.com](https://git-scm.com/downloads). After installation of Git, use the command line to run:

    git clone https://git.ufz.de/CRNS/cornish_pasdy.git

### 2. Install Python

First, get Python, e.g., from here: [Python.org](https://www.python.org/downloads/).
It is important that your system knows where Python has been installed. So on the python install dialogue, check "Add Python to PATH" and "Disable path length limit". Or manually edit your system-specific environmental variables.

Successfully tested on: 

- Windows
    - [Python.org](https://www.python.org/downloads/) distribution, versions `3.9`, `3.10`.
    - [Anaconda/Miniconda](https://docs.conda.io/projects/conda/en/stable/user-guide/install/download.html) distributions, version `3.10`. Specific packages need to be installed with `conda`, others with `pip` (see below).

#### 2.1 Use Virtual Environments (optional)

The concept of virtual environments protects your other python projects from interfering package versions and dependency clashes. **If you are a python developer** and want to keep your packages safe from Corny dependencies, use this option.

To create a new environment, execute in any folder:

    python -m venv env

Now you can install packages and mess around without disturbing your other python projects. To start working, activate the session with:

    source env/Scripts/activate

If the execution fails on Windows, it's probably a line-ending error. Edit the file `env/Scripts/activate` and change line endings to `LF` (Unix).
To deactivate the session, just type `deactivate`.

### 3. Package installation

#### 3.1 Linux

1. You have two options:

    - Install latest versions of the required packages.

            cd install
            pip install -r python_packages/requirements-linux.txt

    - Find missing packages that you need to install or update. For this, once install the `pipreq` package and execute:

            pipreqs ./

        The list of required packages is saved to `requirements.txt`.

            pip install -r requirements.txt

        Please remove the file `requirements.txt` (but not the one in `env/`) after you are done, as it is user-specific and should not be part of the repo.

2. For GUI usage, install kivy garden plot routines:

        garden install matplotlib


#### 3.2 Windows

For **Conda** distributions:

    cd install
    conda  install --file python_packages/requirements-conda.txt
    pip    install  -r    python_packages/requirements-conda-pip.txt
    garden install matplotlib

For **Python.org** distributions: 

Windows has problems with the automatic installation of GDAL and other packages. That's why we split the installation into two parts:

1. Install precompild wheel files by choosing one of the two options:

    - Download pre-compiled packages ("wheels") of `GDAL`, `pyproj`, `Fiona`, `Shapely`, `rasterio`, and `Cartopy` directly from <https://www.lfd.uci.edu/~gohlke/pythonlibs/>. Use `Strg+F` to search for the packages and look for the *newest* versions which fit to your python installation (e.g., `cp310` is for Python version 3.10) and your Windows system (32bit or 64bit). Install them in the suggested order one by one:
        
            pip install filename.whl
        
    - Install the copies provided by Corny:
            
            cd install
            pip install -r python_packages/requirements-windows-1.txt
        
2. Install the remaining "less problematic" packages using:

        cd install
        pip install -r python_packages/requirements-windows-2.txt

3. The GUI needs further installations of the kivy garden package. By now, your console should know about the command `garden`, if not, restart the console or go directly to its containing folder (e.g., `path/to/Python39/Scripts/garden`). Then execute:

        garden install matplotlib
 

Test the installation by running `python cornyApp.py`. Let us know whether you come over any issues.

### 3.3 Jupyter Lab (optional)

[Jupyter notebooks](https://jupyter.org/) are a great way to play with python, here is a short instruction of installation:

    pip install jupyterlab

Run Jupyter Lab with the command `jupyter lab`.

An alternative is using the [Hydrogen package](https://atom.io/packages/hydrogen) inside [Atom Editor](https://atom.io/). It emulates a similar interactive notebook-like experience, with the advantage of clean python code that can be more easily shared and version-controlled.


## Examples

Example station data:

    python instantPASDy.py examples/station.cfg

Example rover data:

    python instantPASDy.py examples/rover.cfg
    python instantPASDy.py examples/rover-styx.cfg
