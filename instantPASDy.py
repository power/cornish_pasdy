#!/usr/bin/env python

import os
from sys import exit
from pdb import set_trace as debug
from datetime import date, datetime
from corny import *
from corny.exdata import *
from corny.exdata.raster import luse_code2str, get_from_raster
import corny.exdata as exdata
from corny.exdata import footprint
import corny.io
from corny.io import app_file_path #as file_save_msg_q

from corny.basics import Progress, Progressbar, file_save_msg #clean_interval_str
from corny.figures import Fig, add_basemap, add_scalebar, add_colorbar, add_circle, add_latlon_ticks

# Get the current version of Corny
from corny.version import __version__

from corny.reports import Journalist

_orig_print = print
def print(*args, **kwargs):
    text = " ".join(map(str, args))
    color_start = ''
    if len(text)>0:
        os.system("")
        if text[0:2] == '> ': color_start = '\033[33m'
        if text[0:2] == '< ': color_start = '\033[32m'
        if text[0:2] == '# ': color_start = '\033[35m'
        if text[0:2] == 'i ': color_start = '\033[36m'
        if text[0:2] == '! ': color_start = '\033[31m'
        #if text[0] == '[': font_name = script_path + 'app/_ui/fonts/consolab.ttf'
        if re.match('^\s*\w+Error:', text):
            color_start = '\033[31m'
    if color_start:
        text = color_start + text + '\033[0m'
    if not 'flush' in kwargs:
        _orig_print(text, flush=True, **kwargs)
    else:
        _orig_print(text, **kwargs)
    
def main(configfile=None, cfg_prefix=None):
    print("Welcome to instant-PASDy %s!" % __version__)

    corny.io.print = print
    corny.basics.print = print
    corny.corn.print = print
    # Find script path
    pasdy_path = os.path.dirname(os.path.abspath(__file__))

    # Report stories while proceeding.
    # Later post in PDF reports.
    J = Journalist()

    # Read config
    if configfile is None: configfile = 'config.cfg'
    configfile_abspath = os.path.abspath(configfile)
    config = read_config(configfile, chdir=True)
    if not config:
        exit()

    print('[Update]')
    # Update data (FTP, SD, ...)
    if 'update' in config:
        for source in csplit(config['update']['data_source']):
            print('| Updating data from ' + source + ' into ' + config['input']['path'])
            download(
                archive_file = config['input']['path'],        source     = source,
                server       = config['update']['FTP_server'], user       = config['update']['FTP_user'],
                pswd         = config['update']['FTP_pswd'],   ftp_prefix = config['update']['FTP_prefix'],
                ftp_suffix   = config['update']['FTP_suffix'], sd_path    = config['update']['SD_path'],
                sd_prefix    = config['update']['SD_prefix'],  sd_suffix  = config['update']['SD_suffix'],
                update_all = yesno2bool(gc(config,'update','update_all','no')))

    coordinates = xsplit(gc(config,'info','coords'), range=3, type=float)

    ####################################################
    ######  Input  #####################################
    #################################################### TODO: outsource this
    print('[Input]')

    
    if yesno2bool(gc(config, 'input', 'restore_from_pickle')):

        pickle_file = gc(config, 'input', 'store_pickle')
        print('> Restoring from pickle: %s' % pickle_file)

        import compress_pickle as pickle
        with open(pickle_file, 'rb') as f:
            data = pickle.load(f)
    else:
        delimiter = ''
        if config['input']['path'].endswith('.zip'):
            delimiter = '/'
        if cfg_prefix is None:
            cfg_prefix = config['input']['prefix']

        pattern = cfg_prefix+'*'+config['input']['suffix']
        print('> Reading data from ' + config['input']['path'] + delimiter + pattern)

        archive = None
        files = []

        if config['input']['path'].endswith('.zip'):
            archive = zipfile.ZipFile(config['input']['path'], 'r')
            files = [x for x in archive.namelist() if x.endswith(config['input']['suffix']) and x.startswith(cfg_prefix)]

        elif os.path.isdir(config['input']['path']):
            files = glob1(config['input']['path'], pattern)
        else:
            if 'gui' in globals():
                gui.Popup("Input path is neither a folder nor a zip archive.", title='Read ERROR', non_blocking=True)
            else:
                print("! ERROR: Input path is neither a folder nor a zip archive.")
            return(None)

        if len(files) == 0:
            print('! No matching files found.')
            return()

        datastr = ''
        data_columns = []
        if cc(config,'input','input_columns'):
            if config['input']['input_columns'] != '':
                data_columns = csplit(config['input']['input_columns'])

        i = 0
        last_progress = 0 # in %

        for filename in files:
    #        print('.', end='')
            #print(filename)
            #add path again
            #filename=os.path.join(os.path.normpath(config['input']['path']),filename)
            progressbar(i-1, len(files), title='Reading', prefix=filename)
            i += 1
            progress = (i+0.01)/len(files)//0.1*10 # in %, offset 0.01 because x/x//0.1 is 9
            if progress > last_progress:
                #print("%.0f%%.." % progress, end='')
                last_progress = progress

            if datastr=='':
                if len(data_columns)<=0:
                    data_columns = read_header(filename, archive, path=config['input']['path'])
                datastr = read(filename, archive, path=config['input']['path'], skip=gc(config,'input','skip_lines',0,dtype=int))
            else:
                datastr += read(filename, archive, path=config['input']['path'], skip=gc(config,'input','skip_lines',0,dtype=int))

        progressbar(i-1, len(files), title='Reading', prefix=filename, end=True)
        if not archive is None: archive.close()
        #DEBUG output raw data file
        #with open('temp.txt','a') as f:
        #    f.write(datastr)

        index_col_num = 1
        if cc(config,'input','index_column_number'):
            ia = xsplit(config['input']['index_column_number'], type=int)
            if len(ia) == 1:
                index_col_num = ia[0]
            else:
                index_col_num = list(ia)
        
        data = make_pandas(datastr, data_columns, index=index_col_num, 
                sep=gc(config,'input','sep',alt=','), decimal=gc(config,'input','decimal',alt='.'),
                timestamp=yesno2bool(gc(config,'input','timestamp','')))
        #data.attrs = config

        if not check_data(data): exit()

        #print(data[['N1Cts','N2Cts']])

        ####################################################
        # GPS

        if cc(config,'input','separate_gps','yes'):

            delimiter = ''
            if config['input']['path'].endswith('.zip'):
                delimiter = '/'
            pattern = config['input']['gps_prefix']+'*'+config['input']['gps_suffix']
            print('> Reading separate GPS data from ' + config['input']['path'] + delimiter + pattern)

            archive = None
            files = []

            if config['input']['path'].endswith('.zip'):
                archive = zipfile.ZipFile(config['input']['path'], 'r')
                files = [x for x in archive.namelist() if x.endswith(config['input']['gps_suffix']) and x.startswith(config['input']['gps_prefix'])]

            elif os.path.isdir(config['input']['path']):
                files = glob1(config['input']['path'], pattern)
            else:
                if 'gui' in globals():
                    gui.Popup("Input path is neither a folder nor a zip archive.", title='Read ERROR', non_blocking=True)
                else:
                    print("! ERROR: Input path is neither a folder nor a zip archive.")
                return(None)

            if len(files) == 0:
                print('! No matching files found.')
                return()

            gpsdatastr = ''
            gpsdata_columns = []
            if cc(config,'input','gps_input_columns'):
                if config['input']['gps_input_columns'] != '':
                    gpsdata_columns = csplit(config['input']['gps_input_columns'])

            i = 0
            last_progress = 0 # in %

            for filename in files:
        #        print('.', end='')
                #print(filename)
                #add path again
                #filename=os.path.join(os.path.normpath(config['input']['path']),filename)
                progressbar(i-1, len(files), title='Reading', prefix=filename)
                i += 1
                progress = (i+0.01)/len(files)//0.1*10 # in %, offset 0.01 because x/x//0.1 is 9
                if progress > last_progress:
                    #print("%.0f%%.." % progress, end='')
                    last_progress = progress

                if gpsdatastr=='':
                    if len(gpsdata_columns)<=0:
                        gpsdata_columns = read_header(filename, archive, path=config['input']['path'])
                    gpsdatastr = read(filename, archive, path=config['input']['path'], skip=gc(config,'input','gps_skip_lines',0,dtype=int))
                else:
                    gpsdatastr += read(filename, archive, path=config['input']['path'], skip=gc(config,'input','gps_skip_lines',0,dtype=int))

            progressbar(i-1, len(files), title='Reading', prefix=filename, end=True)
            if not archive is None: archive.close()
            #DEBUG output raw data file
            #with open('temp.txt','a') as f:
            #    f.write(datastr)

            #print(data)

            index_col_num = 1
            if cc(config,'input','gps_index_column_number'):
                ia = xsplit(config['input']['gps_index_column_number'], type=int)
                if len(ia) == 1:
                    index_col_num = ia[0]
                else:
                    index_col_num = list(ia)

            gpsdata = make_pandas(gpsdatastr, gpsdata_columns, index=index_col_num,
                        sep=gc(config,'input','sep',alt=','), timestamp=yesno2bool(gc(config,'input','gps_timestamp','')))
            #data.attrs = config

            if not check_data(gpsdata): exit()

            with Progress('| Merge data and gps'):
                data = pandas.merge_asof(data, gpsdata, right_index=True, left_index=True)

            with Progress('| Cut period'):
                data = cut_period_to(data, gc(config,'clean','start'), gc(config,'clean','end'))

            if not check_data(data): exit()

    if cc(config, 'input', 'store_pickle') and not yesno2bool(gc(config, 'input', 'restore_from_pickle')):
        
        import compress_pickle as pickle
        pickle_file = gc(config, 'input', 'store_pickle')
        with open(pickle_file, 'wb') as f:
            pickle.dump(data, f)
            
        file_save_msg(pickle_file, 'Pickle DataFrame', end='\n')
            

    ####################################################
    ######  Basic cleaning  ############################
    ####################################################

    print('Quality check')
    
    if cc(config, 'clean', 'bbox'):
        bbox = xsplit(config['clean']['bbox'], type=float)
    else:
        bbox = []


    lat = gc(config, 'input',      'latitude_column')
    lon = gc(config, 'input',      'longitude_column')
    alt = gc(config, 'input',      'altitude_column')
    p  =  gc(config, 'input',      'pressure_column')
    rh =  gc(config, 'input',      'humidity_column')
    T  =  gc(config, 'input',      'temperature_column')
    ah =  gc(config, 'correction', 'new_humidity_column')
    inc = gc(config, 'correction', 'new_incoming_column')

    # Thermal neutrons
    th = '_thermal'
    # Cleaned data
    clean = '_clean'
    # Corrected data
    pih = '_pih'
    # Suffix for vairable errors
    err = '_err' # followed by _sym, _low, _upp
    # Standard deviation
    std = '_std'
    # Smooth
    mov = '_mov'
    # Biomass
    bio = '_bio'
    # Urban
    urb = '_urb'
    # Road
    road = '_road'

    # check wrong column names
    for name in [p, rh, T]:
        if not name in data.columns:
            print('! Warning: %s is not a column name.' % name)
            data[name] = np.nan

    J.report("general",
             "Original data period was from {:} to {:}.",
             data.index.min().strftime('%Y-%m-%d %H:%M:%S %Z'),
             data.index.max().strftime('%Y-%m-%d %H:%M:%S %Z'))

    print('i   Data period from', data.index.min().strftime('%Y-%m-%d %H:%M:%S %Z'),
                            'to', data.index.max().strftime('%Y-%m-%d %H:%M:%S %Z'))

    # Estimate time resolution if does not exist
    tres = gc(config, 'input', 'resolution_column', alt='N1ET_sec')
    if not tres in data.columns:
        data[tres] = infer_tres(data, default=gc(config, 'input', 'resolution_default'))

    print('i Time resolution approx. %.0f +/- %.2f sec.' % (data[tres].median(), data[tres].std()))
    #(%.2f-%.2f)  data[tres].min(), data[tres].max()))
    J.report("general",
             "Original median time resolution was {:.1f} ±{:.1f} sec.",
             data[tres].median(), data[tres].std()) 


    ## Sum up neutron columns
    N =  gc(config, 'correction', 'new_neutron_column')
    data[N] = 0

    for c in xsplit(config['input']['neutron_columns']):
        # If neutron range per tube is given, first filter outliers from each tube, then sum up
        if cc(config,'input','ignore_neutron_columns'):
            ignored = 0
            if c in xsplit(config['input']['ignore_neutron_columns']):
                ignored += 1
                print('i Ignoring column %s' % c)
                continue 
            J.report("neutrons", "As by configuration, {:.0f} tubes were ignored for this report.", ignored)
                
        if cc(config,'clean','neutron_range_per_tube'):
            if cc(config,'clean','neutrons_unit','cps'):
                data[c+err] = N_err(data[c]*data[tres])/data[tres]
            else:
                data[c+err] = N_err(data[c])

            (Napt,Nbpt)  = xsplit(gc(config,'clean','neutron_range_per_tube'), range=2, type=float)
            data[c+'_clean'] = find_outlier(data, c, lower=Napt, upper=Nbpt,
                                       n_sigma = gc(config,'clean','neutron_n_sigma', dtype=float),
                                       sigma = c+err)
            data[N] += data[c+'_clean']

        else:
            data[N] += data[c]
            data[c+'_clean'] = data[c]

    # Harmonize units to "sum per time interval" (e.g., styx logs mean counts per second)
    if cc(config,'clean','neutrons_unit','cps'):
        data[N] *= data[tres]

    # Calculate error
    data[N+err] = N_err(data[N])

    # Report
    report_N(data, N, units='raw counts')
    J.report("neutrons",
             "Data provided ~{:.0f} raw counts per record time,",
             data[N].median())

    # convert to units of cph
    data[N]     = Unit_convert(data[N], data[tres], 'cph')
    data[N+err] = Unit_convert(data[N+err], data[tres], 'cph')

    # Report
    report_N(data, N)
    J.report("neutrons",
             "which is ~{:.0f} ±{:.0f} cph.",
             data[N].median(), data[N+err].median()) 

    # Thermal neutrons
    if config['input']['thermal_neutron_columns']:

        # sum up thermal neutrons
        data[N+th] = 0
        for c in xsplit(config['input']['thermal_neutron_columns']):

            # If neutron range per tube is given, first filter outliers from each tube, then sum up
            if cc(config,'input','ignore_neutron_columns'):
                ignored = 0
                if c in xsplit(config['input']['ignore_neutron_columns']):
                    ignored += 1
                    print('i Ignoring column %s' % c)
                    continue 
                J.report("neutronsth", "As by configuration, {:.0f} tubes were ignored for this report.", ignored)

            if cc(config,'clean','neutron_range_per_tube'):
                if cc(config,'clean','neutrons_unit','cps'):
                    data[c+err] = N_err(data[c]*data[tres])/data[tres]
                else:
                    data[c+err] = N_err(data[c])

                (Napt,Nbpt)  = xsplit(gc(config,'clean','neutron_range_per_tube'), range=2, type=float)
                data[c+'_clean'] = find_outlier(data, c, lower=Napt, upper=Nbpt,
                                        n_sigma = gc(config,'clean','neutron_n_sigma', dtype=float),
                                        sigma = c+err)
                data[N+th] += data[c+'_clean']

            else:
                data[N+th] += data[c]
                data[c+'_clean'] = data[c]
        #
        # data[N+th] += data[c]
        # data[N+th+err] = N_err(data[N+th])
        # # convert to units of cph
        # data[N+th]     = Unit_convert(data[N+th], data[tres], 'cph')
        # data[N+th+err] = Unit_convert(data[N+th+err], data[tres], 'cph')

        # Harmonize units to "sum per time interval" (e.g., styx logs mean counts per second)
        if cc(config,'clean','neutrons_unit','cps'):
            data[N+th] *= data[tres]

        # Calculate error
        data[N+th+err] = N_err(data[N+th])

        # Report
        report_N(data, N+th, units='raw counts')
        J.report("neutronsth",
                 "Data showed ~{:.0f} raw thermal neutron counts,",
                 data[N+th].median()) 

        # convert to units of cph
        data[N+th]     = Unit_convert(data[N+th], data[tres], 'cph')
        data[N+th+err] = Unit_convert(data[N+th+err], data[tres], 'cph')

        # Report
        report_N(data, N+th)
        J.report("neutronsth",
                 "which is ~{:.0f} ±{:.0f} cph. ",
                 data[N+th].median(), data[N+th+err].median()) 


        Nth = N+th
    else:
        Nth = None

    ####################################################
    ######  Clean/filter  ##############################
    ####################################################
    print('[Cleaning]')

    has_coords = (lat and lon) or not np.isnan(coordinates[0:2]).any()

    # Clean altitude
    if not alt:
        if has_coords:
            data[alt] = coordinates[2]
        else:
            print('i No coordinates specified, assuming altitude = 0 m.')
            data[alt] = 0.0

    # Clean Latlon
    if not lat is None and lat != '' and lat in data.columns:
        #JJ Strangely formatted coordinates occur for some Rovers (Jülich, Melbourne)

        if 'EW' in data.columns or 'NS' in data.columns:
            print('| Trying to interpret EW/NS GPS data...')
            # Remove cardinal direction if it is attached to the coordinate (has been necessary in some configurations)
            data[lat] = pd.to_numeric(data[lat].astype(str).replace({'N':''}, regex=True), errors='coerce') # replace N if existent in column and convert to numeric
            data[lon] = pd.to_numeric(data[lon].astype(str).replace({'E':''}, regex=True), errors='coerce') # replace E if existent in column and convert to numeric
            # Convert to readable coordinate
            data.NS = 'N'
            data.EW = 'E'
            data[lat] = deg100min2dec( data[[lat, 'NS']] )
            data[lon] = deg100min2dec( data[[lon, 'EW']] )


        data.loc[data[lat] == 0.0, lat] = np.nan
        data.loc[data[lon] == 0.0, lon] = np.nan

        data['lon'] = data[lon]
        data['lat'] = data[lat]

        if 'interpolate_coords' in config['clean']:
            if yesno2bool(config['clean']['interpolate_coords']):
                data['lon'] = data['lon'].interpolate()
                data['lat'] = data['lat'].interpolate()

        data = data.dropna(subset=['lat'])
        data = data.dropna(subset=['lon'])

        if len(bbox)>0:
            data = data.query("lat > %s and lat < %s" % (bbox[2], bbox[3]))
            data = data.query("lon > %s and lon < %s" % (bbox[0], bbox[1]))
            print('cut out bbox')
            if len(data) == 0:
                print('! You seem to have erased the whole dataset using an unfortunate bbox.')
                return()
        else:
        # Derive bbox from data if not set in config
            bbox = [data[lon].min(), data[lon].max(), data[lat].min(), data[lat].max()]
            print('i   Derived bounding box:', bbox)

        try:
            data['ll'] = data.apply(lambda row: tuple((row['lat'],row['lon'])), axis=1)
        except ValueError:
            raise ValueError('NO GPS Data Available, processing stopped')
        #D = D.dropna(subset=['lat'])
        # Lat lon to UTM
        utmx, utmy = latlon2utm(data.lat, data.lon,
                epsg = gc(config,"clean","utm_epsg", alt=31468))
        data['utmy'] = utmy
        data['utmx'] = utmx

        #### exclude
        excl_list = gc(config,'clean','exclude_table', dtype='multi')
        if excl_list:
            _excl_data_columns = ['lat','lon','radius','utmx','utmy']
            _excl_data = []
            for item in excl_list:
                _excl_data.append(xsplit(item, range=len(_excl_data_columns)))
            excl_data = pandas.DataFrame(_excl_data, columns=_excl_data_columns)
            del _excl_data

            excl_data.lat = pandas.to_numeric(excl_data.lat)
            excl_data.lon = pandas.to_numeric(excl_data.lon)
            excl_data.radius = pandas.to_numeric(excl_data.radius)
            utmx, utmy = latlon2utm(excl_data.lat, excl_data.lon,
                epsg = gc(config,"clean","utm_epsg", alt=31468))
            excl_data['utmy'] = utmy
            excl_data['utmx'] = utmx

            for i, row in excl_data.iterrows():
                data['distance2excl'] = np.sqrt((data.utmx-row['utmx'])**2+(data.utmy-row['utmy'])**2)

                _query = (data.distance2excl < row['radius']) # & (~np.isnan(_data[Nfinal]))
                excl_lines = len(data[_query])
                if excl_lines > 0:
                    data.loc[_query, N] = np.nan
                    print('i Excluded %s points around (%.2f,%.2f)+/-%d m.' % (row['lat'], row['lon'], row['radius'], excl_lines))

        # if no geographic information available we stop processing here
        if data[lat].isna().sum()==len(data) or data[lon].isna().sum()==len(data):
            print('! Cannot find any GPS data:')
            raise ValueError('No GPS data available, processing stopped.')

        # replace altitude column with data from DEM
        if 'altitude_raster' in config['clean'] and config['clean']['altitude_raster']:
            data = get_from_raster(data, alt, config['clean']['altitude_raster'])
            print('i Derived altitude: %.0f +/- %.0f m' % (data[alt].mean(), data[alt].std()))

        if cc(config, 'clean', 'height_above_ground_column'):
            hag_column = gc(config, 'clean', 'height_above_ground_column')
            dem_raster_file = gc(config, 'clean', 'dem_raster')
            data = get_from_raster(data, 'dem', dem_raster_file)
            data[hag_column] = data[alt] - data['dem']
            print('i DEM: %.0f+/-%.0f, Altitude: %.0f+/-%.0f, HAG: %.0f+/-%.0f' \
                % (data['dem'].mean(), data['dem'].std(), data[alt].mean(),
                   data[alt].std(), data[hag_column].mean(), data[hag_column].std()))

    # Get upper and lower bounds from config
    (Na,Nb)  = xsplit(config['clean']['neutron_range'], range=2, type=float)
    (pa,pb)  = xsplit(config['clean']['pressure_range'], range=2, type=float)
    (rha,rhb)= xsplit(config['clean']['humidity_range'], range=2, type=float)
    (Ta,Tb)  = xsplit(config['clean']['temperature_range'], range=2, type=float)
    if 'timeres_range' in config['clean']:
        (tra,trb)= xsplit(config['clean']['timeres_range'], range=2, type=float)
    


    # Define new column N+clean
    data[N+clean] = data[N]
    data[N+clean+err] = data[N+err]
    Ncleanstr = N+clean

    if Nth:
        data[N+th+clean] = data[N+th]
        data[N+th+clean+err] = data[N+th+err]
        Nthcleanstr = N+th+clean

    #print(data[['N1Cts','N2Cts','N1ETsec','T_CS215','RH_CS215','N_clean']])

    neutron_n_sigma = gc(config,'clean','neutron_n_sigma', dtype=float)

    # Drop lines option
    if config['clean']['invalid_data'] == 'drop lines':
        len_before = len(data)
        data = data.query("@Ncleanstr > %s and @Ncleanstr < %s" % (Na, Nb))
        
        if Nth:
            data = data.query("@Nthcleanstr > %s and @Nthcleanstr < %s" % (Na, Nb))
            
        data = data.query("@p > %s and @p < %s" % (pa, pb))
        data = data.query("@rh> %s and @rh< %s" % (rha,rhb))
        data = data.query("@T > %s and @T < %s" % (Ta, Tb))
        
        if 'timeres_range' in config['clean']:
            data = data.query("@tres > %s and @tres < %s" % (tra, trb))
            
        print("dropped %s invalid lines." % (len_before - len(data)))
    # Fill-na option
    else:
        nan_before = data.isnull().sum().sum()
        local_storage_name = os.path.join(config['output']['out_path'],'') + config['output']['out_basename']

        # Clean range: neutrons
        data[Ncleanstr] = find_outlier(data, Ncleanstr, lower=Na, upper=Nb,
                                       n_sigma = neutron_n_sigma,
                                       sigma = Ncleanstr+err)
        if Nth:
            data[Nthcleanstr] = find_outlier(data, Nthcleanstr, lower=Na, upper=Nb,
                                       n_sigma = neutron_n_sigma,
                                       sigma = Nthcleanstr+err)
        #if Nth:
        #    data[N+th+clean] = find_outlier(data, N+th+clean, lower=Na, upper=Nb,
        #                                   n_sigma = gc(config,'clean','neutron_n_sigma', dtype=float),
        #                                   sigma = N+th+clean+err)
        # Clean range: time resolution
        if cc(config, 'clean', 'timeres_range'):
            data[Ncleanstr] = find_outlier(data, tres, Ncleanstr, lower=tra, upper=trb)
            if Nth:
                data[Nthcleanstr] = find_outlier(data, tres, Nthcleanstr, lower=tra, upper=trb)

        # Clean range: air humidity
        data[rh] = find_outlier(data, rh, lower=rha, upper=rhb)
        data[rh] = fill_gaps(data, rh, var='humidity', fill=gc(config, 'clean', 'missing_humidity'),
                            n_stations=4, lon=one_of(lon, coordinates[1]), lat=one_of(lat, coordinates[0]),
                            local_storage_name=local_storage_name)

        # Clean range: air temperature
        data[T] = find_outlier(data, T, lower=Ta, upper=Tb)
        data[T] = fill_gaps(data, T, var='temperature', fill=gc(config, 'clean', 'missing_temperature'),
                            n_stations=4, lon=one_of(lon, coordinates[1]), lat=one_of(lat, coordinates[0]),
                            local_storage_name=local_storage_name)

        # Clean range: air pressure
        if cc(config,'clean','pressure_unit','Pa'):
            data[p] /= 100
        elif cc(config,'clean','pressure_unit','kPa'):
            data[p] /= 10

        data[p] = find_outlier(data, p, lower=pa, upper=pb)
        data[p] = fill_gaps(data, p, var='pressure', fill=gc(config, 'clean', 'missing_pressure'),
                            n_stations=4, lon=one_of(lon, coordinates[1]), lat=one_of(lat, coordinates[0]),
                            alt = alt, T = T, local_storage_name=local_storage_name)

        print("i Replaced %s values by NaN." % (data.isnull().sum().sum() - nan_before))

    if len(data)==0:
        raise ValueError('! No valid data left :\'(')

    J.report("neutrons",
             "Cleaning dropped neutrons beyond {:.0f}—{:.0f} cph.",
             Na, Nb)
    J.report("neutronsth",
             "Cleaning dropped neutrons beyond {:.0f}—{:.0f} cph.",
             Na, Nb)
    if neutron_n_sigma and neutron_n_sigma>0:
        J.report("neutrons",
                 "Outliers with deviations >{:.1f} sigma were removed.",
                 neutron_n_sigma)
        J.report("neutronsth",
                 "Outliers with deviations >{:.1f} sigma were removed.",
                 neutron_n_sigma)                
    
    J.report("atmos",
             "Cleaning dropped data beyond {:.0f}—{:.0f} hPa, {:.0f}—{:.0f} % humidity, {:.0f}—{:.0f} °C.",
             pa, pb, rha, rhb, Ta, Tb)    
    if 'timeres_range' in config['clean']:
        J.report("general",
             "Cleaning dropped record periods beyond {:.0f}—{:.0f} sec.",
             tra, trb)

    #print(data[['N1Cts','N2Cts','N_clean']])


    # Interpolate over nans
    if yesno2bool(config['clean']['interpolate_neutrons']):

        max_gap_size = gc(config,'clean','interpolate_neutrons_gapsize',dtype=int)

        data[N+clean]     = data[N+clean].interpolate(limit=max_gap_size)
        data[N+clean+err] = data[N+clean+err].interpolate(limit=max_gap_size)
        J.report("neutrons",
                 "Data was interpolated through gaps with no more than {:.0f} missing records.",
                 max_gap_size)
        if Nth:
            data[N+th+clean]     = data[N+th+clean].interpolate(limit=max_gap_size)
            data[N+th+clean+err] = data[N+th+clean+err].interpolate(limit=max_gap_size)
            J.report("neutronsth",
                     "Data was interpolated through gaps with no more than {:.0f} missing records.",
                     max_gap_size)

    report_N(data, N+clean)
    if Nth:
        report_N(data, N+th+clean)

    #print(data[['N1Cts','N2Cts','N_clean']])


    terminal_plot(data[N+clean], lines=True, y_gridlines=[], x_gridlines=[])

    #data[['GpsUTC', 'LatDec', 'LongDec', 'Alt']].to_csv('testgps.csv', index=False)

    ####################################################
    ###### Muons #######################################
    ####################################################

    Muons = False
    if cc(config,'input','muon_column'):
        Muons = True
        mu =  gc(config,'input','muon_column')
        (Ma,Mb)  = gc(config,'clean','muon_range', range=2, dtype=list, types=float)
        data[mu+"_cph"] = Unit_convert(data[mu], data[tres], 'cph')
        data[mu+clean] = find_outlier(data, mu+"_cph", lower=Ma, upper=Mb)
        
        beta_M  = gc(config,'correction','muon_beta',  dtype=float)
        alpha_M = gc(config,'correction','muon_alpha', dtype=float)
        Tref_M  = gc(config,'correction','muon_T_ref', dtype=float)
        
        data[mu+clean+"_p"]  = data[mu+clean]      * np.exp((data[p] - 1013) / beta_M)
        data[mu+clean+"_ph"] = data[mu+clean+"_p"] * (1 + alpha_M * (data[T] - Tref_M))

        mufinal = mu+clean+"_ph"

        J.report("muons",
             "Muons have been dropped beyond below {:.0f} and above {:.0f} cph. " \
            +"Corrections were applied based on Stevanato et al. (2022) using beta={:.0f}, alpha={:.0f}, and T_ref={:.0f}.",
             Ma, Mb, beta_M, alpha_M, Tref_M)  

    ####################################################
    ######  Soil #######################################
    ####################################################

    print('> Sampling land data... ')
    #depending on Option for soil data retrieval we add soil data
    #print('Retrieve soil properties from constant values')
    #add static data -- this is the default and also the basis
    data['bd']  = float(config['conversion']['bulk_density'])
    data['org'] = float(config['conversion']['soil_org_carbon'])
    data['lw']  = float(config['conversion']['lattice_water'])

    # add from raster datasets
    if cc(config,'conversion','soil_data_source', value='raster'):
        if not has_coords:
            ValueError('No information on coordinates, no retrieval of soil info possible, change input option!')
        else:
            print('Retrieve soil properties from static maps')

            if config['conversion']['bulk_density_raster']:
                print('> %s' % config['conversion']['bulk_density_raster'])
                data = get_from_raster(data, 'bd',   config['conversion']['bulk_density_raster'])
            if config['conversion']['clay_content_raster']:
                print('> %s' % config['conversion']['clay_content_raster'])
                data = get_from_raster(data, 'clay', config['conversion']['clay_content_raster'])
            if config['conversion']['soil_org_carbon_raster']:
                print('> %s' % config['conversion']['soil_org_carbon_raster'])
                data = get_from_raster(data, 'org',  config['conversion']['soil_org_carbon_raster'])
            J.report("soil",
                     "Soil properties were sampled from raster data,")

        #convert lattice water from clay
        data['lw']  = lw_from_clay(data['clay'], method='Greacen et al. 1981')

    # add from soilgrids
    elif cc(config,'conversion','soil_data_source', value='soilgrids'):
        #check whether geographic information exist
        if not has_coords:
            ValueError('No information on coordinates, no retrieval of soil info possible, change input option!')
        else:
            print('Retrieve soil properties from SoilGrids API')
            soil_data=retrieve_soilgrid_data(data.copy(),
                        lon = one_of(lon, coordinates[1]), lat = one_of(lat, coordinates[0]),
                        soilgridlrs=['bdod','clay','soc'],
                        soil_layerdepths=['5-15cm'], #'0-5cm','5-15cm','15-30cm','30-60cm'],
                        raster_res=(250, 250),
                        statistical_metric=['mean'],
                        all_touched=True)

            # add the soil data to the dataset
            data['org'] = soil_data['org'] /10 # new units
            data['bd']  = soil_data['bd']
            data['clay']= soil_data['clay']
            J.report("soil",
                     "Soil properties were sampled from SoilGrids.org,")
    else:
        J.report("soil",
                 "Constant soil properties were provided")
        
    if 'bd' in data.columns:
        data.loc[data.bd   <= 0, 'bd'] = np.nan
        report_mmsm(data.bd)
        J.report("soil",
                 "with bulk density ~{:.2f} ±{:.2f} kg/m³,",
                 data['bd'].median(), data['bd'].std())
        data['bd'].fillna(1.0, inplace=True)
        
    lw_method = "Greacen et al. 1981"
    if 'clay' in data.columns:
        data.loc[data.clay <= 0, 'clay'] = np.nan
        report_mmsm(data.clay)
        J.report("soil",
                 "clay content ~{:.2f} ±{:.2f} %%,",
                 data['clay'].median(), data['clay'].std())
        data['clay'].fillna(0.0, inplace=True)

        data['lw']  = lw_from_clay(data['clay'], method=lw_method)
        report_mmsm(data.lw)

    owe_method = "Franz et al. 2015"
    if 'org' in data.columns:
        data.loc[data.org  < 0, 'org'] = np.nan
        report_mmsm(data.org)
        J.report("soil",
                 "organic carbon ~{:.2f} ±{:.2f} g/g.",
                 data['org'].median(), data['org'].std())
        data['org'].fillna(0.0, inplace=True)
        #convert organic water equivalent from organic matter content
        data['owe'] = owe_from_corg(data['org'], method=owe_method)
        report_mmsm(data.owe)

    J.report("soil",
             "Missing soil data was filled with 1 kg/m³, 0% clay, 0 g/g SOC, respectively.")
    J.report("soil",
             "Lattice water was derived from clay content using {:} and was ~{:.2f} ±{:.2f} g/g.",
             lw_method, data['lw'].median(), data['lw'].std())
    J.report("soil",
             "Organic water equivalent was derived from SOC using {:} and was ~{:.2f} ±{:.2f} g/g.",
             owe_method, data['owe'].median(), data['owe'].std())

    #add landuse data
    if lat in data.columns:
        if cc(config,'conversion','land_use_data_source', value='raster'):
            print('> %s' % gc(config,'conversion','land_use_raster'))
            data = get_from_raster(data, 'luse', gc(config,'conversion','land_use_raster'))
            #data['luse_str'] = ''
            data.loc[data.luse==2, 'luse_str'] = 'urban'
            data.loc[data.luse==18, 'luse_str'] = 'agriculture'
            data.loc[data.luse==7, 'luse_str'] = 'mineral_extraction'
            data.loc[data.luse==24, 'luse_str'] = 'forest' # Coniferous
            data.loc[data.luse==3, 'luse_str'] = 'Industry' # Coniferous
            data.loc[data.luse==23, 'luse_str'] = 'forest' # woodland
            data.loc[data.luse==24, 'luse_str'] = 'forest' # mixes
            data.loc[data.luse==25, 'luse_str'] = 'forest' # woodland
            data.loc[data.luse==29, 'luse_str'] = 'forest' # woodland
            data.loc[data.luse==12, 'luse_str'] = 'agriculture' # arable land
            data.loc[data.luse==16, 'luse_str'] = 'pasture' # arable land
            J.report("luse", "Land use was sampled from raster file:")

        elif cc(config,'conversion','land_use_data_source', value='corine'):
            data['luse'] = exdata.corine.retrieve_corine_data(data,
                            lon = one_of(lon, coordinates[1]),
                            lat = one_of(lat, coordinates[0])
                            ).values.astype(int)
            data['luse_str'] = luse_code2str(data)
            J.report("luse", "Land use was sampled from CORINE:")

        if 'luse_str' in data.columns:
            luse_unique = data.luse_str.unique()
            list_of_luse_types = ', '.join([ str(luse)+'(%d)' % (data.loc[data.luse_str==luse, 'luse_str'].count()) for luse in luse_unique ])
            print('i Found land use types: %s' % list_of_luse_types)
            J.report("luse", "{:}", list_of_luse_types)

    #print('bulk dens. (%.2f +/- %.2f g/cm3), lattice w. (%.2f +/- %.2f g/g), org.we. (%.2f +/- %.2f g/g)' % (data.bd.mean(), data.bd.std(), data.lw.mean(), data.lw.std(), data.owe.mean(), data.owe.std()))


    ####################################################
    ######  Incoming  ##################################
    ####################################################
    print('[Corrections]')

    data.index = data.index.tz_localize(None)
    data.index = data.index.tz_localize('UTC')
    data = data[~data.index.duplicated(keep='first')]

    print('> Reading incoming radiation data from ' + config['correction']['NM_path'])

    M = None
    if cc(config, "correction", "incoming_method"):
        M = NM(config=config).get(
            start = data.index.min().replace(tzinfo=pytz.UTC),
            end   = data.index.max().replace(tzinfo=pytz.UTC))

        if isinstance(M.data, pandas.DataFrame):
            M.data.index = M.data.index.tz_localize(None)
            M.data.index = M.data.index.tz_localize('UTC')
            M.data = M.data[~M.data.index.duplicated(keep='first')]
            M.data = M.data.reindex(data.index, method='nearest', limit=1, tolerance=timedelta(seconds=M.resolution_min*60/2-1)).interpolate()
            data[inc] = M.data
            data.loc[data[inc] < data[inc].mean()/1.2, inc] = np.nan
            data.loc[data[inc] > data[inc].mean()*1.2, inc] = np.nan
        else:
            M = None
    
    if M is None:
        data[inc] = gc(config, "correction", "incoming_ref", dtype=float)

    
    ####################################################
    ######  pih  #######################################
    ####################################################

    print("\n| Corrections... ", end='')
    data[ah] = abs_humidity(data[rh], data[T])
    J.report("atmos",
             "Derived absolute air humidity was ~{:.1f} ±{:.1f} g/m³.",
             data[ah].median(), data[ah].std())

    data['correct_h'] = C_humid( data[ah], gc(config, 'correction', 'humidity_ref', dtype=float),
                                           gc(config, 'correction', 'alpha', dtype=float),
                                           method=gc(config, 'correction', 'humidity_method'))
    J.report("corrections",
             "Air humidity correction was applied following {:} with alpha={:.4f} and reference {:.1f} g/m³.",
             gc(config, 'correction', 'humidity_method'),
             gc(config, 'correction', 'alpha', dtype=float),
             gc(config, 'correction', 'humidity_ref', dtype=float))

    data['correct_p'] = C_press( data[p],  gc(config, 'correction', 'pressure_ref', dtype=float),
                                           gc(config, 'correction', 'beta', dtype=float),
                                           method=gc(config, 'correction', 'pressure_method'),
                                           inclination = gc(config, 'correction', 'inclination', dtype=float))
    J.report("corrections",
             "Air pressure correction was applied following {:} with beta={:.1f} and reference {:.2f} hPa.",
             gc(config, 'correction', 'pressure_method'),
             gc(config, 'correction', 'beta', dtype=float),
             gc(config, 'correction', 'pressure_ref', dtype=float))


    if gc(config, 'correction', 'incoming_method') == "McJannet and Desilets et al. (2023)":
        data["Rc"] = gc(config,"correction","Rc", alt=2.3)
        data_coord = data[[lat, lon, alt, "Rc"]].copy()
    else:
        data_coord = None

    data['correct_i'] = C_inc( data[inc],  gc(config, 'correction', 'incoming_ref', dtype=float),
                                           gc(config, 'correction', 'gamma', dtype=float),
                                           method=gc(config, 'correction', 'incoming_method'),
                                           Rc = gc(config, 'correction', 'Rc', dtype=float),
                                           Rc_NM = gc(config, 'correction', 'Rc_NM', dtype=float),
                                           data = data_coord)

    J.report("corrections",
             "Incoming correction was applied following {:} with gamma={:.2f}, R_cutoff={:.2f} GV, and reference {:.1f} cps,",
             gc(config, 'correction', 'incoming_method'),
             gc(config, 'correction', 'gamma', dtype=float),
             gc(config, 'correction', 'Rc', dtype=float),
             gc(config, 'correction', 'incoming_ref', dtype=float))
    J.report("corrections",
             "using data from neutron monitor station '{:}' with time resolution of {:} via the NMDB.eu.",
             gc(config,'correction','NM_station'), gc(config,'correction','NM_resolution'))

    if cc(config, 'correction', 'hag_method'):
        data['correct_H'] = C_hag( data[hag_column], gc(config, 'correction', 'hag_eta', dtype=float),
                                           method=gc(config, 'correction', 'hag_method'))
        print("for height above ground (%+.0f%%)" % (100*(data['correct_H'].mean()-1)))
        data[N+clean] *= data['correct_H']
        data[N+clean+err] *= data['correct_H']

        J.report("corrections",
                 "Height above ground was corrected for following {:} with heights of {:.0f} ±{:.0f} m.",
                 gc(config,'correction','hag_method'),
                 data[hag_column].median(), data[hag_column].std())



    data[N+clean+pih]     = data[N+clean]     * data['correct_p'] * data['correct_i'] * data['correct_h']
    data[N+clean+pih+err] = data[N+clean+err] * data['correct_p'] * data['correct_i'] * data['correct_h']

    if Nth:
        data[N+th+clean+pih]     = data[N+th+clean]     * data['correct_p'] * data['correct_i'] * data['correct_h']
        data[N+th+clean+pih+err] = data[N+th+clean+err] * data['correct_p'] * data['correct_i'] * data['correct_h']
        Nthfinal = N+th+clean+pih

    print("for pressure (%+.0f%%), abs. humidity (%+.0f%%), incoming radiation (%+.0f%%)" % \
        (100*(data['correct_p'].mean()-1), 100*(data['correct_h'].mean()-1), 100*(data['correct_i'].mean()-1)))

    Nfinal = N+clean+pih
    report_N(data, N+clean+pih, correction=['correct_p','correct_i','correct_h'])


    #If no data remain with valid corrected data, we stop processing
    if data[N+clean+pih].isna().sum()==len(data):
        print('! No valid neutron data remaining. Are all meteo or GPS data out of range? Or is one of the neutron tubes sick? (then the sum of all tubes would be nan, too).')
        raise ValueError('No valid neutron data remaining.')

    ####################################################
    ######  Urban  #####################################
    ####################################################

    data['correct_urban'] = 1

    if 'luse_str' in data.columns and 'land_use_urban_corr' in config['conversion'] and config['conversion']['land_use_urban_corr'] != '':
        if config['conversion']['land_use_urban_corr'] == 'nan':
            data.loc[data.luse_str=='urban', 'correct_urban'] = np.nan
        else:
            data.loc[data.luse_str=='urban', 'correct_urban'] = float(config['conversion']['land_use_urban_corr'])

        data[Nfinal+urb] = data[Nfinal] * data['correct_urban']
        data[Nfinal+urb+err] = data[Nfinal+err] * data['correct_urban']
        Nfinal += urb
        report_N(data, Nfinal, 'correct_urban')
    else:
        urb = ''

    ####################################################
    ######  Bio  #######################################
    ####################################################

    ## vegetation correct
    ## assume neutron counts were reduced by 10% for every measurement in a forest
    data['correct_bio'] = 1
    data['correct_bio_err'] = 0

    bio = ""
    if 'luse_str' in data.columns and 'land_use_forest_corr' in config['conversion'] and config['conversion']['land_use_forest_corr'] != '':
        if config['conversion']['land_use_forest_corr'] == 'nan':
            data.loc[data.luse_str=='forest', 'correct_bio'] = np.nan

        else:
            data.loc[data.luse_str=='forest', 'correct_bio'] = float(config['conversion']['land_use_forest_corr'])
            print("| Corrected %.0f data points for forest biomass (%+.0f%%)." % (len(data[data.luse_str=='forest']), 100*(data.loc[data.luse_str=='forest', 'correct_bio'].mean()-1)))

        # well do that after road correction
        #data[Nfinal+'_bio'] = data[Nfinal] * data['correct_bio']
        #data[Nfinal+'_bio'+err] = data[Nfinal+err] * data['correct_bio']
        #Nfinal += '_bio'
        #report_N(data, Nfinal, 'correct_bio')
        bio = "_bio"

        J.report("corrections",
            "Vegetation correction of neutron counts by {:+.1f}% was applied on {:.0f} data points with forested land use.",
            100*(data.loc[data.luse_str=='forest', 'correct_bio'].mean()-1),
            len(data[data.luse_str=='forest']) )
    
    if cc(config,'conversion','veg_method'):

        veg_biomass = gc(config, 'conversion','biomass', dtype=float)
        veg_nu      = gc(config, 'conversion','nu', dtype=float)
        veg_method  = gc(config, 'conversion','veg_method')

        data['correct_bio'] *= C_veg(veg_biomass, veg_nu, veg_method)
        bio = "_bio"

        print("| Correction for biomass (%+.1f%%)." % (100*(data['correct_bio'].mean()-1)))

        J.report("corrections",
            "Vegetation correction of neutron counts by {:+.1f}% was applied on the whole dataset, assuming constant biomass of {:.1f} kg/m² and following {:} with paramter nu={:f}.",
            100*(data['correct_bio'].mean()-1),
            veg_biomass,
            veg_method,
            veg_nu)
    

    ####################################################
    ######  Road  ######################################
    ####################################################

    data['correct_road'] = 1
    #if config['conversion']['road_method'] != '' and config['conversion']['road_method'] != 'None' and lat in data.columns:

    if cc(config,'conversion','road_method', 'Schroen et al. (2018)'):
        if lat in data.columns:

            print('| Correction for road effect...')
            # Load road properties (goes to config?)

            #print('|   Loading road type correction properties...')
            print('>   %s' % config['conversion']['road_type_table'])
            Roads = pd.read_csv(config['conversion']['road_type_table'], sep=r'\s+', skipinitialspace=True, comment='#')

            ####Now we decide whether we get the road_network_Data####
            ####from osmnx or fromroverweb Overpass API####
            if 'road_network_source' in config['conversion'] and config['conversion']['road_network_source']=='osm-nx':
                print('retrieve road network information from osmnx')
                # Load road network
                #print('>   ', end='') # Graph prints a message.
                road_network_file = config['conversion']['road_network_file']

                # Load nearest road types
                road_type_file = os.path.join(config['output']['out_path'],'') + config['output']['out_basename'] +'.roads' #configfile[0:configfile.index('.cfg')]
                print(os.path.join(config['output']['out_path'],'') + config['output']['out_basename'] +'.roads')
                print('|   Loading road network... ', end='')
                if os.path.exists(road_type_file):
                    # Read from file
                    #print('|   Loading nearest road types...')
                    print('\n>   %s' % road_type_file)
                    roadsdf = pd.read_csv(road_type_file, index_col=0, parse_dates=True, infer_datetime_format=True)
                    data['road'] = roadsdf
                else:
                    #download via osmnx, boundary is used from a Polygon, However, bbox is possible as well
                    boundary_polygon=footprint.retrieve_footprints(data,footprint_radius=200,lon=lon,lat=lat,
                            output=None,unify=True)
                    R = Graph('roads', boundary_polygon, file=road_network_file, report_save='<   Saved')
                    R.get(save=True) # read from file, or download and save

                    if len(R.data)<=0:
                        print('i   Road network is empty!')
                    else:
                        # Calculate
                        print('|   Searching nearest roads...')
                        # invoke progress bar feature
                        from tqdm import tqdm
                        tqdm.pandas()

                        data['road'] = data.progress_apply(lambda row: R.on_road_type(point=row['ll']), axis=1)

                        print('    Set priorities...')
                        for i, row in data.iterrows():
                            row = row.copy()
                            if isinstance(row.road, list):
                                if 'primary' in row.road:
                                    data.loc[i, 'road'] = 'primary'
                                elif 'track' in row.road:
                                    data.loc[i, 'road'] = 'track'
                            #print(D.loc[i, 'road'], end=' ')
                        #print('')

                        # Save backup
                        data['road'].to_csv(road_type_file)
                        file_save_msg(road_type_file, 'Nearest roads', end="\n")

                J.report("corrections",
                         "Road effect was corrected following {:} using data from OSMNX,",
                         gc(config,'conversion','road_method'))

            elif 'road_network_source' in config['conversion'] and config['conversion']['road_network_source']=='osm-overpass':
                print('| Overpass')
                print('retrieve road network information from overpass API')
                data=retrieve_osm_overpass_data(data, lon=lon,lat=lat,crcl_radius=50,
                                no_of_clusters=None,queryfeatures={'way': ['highway']},
                                output_col_name='road')
                
                J.report("corrections",
                         "Road effect was corrected following {:} using data from Overpass API,",
                         gc(config,'conversion','road_method'))

            # Set road values
            data['road_moisture'] = np.nan
            data['road_width'] = 0.0
            if 'road' in data.columns:
                for i, row in Roads.iterrows():
                    row = row.copy()
                    data.loc[data.road==row['category'], 'road_moisture'] = row['moisture']
                    data.loc[data.road==row['category'], 'road_width']    = row['width']
            else:
                print('|   Applying constant road parameters theta=%s, width=%s' % (config['conversion']['road_moisture'], config['conversion']['road_width']))
                data['road_moisture'] = float(config['conversion']['road_moisture'])
                data['road_width']    = float(config['conversion']['road_width'])
            #pdb.set_trace()
            # correct for biomass to avoid smearing out
            data[Nfinal+'_temp'] = data[Nfinal] * data['correct_bio']
            if cc(config,'clean','smooth'):
                data = moving_avg(data, Nfinal+'_temp', window=gc(config,'clean','smooth',dtype=int), err=False, std=False)
            # back to Nfinal, but mavged
            data[Nfinal+'_temp_mov'] /= data['correct_bio']
            # N2SM first order
            if cc(config,'conversion','N0'):
                N0 = gc(config,'conversion','N0', dtype=float)
            else:
                N0 = np.nanmax(data[Nfinal])*1.1
                print('! N0 is not defined, but we need it for road correction. Guessing %.0f cph.' % N0)
            
            if cc(config,'conversion','N2sm_method','Desilets et al. (2010)'):    
                data['sm_temp'] = N2SM_Desilets(data[Nfinal+'_temp_mov'], N0, bd=data.bd, lw=data.lw, owe=data.owe,
                            a0=float(config['conversion']['a0']), a1=float(config['conversion']['a1']), a2=float(config['conversion']['a2']))
            
            elif cc(config,'conversion','N2sm_method','Schmidt et al. (2021)'):
                if cc(config, 'conversion','Schmidt_parameterset'):
                    parameterset = gc(config, 'conversion','Schmidt_parameterset')
                else:
                    parameterset = 'Mar21_mcnp_drf'
                data['sm_temp'] = N2SM_Schmidt(data, Nfinal+'_temp_mov', ah, N0, bdstr='bd', lwstr='lw', owestr='owe', method=parameterset)
            else:
                print("! No valid sm conversion method selected.")    

            # C_road
            data['correct_road'] = 1/(1 + 0.42*(1-np.exp(-0.5*data.road_width)) * (1.06-4*data.road_moisture-(0.16 + data.road_moisture)/(0.39 + data['sm_temp'])))
            data.loc[(data['correct_road'] > 1), 'correct_road'] = 1
            #data.loc[data.road_width==0, 'correct_road'] = 1
            data.correct_road = data.correct_road.fillna(1)
            print("i   ...road effect (%+.0f%%)." % (100*(data['correct_road'].mean()-1)))

            J.report("corrections",
                     "with average road moisture ~{:.2f} and road width ~{:.1f} m,",
                     data["road_moisture"].median(), data["road_width"].median())
            J.report("corrections",
                     "which led to {:+.1f}% correction.",
                     100*(data['correct_road'].mean()-1))
            
            # tidy up temp columns
            #data.drop([Nfinal+'_temp', 'sm_temp'], axis=1, inplace=True)

    else:
        road = ''


    # Set N_bio
    data[Nfinal+bio] = data[Nfinal] * data['correct_bio']

    if 'land_use_forest_corr_err' in config['conversion']:
        data['correct_bio_err'] = float(config['conversion']['land_use_forest_corr_err'])
    data[Nfinal+bio+err] = errpropag_factor(data, Nfinal, 'correct_bio', err)
    #data[Nfinal+bio+err]      = data[Nfinal+err]     * data['correct_bio']

    Nfinal += bio
    report_N(data, Nfinal, 'correct_bio')

    if cc(config,'conversion','road_method') and gc(config,'conversion','road_method').lower() != 'none'  and lat in data.columns:

        # set N_road
        data[Nfinal+road]     = data[Nfinal]     * data['correct_road']

        data['correct_road_err'] = float(config['conversion']['road_correction_err'])
        data[Nfinal+road+err] = errpropag_factor(data, Nfinal, 'correct_road', err)
        #data[Nfinal+road+err] = data[Nfinal+err] / data['correct_road']

        Nfinal += road
        report_N(data, Nfinal, 'correct_road')

    #if config['conversion']['road_moisture'] and float(config['conversion']['road_width']) > 0 and not 'correct_road' in data.columns:
     #           print("| Corrections for road effect... ", end='')
                #if   config['correction']['road_material'] == 'stone':    road_theta = 0.04
                #elif config['correction']['road_material'] == 'concrete': road_theta = 0.07
                #elif config['correction']['road_material'] == 'asphalt':  road_theta = 0.10
      #          road_theta = float(config['conversion']['road_moisture'])


    ####################################################
    ######  AREA  ######################################
    ####################################################

    if cc(config,'conversion','areal_correction_map'):
        
        print('# Areal correction')
        areal_img = gc(config,'conversion','areal_correction_map')
        with Progress('> Reading site-specific heterogeneity from %s' % areal_img):
        
            from corny.grains.uranos import URANOS
            U = URANOS(folder=os.path.dirname(areal_img)+'/',
                    scaling=2, hum=np.nanmean(data['ah']), verbose=False)
            U = U.read_materials(os.path.basename(areal_img)).generate_distance().find_regions()
            
        if cc(config,'conversion','N0'):
            N0 = gc(config,'conversion','N0', dtype=float)
        else:
            N0 = np.nanmax(data[Nfinal])*1.1
        
        contributions_sm = np.array([1,5,10,20,40,60,100])/100
        contributions_values = []

        with Progressbar(len(contributions_sm), '| Calculating characteristic contributions') as Pb:
            for sm in contributions_sm:
                Pb.update('sm=%.2f' % sm)
                U = U.material2sm(sm).genereate_weights().estimate_neutrons(N0=N0, bd=np.nanmean(data.bd))
                contributions_values.append(U.region_data['Contributions'][0])
        contributions = pandas.DataFrame(dict(sm=contributions_sm, c=contributions_values))

        with Progress('|  Interpolating'):
            from scipy.interpolate import interp1d
            estimate_contributions = interp1d(contributions.sm.values, contributions.c.values, kind='cubic',
                fill_value=(contributions.c.values[0], contributions.c.values[-1]), bounds_error=False)
            #contributions_spline = pandas.DataFrame()
            #contributions_spline['sm'] = np.arange(0.01,0.99,0.01)
            #contributions_spline['v'] = estimate_contributions(np.arange(0.01,0.99,0.01))
            #contributions_spline.set_index('sm', inplace=True)

        with Progress('| Estimating initial soil moisture'):
            if cc(config, 'conversion','Schmidt_parameterset'):
                parameterset = gc(config, 'conversion','Schmidt_parameterset')
            else:
                parameterset = 'Mar21_mcnp_drf'
            
            data['sm_temp'] = N2SM_Schmidt(data, Nfinal, ah, N0, bdstr='bd', lwstr='lw', owestr='owe', method=parameterset)
        
        with Progress('| Estimating main signal contribution'):
            data['contribution'] = estimate_contributions(data['sm_temp'])
        print('i Site-characteristic signal contribution: %.1f +/- %.1f %%' % 
              (np.nanmean(data.contribution)*100, np.nanstd(data.contribution)*100))

        with Progress('| Signal correction based on site characteristics'):
            data[Nfinal+'_a'] = ( data[Nfinal] - np.nanmean(data[Nfinal]))/data['contribution'] + np.nanmean(data[Nfinal])
            data[Nfinal+'_a'+err] = data[Nfinal+err] * data[Nfinal+'_a']/data[Nfinal]
            Nfinal = Nfinal+'_a'
        
        report_N(data, Nfinal)
        J.report("corrections",
                 "Areal correction was applied using the site-characteristic signal contribution: {:.1f} ±{:.1f} %.",
                 np.nanmean(data.contribution)*100, np.nanstd(data.contribution)*100)

    ####################################################
    ######  SNOW  ######################################
    ####################################################

    #total snow water equivalent in mm
    if cc(config,'conversion','snow_data_source', value='DWD'):
        data['SWE'] = exdata.meteoservice.retrieve_dwd_data(data,lon = lon,
                                                            lat = lat,
                                                            dwd_category='water_equiv',
                                                            dwd_parameters=['total_snow_water_equivalent'],
                                                            no_of_nearest_stations = 5,
                                                            temporal_resolution='daily')['total_snow_water_equivalent']


    ####################################################
    ######  RAIN  ######################################
    ####################################################
    #first we determine the temporal resolution
    #Estimate the temporal resolution for the dwd data in seconds
    temporal_resolutions=np.array([600,3600,86400])
    #get the column with the temporal resolution
    print(tres, 'column determines the temporal resolution')
    #get the closest resolution in seconds
    temporal_resolution=temporal_resolutions.flat[np.abs(temporal_resolutions - data[tres].mean()).argmin()]
    #determine the temporal resolution string for dwd
    if temporal_resolution==600:
        dwd_resolution='10_minutes'
        dwd_par='precipitation_heigth_of_the_last_10_minutes'
        dwd_category='precipitation'
    if temporal_resolution==3600:
        dwd_resolution='hourly'
        dwd_par='hourly_precipitation_height'
        dwd_category='precipitation'
    if temporal_resolution==86400:
        dwd_resolution='daily'
        dwd_category='more_precip'
        dwd_par='daily_precipitation_height'
    print('Temporal Resolution to retrieve DWD data is ',dwd_resolution )
    #add information on current rainfall from station precipitation_heigth_of_the_last_10_minutes
    if cc(config,'conversion','rainfall_data_source', value='DWD'):
        data['rain'] = exdata.meteoservice.retrieve_dwd_data(data,lon = lon,
                                                            lat = lat,
                                                            dwd_category=dwd_category,
                                                            dwd_parameters=[dwd_par],
                                                            no_of_nearest_stations = 5,
                                                            temporal_resolution=dwd_resolution)[dwd_par]


    neutron_columns = [Nfinal]
    if Nth: neutron_columns += [N+th, N+th+clean, N+th+clean+pih]

    ####################################################
    ######  Split tracks ###############################
    ####################################################

    dataset = [ data ]
    Summary = pandas.DataFrame()
    continue_track_id = False

    if yesno2bool(gc(config,'clean','split_tracks')):

        data_split = data[['utmx', 'utmy']].copy()
        # Take all the points that are within 20 m of the start location
        minimum_distance = gc(config,'clean','split_location_radius', dtype=float) #50 # meter
        minimum_duration = gc(config,'clean','split_min_duration', dtype=float)

        loc_A = xsplit(gc(config,'clean','split_location_A'), type=float)
        loc_B = xsplit(gc(config,'clean','split_location_B'), type=float)
        AB_distance = np.sqrt((loc_A[0]-loc_B[0])**2 + (loc_A[1]-loc_B[1])**2)

        # Mask nearby points
        data_split['near_A'] = ((data_split.utmx - loc_A[0])**2 + (data_split.utmy - loc_A[1])**2 < minimum_distance**2)
        data_split['near_B'] = ((data_split.utmx - loc_B[0])**2 + (data_split.utmy - loc_B[1])**2 < minimum_distance**2)
        # Select only data which is either near A or B
        data_split = data_split[data_split.near_A | data_split.near_B]
        # Calculate time step difference in minutes
        data_split['diff'] = data_split.index.to_series().diff().dt.total_seconds()/60
        # Manually allow first value to be recognized, too
        data_split.loc[data_split.index[0], 'diff'] = minimum_duration

        # Get all points with large jumps in time
        Turns = data_split[data_split['diff'] >= minimum_duration]
        # If a jump to A is followed by a jump to B, it's a starting spot for the track from A to B
        # Call the latter jump the arrival of that track at B.
        Turns['leads_to_B'] = Turns.near_B.shift(-1)
        Turns['arriv_at_B'] = Turns.near_A.shift(+1)
        Dir1_start = Turns[Turns.near_A & Turns.leads_to_B].index
        Dir1_end   = Turns[Turns.near_B & Turns.arriv_at_B].index
        # Same the other way round
        Turns['leads_to_A'] = Turns.near_A.shift(-1)
        Turns['arriv_at_A'] = Turns.near_B.shift(+1)
        Dir2_start = Turns[Turns.near_B & Turns.leads_to_A].index
        Dir2_end   = Turns[Turns.near_A & Turns.arriv_at_A].index

        # Sort Tracks and set ID
        Dir1 = pandas.DataFrame()
        Dir1['start'] = Dir1_start
        Dir1['end'] = Dir1_end
        Dir1['track_id'] = [x+1 for x in range(len(Dir1_start))]
        Dir1['dir'] = 1
        Dir2 = pandas.DataFrame()
        Dir2['start'] = Dir2_start
        Dir2['end'] = Dir2_end
        Dir2['track_id'] = [x+1 for x in range(len(Dir2_start))]
        Dir2['dir'] = 2
        Tracks = pandas.concat([Dir1, Dir2]).sort_values('start')
        Tracks.index = [x+1 for x in range(len(Tracks))]

        # Plot
        output_basename = os.path.join(config['output']['out_path'],'') + config['output']['out_basename'] + \
            data.index.min().strftime('%Y%m%d') + '-split_tracks'
        figure_name = output_basename + '.pdf'
        with matplotlib.backends.backend_pdf.PdfPages(figure_name) as fig:
            with Fig(fig, title='Split Tracks', ylabel='Distance from Location A (in km)') as ax:

                data_dist_A = np.sqrt((data.utmx - loc_A[0])**2 + (data.utmy - loc_A[1])**2)/1000
                data_dist_A.plot(ax=ax, marker='o', ms=1, ls='', color='black', label='Track data')
                ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%H:%M'))
                ax.figure.autofmt_xdate(rotation=0, ha='center')

                ax.grid()
                for track_id in range(len(Dir1_start)):
                    mylabel = 'A $\longrightarrow$ B' if track_id==0 else None
                    ax.axvspan(Dir1_start[track_id], Dir1_end[track_id], color='C0', alpha=0.2, label=mylabel)
                    ax.text(Dir1_start[track_id]+(Dir1_end[track_id]-Dir1_start[track_id])/2, data_dist_A.max(),
                        '%d' % (Tracks[Tracks['start']==Dir1_start[track_id]].index.values[0]),
                        ha="center", va="center", bbox = dict(boxstyle="circle", fc='none'))
                for track_id in range(len(Dir2_start)):
                    mylabel = 'B $\longrightarrow$ A' if track_id==0 else None
                    ax.axvspan(Dir2_start[track_id], Dir2_end[track_id], color='C1', alpha=0.2, label=mylabel)
                    ax.text(Dir2_start[track_id]+(Dir2_end[track_id]-Dir2_start[track_id])/2, data_dist_A.max(),
                        '%d' % (Tracks[Tracks['start']==Dir2_start[track_id]].index.values[0]),
                        ha="center", va="center", bbox = dict(boxstyle="circle", fc='none')) #,pad={point[3]}
                plt.hlines(AB_distance/1000, data_dist_A.index.min(), data_dist_A.index.max(),
                    ls='--', lw=1, color='grey', label='Location B')
                #plt.text(data_dist_A.index.min()-data_split['diff'].dt.median()*3, AB_distance/1000, 'B')
                plt.legend(loc="upper left", frameon=False) #bbox_to_anchor=(1.01,1),

        file_save_msg(output_basename + '.pdf', "Split Tracks", end="\n")

        # Continue Corny for track id
        if cc(config,'clean','split_continue_track'):
            continue_track_id = gc(config,'clean','split_continue_track')
            if continue_track_id == 'each' or continue_track_id == 'all':
                print('# Selecting all %d tracks' % (len(Tracks)))

                dataset = []
                for i in range(len(Tracks)):
                    Track = Tracks.iloc[i].to_dict()
                    dataset.append( data[Track['start']:Track['end']] )
            
            elif continue_track_id == 'A->B':
                print('# Selecting %d tracks from A to B' % (len(Dir1)))
                dataset = []
                for i in Dir1.index:
                    Track = Dir1.iloc[i].to_dict()
                    dataset.append( data[Track['start']:Track['end']] )

            elif continue_track_id == 'B->A':
                print('# Selecting %d tracks from B to A' % (len(Dir2)))
                dataset = []
                for i in Dir2.index:
                    Track = Dir2.iloc[i].to_dict()
                    dataset.append( data[Track['start']:Track['end']] )

            elif int(continue_track_id) > 0:

                continue_track_id = int(continue_track_id)
                if continue_track_id > len(Tracks):
                    max_track = len(Tracks)
                    print('! Warning: Selected track (%d) is larger than the number of tracks (%d). Using track %d instead.' % (continue_track_id, len(Tracks), max_track))
                    continue_track_id = max_track
                    
                print('# Selecting track id %d' % (continue_track_id))
                #for track_id in range(len(Dir1_start)):
                Track = Tracks.iloc[continue_track_id-1].to_dict()
                #print('| Continuing with Track #%d: %s - %s...' % (continue_track_id, Track['start'], Track['end']))
                dataset = [ data[Track['start']:Track['end']] ]

            
    Nfinal_before_loop = Nfinal

    if len(dataset) > 1:
        track = 0
    else:
        track = continue_track_id

    for data in dataset:
        #if len(dataset) > 1:
        track += 1
        if len(dataset) > 1:
            print('# Continuing with Track #%d: %s - %s...' % (track, data.index.min(), data.index.max()))

        if len(dataset) > 1:
            J.report("general", "The data were split between locations {:} and {:} using a minimum radius of {:.1f} m and a minimal duration of {:.0f} seconds.",
                     gc(config,'clean','split_location_A'),
                     gc(config,'clean','split_location_B'),
                     gc(config,'clean','split_location_radius', dtype=float),
                     gc(config,'clean','split_min_duration', dtype=float))
            J.report("general",
                     "This is track {:.0f} out of {:.0f} total tracks.",
                     track, len(dataset))

        ####################################################
        ######  Scoring ####################################
        ####################################################

        scoring_column = None
        if cc(config,'clean','scoring_method'):

            from corny.smoothing import scoring
            scoring_method = gc(config,'clean','scoring_method')
            var = gc(config,'clean','scoring_var')
            
            J.report("clean",
                     "Data was cleaned using a scoring method following {:} on variable {:}.",
                     scoring_method, var)
            
            if var == 'neutrons':
                
                var = Nfinal
                if scoring_method == 'Altdorff et al. (2023)':

                    print('| Scoring after Altdorff et al. (2023)')
                    data = scoring(data,
                                var=var, var_type='neutrons',
                                filters=xsplit(gc(config,'clean','scoring_filters'), type=str),
                                scope=gc(config,'clean','scoring_scope', dtype=int),
                                std_factor=gc(config,'clean','scoring_std_factor', dtype=float),
                                max_score=gc(config,'clean','scoring_max_score', dtype=int)
                            )
                    
                    scoring_column = Nfinal+'_score_combined'
                    data[Nfinal+'_selected'] = data[Nfinal+'_score_selected']
                    data[Nfinal+'_selected'+err] = data[Nfinal+err]
                    Nfinal += '_selected'
                    report_N(data, Nfinal)

                elif scoring_method == 'Altdorff et al. (2023b)':

                    print('| Scoring after Altdorff et al. (2023b)')
                    scoring_radius = gc(config,'clean','scoring_radius', dtype=float)
                    scoring_radius_method = gc(config,'clean','scoring_radius_method')
                    prefix = Nfinal+'_score_radius_'

                    _data = data[['utmx','utmy',Nfinal]].copy()
                    _len_values = data[Nfinal].count()
                    num_neighbors = []
                    ic = 0
                    for i, row in _data.iterrows():
                        ic += 1
                        
                        _data['distance'] = np.sqrt((_data.utmx-row['utmx'])**2+(_data.utmy-row['utmy'])**2)

                        _query = (_data.distance < scoring_radius) & (~np.isnan(_data[Nfinal])) & (_data.distance > 0)
                        _d = _data[_query]
                        
                        if (_d[Nfinal].count() > 2):
                            if len(_d)==0:
                                continue
                            elif len(_d)==1:
                                _d['distance_weight'] = 1.0

                            elif cc(config,'clean','scoring_radius_method','inverse'):
                                _d.loc[i,'distance'] = 1.0
                                _d['distance_weight'] = 1/_d['distance']

                            elif cc(config,'clean','scoring_radius_method','W_r_approx'):
                                _d.loc[i,'distance'] = 0.1
                                _d['distance_weight'] = _d['distance']*Wr_approx(_d['distance'])

                            elif cc(config,'clean','scoring_radius_method','exp'):
                                _d['distance_weight'] = np.exp(-_d['distance']/127)

                            elif cc(config,'clean','scoring_radius_method','linear'):
                                _d['distance_weight'] = 1 - _d['distance']/scoring_radius

                            else:
                                _d['distance_weight'] = 1.0

                            # print(_d)
                            # data.loc[i, Nfinal+mov]
                            indices = ~np.isnan(_d[Nfinal])
                            mittel = np.average(_d[indices][Nfinal], weights=_d[indices]['distance_weight'])
                            stddev = np.sqrt(np.average((_d[indices][Nfinal]-mittel)**2, weights=_d[indices]['distance_weight'])) #
                            
                            data.loc[i, prefix+'minus_mean'] = (row[Nfinal] - mittel) < 0
                            data.loc[i, prefix+'SD'] = (row[Nfinal] - mittel)**2 > stddev**2
                            
                            print('| point %4.0f: neighbours=%4.0f, avg=%5.0f, std=%5.0f' % (ic,
                                _d[Nfinal].count(), mittel, stddev), end='\r')

                    print()
                    report_score = "minus_mean"
                    print("i Score %s: %.0f yes, %.0f no" % (report_score, data[prefix+report_score].value_counts()[0], data[prefix+report_score].value_counts()[1]))
                    report_score = "SD"
                    print("i Score %s: %.0f yes, %.0f no" % (report_score, data[prefix+report_score].value_counts()[0], data[prefix+report_score].value_counts()[1]))
                    
                    filters = ['minus_mean', 'SD']

                    # Calculate summary score
                    data[prefix+'combined'] = data[ [prefix+s for s in filters] ].sum(axis=1)
                    report_score = "combined"
                    print("i Score %s: %.0f yes, %.0f no" % (report_score, data[prefix+report_score].value_counts()[0], data[prefix+report_score].value_counts()[1]))
                    
                    # Select the chose one
                    data[prefix+'selected'] = data.loc[ data[prefix+'combined'] < gc(config,'clean','scoring_max_score', dtype=int), Nfinal]

                    print('i Scoring removed %d out of %d values.' % (
                        _len_values-data[prefix+'selected'].count(), _len_values))   
                    J.report("clean",
                             "Scoring removed {:.0f} out of {:.0f} values.",
                             _len_values-data[prefix+'selected'].count(),
                             _len_values)

                    scoring_column = prefix+'combined'
                    data[Nfinal+'_selected'] = data[prefix+'selected']

                    data[Nfinal+'_selected'+err] = data[Nfinal+err]
                    Nfinal += '_selected'
                    report_N(data, Nfinal)

        ####################################################
        ######  Aggregate ##################################
        ####################################################

        if cc(config,'clean','aggregate'):

            # keep a copy
            data_orig = data.copy()

            # Aggregate
            data = aggregate(data,  gc(config,'clean','aggregate'),
                             func = gc(config,'clean','aggregate_func', alt='mean'),
                             set_std = neutron_columns, adapt_err = neutron_columns,
                             verbose = True)
            # Report
            report_N(data, Nfinal)
            J.report("general",
                     "This data were aggregated to a new regular time resolution of {:}.",
                     gc(config,'clean','aggregate'))

        else:
            data_orig = data

        ####################################################
        ######  Smooth  ####################################
        ####################################################
        
        report_N(data, Nfinal)
        if cc(config,'clean','smooth'):
            smooth_window = gc(config,'clean','smooth', dtype=int)
            print('| Smooth by a moving average over %d time steps...' % smooth_window)

            data = moving_avg(data, Nfinal, window=smooth_window)
            Nfinal += mov
            report_N(data, Nfinal)

            #window = int()
            #rollobj = data[N+clean+pih].rolling(window, center=True)
            #data[N+clean+pih+mov]     = rollobj.mean()
            #data[N+clean+pih+mov+std] = rollobj.std()
            #data[N+clean+pih+mov+err] = data[N+clean+pih+err] * 1/np.sqrt(window)

            J.report("neutrons",
                    "Temporal smoothing was applied over {:.0f} time steps.",
                    smooth_window)

            if Nth:
                data = moving_avg(data, Nthfinal, window=config['clean']['smooth'])
                Nthfinal += mov

                #rollobj = data[N+th+clean+pih].rolling(window, center=True)
                #data[N+th+clean+pih+mov]     = rollobj.mean()
                #data[N+th+clean+pih+mov+std] = rollobj.std()
                #data[N+th+clean+pih+mov+err] = data[N+th+clean+pih+err] * 1/np.sqrt(window)

                J.report("neutronsth",
                    "Temporal smoothing was applied over {:.0f} time steps.",
                    smooth_window)
                
            if Muons:
                data[mufinal+mov] = data[mufinal].rolling(smooth_window, center=True).mean()
                mufinal += mov
                J.report("muons",
                    "Temporal smoothing was applied over {:.0f} time steps.",
                    smooth_window)


        if cc(config,'clean','smooth_radius'):

            smooth_radius = gc(config,'clean','smooth_radius', dtype=float)
            print('| Smooth by radius over '+str(smooth_radius)+' m...')

            _data = data[['utmx','utmy',Nfinal,Nfinal+err]].copy()
            num_neighbors = []
            #print('  length: %.0f, ' % len(_data), end='')
            ic = 0
            for i, row in _data.iterrows():
                #if ic % 1000 == 0:
                #    print('%i ' % ic, end='')
                ic += 1
                _data['distance'] = np.sqrt((_data.utmx-row['utmx'])**2+(_data.utmy-row['utmy'])**2)

                _query = (_data.distance < smooth_radius) & (~np.isnan(_data[Nfinal]))
                _d = _data[_query]

                if len(_d)==0:
                    continue
                elif len(_d)==1:
                    _d['distance_weight'] = 1.0

                elif cc(config,'clean','smooth_radius_method','inverse'):
                    _d.loc[i,'distance'] = 1.0
                    _d['distance_weight'] = 1/_d['distance']

                elif cc(config,'clean','smooth_radius_method','W_r_approx'):
                    _d.loc[i,'distance'] = 0.1
                    _d['distance_weight'] = _d['distance']*Wr_approx(_d['distance'])

                elif cc(config,'clean','smooth_radius_method','exp'):
                    _d['distance_weight'] = np.exp(-_d['distance']/127)

                elif cc(config,'clean','smooth_radius_method','linear'):
                    _d['distance_weight'] = 1 - _d['distance']/smooth_radius

                else:
                    _d['distance_weight'] = 1.0

                #print(_d)
                data.loc[i, Nfinal+mov]     = np.average(_d[Nfinal], weights=_d['distance_weight'])
                data.loc[i, Nfinal+mov+err] = np.average(_d[Nfinal+err], weights=_d['distance_weight']) / np.sqrt(_d[Nfinal+err].count())
                num_neighbors.append(_d[Nfinal].count())

            print('i Smooth by radius over %.0f m (median %.0f neighbors per data point).' % (smooth_radius, np.nanmedian(num_neighbors)))
            J.report("neutrons",
                    "Spatial smoothing was applied using {:} weighing in a radius of {:.0f} m (median {:.0f} neighbors per data point).",
                    gc(config,'clean','smooth_radius_method'), smooth_radius, np.nanmedian(num_neighbors))
            Nfinal += mov
            report_N(data, Nfinal)

            if Nth:
                _data = data[['utmx','utmy',Nthfinal,Nthfinal+err]].copy()
                num_neighbors = []
                #print('  length: %.0f, ' % len(_data), end='')
                ic = 0
                for i, row in _data.iterrows():
                    #if ic % 1000 == 0:
                    #    print('%i ' % ic, end='')
                    ic += 1
                    _data['distance'] = np.sqrt((_data.utmx-row['utmx'])**2+(_data.utmy-row['utmy'])**2)

                    _query = (_data.distance < smooth_radius) & (~np.isnan(_data[Nthfinal]))
                    _d = _data[_query]

                    if len(_d)==0:
                        continue
                    elif len(_d)==1:
                        _d['distance_weight'] = 1.0

                    elif cc(config,'clean','smooth_radius_method','inverse'):
                        _d.loc[i,'distance'] = 1.0
                        _d['distance_weight'] = 1/_d['distance']

                    elif cc(config,'clean','smooth_radius_method','W_r_approx'):
                        _d.loc[i,'distance'] = 0.1
                        _d['distance_weight'] = _d['distance']*Wr_approx(_d['distance'])

                    elif cc(config,'clean','smooth_radius_method','exp'):
                        _d['distance_weight'] = np.exp(-_d['distance']/127)

                    elif cc(config,'clean','smooth_radius_method','linear'):
                        _d['distance_weight'] = 1 - _d['distance']/smooth_radius

                    else:
                        _d['distance_weight'] = 1.0

                    #print(_d)
                    data.loc[i, Nthfinal+mov]     = np.average(_d[Nthfinal], weights=_d['distance_weight'])
                    data.loc[i, Nthfinal+mov+err] = np.average(_d[Nthfinal+err], weights=_d['distance_weight']) / np.sqrt(_d[Nthfinal+err].count())
                    num_neighbors.append(_d[Nthfinal].count())

                print('i Smooth by radius over %.0f m (median %.0f neighbors per data point).' % (smooth_radius, np.nanmedian(num_neighbors)))
                Nthfinal += mov
                J.report("neutronsth",
                    "Spatial smoothing was applied using {:} weighing in a radius of {:.0f} m (median {:.0f} neighbors per data point).",
                    gc(config,'clean','smooth_radius_method'), smooth_radius, np.nanmedian(num_neighbors))

        if not Nfinal.endswith(mov):
            mov = ''

        if Nth:
            J.report("neutronsratio",
                 "The average ratio of thermal over epithermal neutrons was {:.2f} ±{:.2f}.",
                 (data[Nthfinal]/data[Nfinal]).mean(),
                 (data[Nthfinal]/data[Nfinal]).std())

        ####################################################
        ######  Footprint length ###########################
        ####################################################

        if 'Speed_kmh' in data.columns:
            tres = data.index.to_series().dropna().diff().median().total_seconds()
            data['footprint_length'] = data['Speed_kmh']/3.6 * tres
            average_length = data.loc[data['footprint_length'] > 2.0, 'footprint_length'].mean()
            
            print('i   Average footprint length is %.0f m.' % average_length)
            J.report("general",
                    "While moving, average distance between two data points was {:.0f} m.",
                    average_length)

        ##JJ smooth with nearest neighbors
        if config['clean']['number_locations']:
            print('| Smooth data with '+config['clean']['number_locations']+' nearest neighbors.')
            data = data.dropna(subset=[lat, lon]) # drop rows without Latitude or Longitude information
            maxdist, RecNums = Neighbours(data[lat], data[lon], int(config['clean']['number_locations']), data)

            data['maxdistance'] = maxdist
            data['neighbors_RecordNum'] = RecNums.tolist()

            test = []
            Recind = []
            res = []
            result = []
            resTh = []
            result_Th = []

            for i in range(len(data)): # store the nearest recordnumber in neighbors
                test = data['RecordNum'].iloc[data['neighbors_RecordNum'][i]].values
                Recind.append(test.tolist())

            data['neighbors_RecordNum'] = Recind

            for i in range(len(data)): # calculate average N from neighbors index
                res = data[Nfinal][data['RecordNum'].isin(data['neighbors_RecordNum'][i])].mean()
                result.append(res)
                if Nth:
                    resTh = data[Nthfinal][data['RecordNum'].isin(data['neighbors_RecordNum'][i])].mean()
                    result_Th.append(resTh)

            Nfinal += '_neighbors'
            Nthfinal += '_neighbors'
            data[Nfinal] = result
            data[Nthfinal] = result_Th


        ####################################################
        ######  Select Period  #############################
        ####################################################

        # TODO: Add clever selection of date when only time is given
        data = cut_period_to(data, gc(config,'clean','start'), gc(config,'clean','end'))
        data_orig = cut_period_to(data_orig, gc(config,'clean','start'), gc(config,'clean','end'))

    #    if config['clean']['start'] or config['clean']['end']:
    #        if config['clean']['start']:
    #            start = dateutil.parser.parse(config['clean']['start']).replace(tzinfo=pytz.UTC)
    #        else:
    #            start = data.index.min()
    #        if config['clean']['end']:
    #            end = dateutil.parser.parse(config['clean']['end']).replace(tzinfo=pytz.UTC)
    #        else:
    #            end = data.index.max()
    #
    #        # cut
    #        data = data.loc[start:end]
    #        # cut also the raw data
    #        data_orig = data_orig.loc[start:end]

        # guess an original data resolution to plot. #TODO Make this configurable later.
        tres_temp = data_orig.index.to_series().dropna().diff().median().total_seconds()
        #data_orig = aggregate(data_orig,  gc(config,'clean','aggregate_minor_vis', '1H'),
        #                     func = gc(config,'clean','aggregate_func', alt='mean'))

        if len(data) <= 1:
            print('! No data remains. Did you cut out an empty period?')
            
        start_str = data.index.min().strftime('%Y%m%d%H')
        end_str = data.index.max().strftime('%Y%m%d%H')
        end_str = '-'+end_str if start_str != end_str else ''
        track_str = '-track%d' % track if len(dataset) > 1 else ''
        output_basename = os.path.join(config['output']['out_path'],'') + config['output']['out_basename'] + '-' \
                          + start_str + end_str + track_str #config['input']['prefix']
        output_basename_notrack = os.path.join(config['output']['out_path'],'') + config['output']['out_basename'] + '-' \
                  + start_str + end_str

        print('i Data cut from (%s) to (%s)' % (
            data.index.min().strftime('%Y-%m-%d %H:%M'),
            data.index.max().strftime('%Y-%m-%d %H:%M')
            ))
        
        J.report("general",
                 "After basic processing, the data was cut to the time period from {:} to {:}.",
                 data.index.min().strftime('%Y-%m-%d %H:%M %Z'),
                 data.index.max().strftime('%Y-%m-%d %H:%M %Z'))

        ####################################################
        ######  Gridding  ##################################
        ####################################################

        gridding = False
        if cc(config,'clean','grid_master_file'):
            gridding = True
            print('| Gridding on master grid...')
            from corny.smoothing import nearby_avg, filter_nearby

            grid_master_file = gc(config,'clean','grid_master_file')
            if not os.path.isfile(grid_master_file):
                print('! Cannot find grid_master_file: %s' % grid_master_file)
            else:
                M = pandas.read_csv(grid_master_file, skipinitialspace=True)
                #M['distance_x'] = M.iloc[:, 0].diff()
                #M['distance_y'] = M.iloc[:, 1].diff()
                #M['distance'] = np.sqrt(M.distance_x**2 + M.distance_y**2)

                time_header = data.index.mean().strftime('T_%Y-%m-%d_%H:%M')
                
                for var in ['nearby_points','avg_neutrons','avg_neutrons_err','bulk_density','air_humidity','lattice_water','organic_water','Pressure','Temperature','Vbat','T1_degC','RH1','P1_mb','Alt','KpH','NM','Scoring']:
                    M[var] = np.nan
                    
                nearby_avg_kw = dict(
                    max_radius = gc(config,'clean','grid_smooth_radius', dtype=float),
                    method = gc(config,'clean','grid_smooth_method'))

                data_numeric = data.apply(pandas.to_numeric, errors='coerce')

                #M2['data_points'] = np.nan
                #if track == 1:
                #    print(' id  L        N,     Nerr,       bd,        h,       lw,       ow,        P,        T')
                with Progressbar(len(M[['utmx','utmy']]), '| Grid points...') as Pb:
                    i = 0
                    for row in M[['utmx','utmy']].copy().itertuples():
                        i += 1
                        #print(row)
                        #print(row[1], data.utmx.median(), row[2], data.utmy.median())
                        data_numeric['M_distance'] = np.sqrt((row[1]-data_numeric.utmx)**2 + (row[2]-data_numeric.utmy)**2)
                        #nearby = R1.loc[R1.distance_to_M < 20, ['distance_to_M', 'moisture_vol']]
                        #data_points = len(nearby)
                        #data['M_distance'].to_csv('test.csv')
                        nearby_data = filter_nearby(data_numeric, 'M_distance',
                            max_radius=gc(config,'clean','grid_smooth_radius', dtype=float),
                            max_points=gc(config,'clean','grid_smooth_max_points', dtype=float),
                            or_nearest=True)

                        M.at[row[0],'avg_neutrons']  = nearby_avg(nearby_data, 'M_distance', Nfinal, **nearby_avg_kw)
                        M.at[row[0],'avg_neutrons_err'] = nearby_avg(nearby_data, 'M_distance', Nfinal+err, err_sqrt=True, **nearby_avg_kw)
                        M.at[row[0],'bulk_density']  = nearby_avg(nearby_data, 'M_distance', 'bd', **nearby_avg_kw)
                        M.at[row[0],'air_humidity']  = nearby_avg(nearby_data, 'M_distance', ah, **nearby_avg_kw)
                        M.at[row[0],'lattice_water'] = nearby_avg(nearby_data, 'M_distance', 'lw', **nearby_avg_kw)
                        M.at[row[0],'organic_water'] = nearby_avg(nearby_data, 'M_distance', 'owe', **nearby_avg_kw)
                        M.at[row[0],'Pressure']      = nearby_avg(nearby_data, 'M_distance', p, **nearby_avg_kw)
                        M.at[row[0],'Temperature']   = nearby_avg(nearby_data, 'M_distance', T, **nearby_avg_kw)

                        if scoring_column in data:
                            M.at[row[0],'Scoring'] = nearby_avg(nearby_data, 'M_distance', scoring_column, **nearby_avg_kw)

                        # Other variables
                        for var in ['Vbat','T1_degC','RH1','P1_mb','Alt','KpH','NM']:
                            M.at[row[0], var] = nearby_avg(nearby_data, 'M_distance', var, **nearby_avg_kw)
                        
                        M.at[row[0],'nearby_points'] = len(nearby_data)
                        #M2.at[row[0],'data_points'] = data_points
                        #M.at[row[0],'avg_neutrons'] = avg_value
                        #print(row[0], data_points, avg_value, end='\r')

                        #if track == 1:
                        #    print('%3d' % row[0], '%2d' % len(nearby_data), ', '.join(['%8.2f' % x for x in M.iloc[row[0]][['avg_neutrons',
                        #        'avg_neutrons_err','bulk_density','air_humidity','lattice_water','organic_water',
                        #        'Pressure','Temperature']].values]))
                        #print('nan values for grid: %d' % len(M[np.isnan(M['avg_neutrons'])]))
                        Pb.update('%d' % i)

                J.report("general",
                        "Then, all data points were reprojected on a given master grid with {:.0f} grid cells." \
                            + "The {:.0f} nearest data points were averaged within a radius of {:.1f} m using {:} weighting.",
                            len(M),
                            gc(config,'clean','grid_smooth_max_points', dtype=float),
                            gc(config,'clean','grid_smooth_radius', dtype=float),
                            gc(config,'clean','grid_smooth_method'))
        
        elif cc(config,'clean','grid_bbox'):

            gridding = True
            print('| Gridding on a regular grid...')
            from corny.smoothing import RegularGrid

            grid_bbox = gc(config, "clean", "grid_bbox", dtype=list, types=float)
            RG = RegularGrid(
                grid_bbox,
                gc(config,'clean','grid_resolution', alt=1, dtype=float))
            
            data["grid_id"] = RG.get_node_id_from_pandas_xy(data[["utmx","utmy"]])

            data_numeric = data.copy()
            if "ll" in data.columns:
                data_numeric = data_numeric.drop("ll", axis=1)
            if "road" in data.columns:
                data_numeric = data_numeric.drop("road", axis=1) 
            if "luse_str" in data.columns:
                data_numeric = data_numeric.drop("luse_str", axis=1)    
            data_numeric = data_numeric.apply(pandas.to_numeric, errors='ignore')

            data_grouped = data_numeric.groupby("grid_id")
            data_avg = data_grouped.mean()
            data_sum = data_grouped.sum()
            data_avg["num"] = data_grouped.count()["utmx"].values
            data_avg["num"] = data_avg["num"].fillna(1)
            M = data_avg[["NM","Vbat","Alt"]]
            M["nearby_points"] = data_avg["num"]
            M["avg_neutrons"] = data_avg[Nfinal] # is the sum/3 
            M["avg_neutrons_err"] = np.sqrt(data_sum[Nfinal]) / data_avg["num"]
            M["bulk_density"] = data_avg["bd"]
            M["air_humidity"] = data_avg[ah]
            M["lattice_water"] = data_avg["lw"]
            M["organic_water"] = data_avg["owe"]
            M["Pressure"] = data_avg[p]
            M["Temperature"] = data_avg[T]
            M["x"] = data_avg["utmx"]
            M["y"] = data_avg["utmy"]

            J.report("general",
                "Then, all data points were reprojected on a regular grid with {:.0f} grid cells.",
                    len(M))


        ####################################################
        ######  Soil Moisture  #############################
        ####################################################
        print('[Conversion]')

        if config['conversion']['N2sm_method'] != 'None':

            # Calibrate
            N0 = config['conversion']['N0']
            calibration = False
            if not N0:
                print('| Calibration... ', end='')
                calibration = True
                start = dateutil.parser.parse(config['conversion']['campaign_start']).replace(tzinfo=pytz.UTC)
                end = dateutil.parser.parse(config['conversion']['campaign_end']).replace(tzinfo=pytz.UTC)
                datac = data.copy()
                datac = datac.loc[start:end]
                Nc = np.nanmean(datac[Nfinal])
                Nc_err = np.nanstd(datac[Nfinal])
                print('(sm=%.3f, N=%.0f, bd=%.3f, lw=%.3f, owe=%.3f)' % (float(config['conversion']['measured_sm']), Nc, data.bd.mean(), data.lw.mean(), data.owe.mean()))
                N0     = Calibrate_N0_Desilets(Nc, float(config['conversion']['measured_sm']), bd=data.bd.mean(), lw=data.lw.mean(), owe=data.owe.mean(),
                        a0=float(config['conversion']['a0']), a1=float(config['conversion']['a1']), a2=float(config['conversion']['a2']))       #                Nc/(float(config['conversion']['a0']) / (float(config['conversion']['measured_sm'])/float(config['conversion']['bulk_density']) + float(config['conversion']['a2']) + float(config['conversion']['lattice_water']) + float(config['conversion']['soil_org_carbon'])*0.556) + float(config['conversion']['a1']))
                N0_err = Calibrate_N0_Desilets(Nc+Nc_err, float(config['conversion']['measured_sm']), bd=data.bd.mean(), lw=data.lw.mean(), owe=data.owe.mean(),
                        a0=float(config['conversion']['a0']), a1=float(config['conversion']['a1']), a2=float(config['conversion']['a2'])) - N0  #/(float(config['conversion']['a0']) / (float(config['conversion']['measured_sm'])/float(config['conversion']['bulk_density']) + float(config['conversion']['a2']) + float(config['conversion']['lattice_water']) + float(config['conversion']['soil_org_carbon'])*0.556) + float(config['conversion']['a1']))

                print('i   N0 = %.0f +/- %.0f cph' % (N0, abs(N0_err))) # todo: make a report using N0 as column
                J.report("sm",
                         "Calibration of N0={:.0f} ±{:.0f} cph was performed following Desilets et al. (2010)" \
                            + "on {:} using measured soil moisture of {:.3f} m³/m³ and average ~{:.3f} kg/m³ bulk density" \
                            + "~{:.3f} g/g lattice water, and ~{:.3f} g/g organic water equivalent.",
                            N0, abs(N0_err), start.strftime("%Y-%m-%d"),
                            gc(config,'conversion','measured_sm', dtype=float),
                            data["bd"].mean(), data["lw"].mean(), data["owe"].mean())
            
            else:
                N0_orig = float(N0)
                if cc(config, 'input', 'ignore_neutron_columns'):
                    len_neutron_columns = len(xsplit(config['input']['neutron_columns']))
                    len_ignored_columns = len(xsplit(config['input']['ignore_neutron_columns']))
                    N0 = N0_orig * (1 - len_ignored_columns / len_neutron_columns)
                    print('i Initial N0 (%.0f) adapted on active tubes is: %.0f' % (N0_orig, N0))
                    J.report("sm",
                         "The initial N0 ({:.0f}) was adapted to the number of active tubes (N0'={:.0f})",
                            N0_orig, N0)
                else:
                    N0 = N0_orig
                

            # Conversion
            sm = config['conversion']['new_moisture_column']
            sm_neighbors = sm+'_neighbors'

            #while True:
            smv = sm+'_vol'
            smg = sm+'_grv'

            print('| Conversion to water content (N0=%i, bd=%.3f, lw=%.3f, owe=%.3f)... ' % (N0, data.bd.mean(), data.lw.mean(), data.owe.mean()))

            if cc(config,'conversion','N2sm_method','Desilets et al. (2010)'):

                parameterset = [
                    gc(config,'conversion','a0', dtype=float),
                    gc(config,'conversion','a1', dtype=float),
                    gc(config,'conversion','a2', dtype=float)]
                data[smg] = N2SM_Desilets(data[Nfinal], N0, bd=1, lw=data.lw, owe=data.owe,
                            a0=parameterset[0], a1=parameterset[1], a2=parameterset[2])
                data[smv] = N2SM_Desilets(data[Nfinal], N0, bd=data.bd, lw=data.lw, owe=data.owe,
                            a0=parameterset[0], a1=parameterset[1], a2=parameterset[2])
                # same for orig
                data_orig[smv] = N2SM_Desilets(data_orig[N+clean+pih], N0, bd=data_orig.bd, lw=data_orig.lw, owe=data_orig.owe,
                            a0=parameterset[0], a1=parameterset[1], a2=parameterset[2])

                if gridding:
                    M_bd   = M['BD_smooth']                  if 'BD_smooth'   in M.columns else M['bulk_density']
                    M_lw   = lw_from_clay( M['Clay_smooth']) if 'Clay_smooth' in M.columns else M['lattice_water']
                    M_owe  = owe_from_corg(M['SOC_smooth'] ) if 'SOC_smooth'  in M.columns else M['organic_water']
                    M_N    = M['avg_neutrons']*M['Veg_Corr'] if 'Veg_Corr'    in M.columns else M['avg_neutrons']
                        
                    M['soil_moisture'] = N2SM_Desilets(M_N, N0, bd=M_bd, lw=M_lw, owe=M_owe,
                        a0=parameterset[0], a1=parameterset[1], a2=parameterset[2])


            elif cc(config,'conversion','N2sm_method','Schmidt et al. (2021)'):
                if cc(config, 'conversion','Schmidt_parameterset'):
                    parameterset = gc(config, 'conversion','Schmidt_parameterset')
                else:
                    parameterset = 'Mar21_mcnp_drf'
                
                use_ah_column = ah
                if cc(config, 'correction', 'humidity_method', 'None'):
                    data['ah_const'] = 5
                    data_orig['ah_const'] = 5
                    use_ah_column = 'ah_const'
                    
                data[smv] = N2SM_Schmidt(data, Nfinal, use_ah_column, N0, bdstr='bd', lwstr='lw', owestr='owe', method=parameterset)
                data[smg] = data[smv]/data.bd
                # same for orig
                print('.')
                data_orig[smv] = N2SM_Schmidt(data_orig, N+clean+pih, use_ah_column, N0, bdstr='bd', lwstr='lw', owestr='owe', method=parameterset)

                if gridding:
                    M['soil_moisture'] = N2SM_Schmidt(M, 'avg_neutrons', 'air_humidity', N0,
                        bdstr='bulk_density', lwstr='lattice_water', owestr='organic_water',
                        method=parameterset)
                
            J.report("sm",
                "Soil moisture was derived following {:} using the parameterset {:}.",
                gc(config,'conversion','N2sm_method'), parameterset)


            data[smg+err]        = dtheta_stdx(data[Nfinal], data[Nfinal+err], N0, a0=float(config['conversion']['a0']), a1=float(config['conversion']['a1']), a2=float(config['conversion']['a2']))
            data[smg+err+'_low'] = dtheta_low(data[Nfinal], data[Nfinal+err], N0, a0=float(config['conversion']['a0']), a1=float(config['conversion']['a1']), a2=float(config['conversion']['a2']))
            data[smg+err+'_upp'] = dtheta_upp(data[Nfinal], data[Nfinal+err], N0, a0=float(config['conversion']['a0']), a1=float(config['conversion']['a1']), a2=float(config['conversion']['a2']))
            #report_SM(data, smg)

            data['bd_err'] = 0
            if 'bulk_density_err' in config['conversion']:
                data['bd_err'] = float(config['conversion']['bulk_density_err'])
            data[smv+err]        = errpropag_factor(data, 'bd', smg, err=err)                  #data.bd * data[smg+err]        + data[smg]        *
            data[smv+err+'_low'] = errpropag_factor(data, 'bd', smg, err=err, err2=err+'_low') #data.bd * data[smg+err+'_low'] + data[smg+err+'_low'] * float(config['conversion']['bulk_density_err'])
            data[smv+err+'_upp'] = errpropag_factor(data, 'bd', smg, err=err, err2=err+'_upp') #data.bd * data[smg+err+'_upp'] + data[smg+err+'_upp'] * float(config['conversion']['bulk_density_err'])
            if data[smv+err+'_low'].mean() > 0:
                data[smv+err+'_low'] *= -1
            #report_SM(data, smv)

            if gridding:
                M['soil_moisture_err'] = dtheta_stdx(M['avg_neutrons'], M['avg_neutrons_err'], N0,
                    a0=float(config['conversion']['a0']), a1=float(config['conversion']['a1']), a2=float(config['conversion']['a2']))
                M['soil_moisture_err_low'] = dtheta_low(M['avg_neutrons'], M['avg_neutrons_err'], N0,
                    a0=float(config['conversion']['a0']), a1=float(config['conversion']['a1']), a2=float(config['conversion']['a2']))
                M['soil_moisture_err_upp'] = dtheta_upp(M['avg_neutrons'], M['avg_neutrons_err'], N0,
                    a0=float(config['conversion']['a0']), a1=float(config['conversion']['a1']), a2=float(config['conversion']['a2']))
                #report_SM(data, smg)

                if M['soil_moisture_err_low'].mean() > 0:
                    M['soil_moisture_err_low'] *= -1

                M.loc[M['soil_moisture'] <= 0, 'soil_moisture'] = np.nan
                M.loc[M['soil_moisture'] >= 1, 'soil_moisture'] = np.nan
                M.loc[M['soil_moisture_err'] <= 0, 'soil_moisture_err'] = np.nan
                M.loc[M['soil_moisture_err'] >= 1, 'soil_moisture_err'] = np.nan
                M.loc[M['soil_moisture_err_low'] >= 0, 'soil_moisture_err_low'] = np.nan
                M.loc[M['soil_moisture_err_low'] <=-1, 'soil_moisture_err_low'] = np.nan
                M.loc[M['soil_moisture_err_upp'] <= 0, 'soil_moisture_err_upp'] = np.nan
                M.loc[M['soil_moisture_err_upp'] >= 1, 'soil_moisture_err_upp'] = np.nan

        ####################################################
        ######  Clean soil moisture  #######################
        ####################################################
            
            ####################################################
            ######  Scoring ####################################
            ####################################################

            if cc(config,'clean','scoring_method'):

                from corny.smoothing import scoring
                scoring_method = gc(config,'clean','scoring_method')
                var = gc(config,'clean','scoring_var')
                
                if var == 'moisture':
                    if scoring_method == 'Altdorff et al. (2023)':
                        print('| Scoring after Altdorff et al. (2023)')

                        for var in [smg, smv]:
                            data = scoring(data,
                                        var=var, var_type='moisture',
                                        filters=xsplit(gc(config,'clean','scoring_filters'), type=str),
                                        scope=gc(config,'clean','scoring_scope', dtype=int),
                                        std_factor=gc(config,'clean','scoring_std_factor', dtype=float),
                                        max_score=gc(config,'clean','scoring_max_score', dtype=int)
                                    )
                            data[var]     = data[var+'_score_selected']
                        
                        scoring_column = smg+'_score_combined'
                        report_SM(data, smg, units='grv.%%')
                        report_SM(data, smv, units='vol.%%')
                        
                        if gridding:
                            for row in M[['utmx','utmy']].copy().itertuples():
                                data['M_distance'] = np.sqrt((row[1]-data.utmx)**2 + (row[2]-data.utmy)**2)
                                nearby_data = filter_nearby(data, 'M_distance',
                                    max_radius=gc(config,'clean','grid_smooth_radius', dtype=float),
                                    max_points=gc(config,'clean','grid_smooth_max_points', dtype=float),
                                    or_nearest=True)
                            
                                M.at[row[0],'Scoring'] = nearby_avg(nearby_data,
                                    'M_distance', scoring_column,
                                    max_radius = gc(config,'clean','grid_smooth_radius', dtype=float),
                                    #method='equal',
                                    #func='median'
                                    )

                    elif scoring_method == 'Altdorff et al. (2023b)':

                        print('| Scoring after Altdorff et al. (2023b)')
                        scoring_radius = gc(config,'clean','scoring_radius', dtype=float)
                        scoring_radius_method = gc(config,'clean','scoring_radius_method')

                        for var in [smg, smv]:
                            prefix = var+'_score_radius_'

                            _data = data[['utmx','utmy',var]].copy()
                            _len_values = data[var].count()
                            num_neighbors = []
                            ic = 0
                            for i, row in _data.iterrows():
                                if ic % 1000 == 0:
                                    print('%i ' % ic, end='')
                                ic += 1
                                
                                _data['distance'] = np.sqrt((_data.utmx-row['utmx'])**2+(_data.utmy-row['utmy'])**2)

                                _query = (_data.distance < scoring_radius) & (~np.isnan(_data[var])) & (_data.distance > 0)
                                _d = _data[_query]
                                
                                if (_d[var].count() > 2):

                                    if len(_d)==0:
                                        continue
                                    elif len(_d)==1:
                                        _d['distance_weight'] = 1.0

                                    elif cc(config,'clean','scoring_radius_method','inverse'):
                                        _d.loc[i,'distance'] = 1.0
                                        _d['distance_weight'] = 1/_d['distance']

                                    elif cc(config,'clean','scoring_radius_method','W_r_approx'):
                                        _d.loc[i,'distance'] = 0.1
                                        _d['distance_weight'] = _d['distance']*Wr_approx(_d['distance'])

                                    elif cc(config,'clean','scoring_radius_method','exp'):
                                        _d['distance_weight'] = np.exp(-_d['distance']/127)

                                    elif cc(config,'clean','scoring_radius_method','linear'):
                                        _d['distance_weight'] = 1 - _d['distance']/scoring_radius

                                    else:
                                        _d['distance_weight'] = 1.0

                                    # print(_d)
                                    # data.loc[i, Nfinal+mov]
                                    mittel = np.average(_d[var], weights=_d['distance_weight'])
                                    stddev = np.sqrt(np.average((_d[var]-mittel)**2, weights=_d['distance_weight']))
                                    
                                    data.loc[i, prefix+'minus_mean'] = (row[var] - mittel) > 0
                                    data.loc[i, prefix+'SD'] = (row[var] - mittel)**2 > stddev

                            filters = ['minus_mean', 'SD']

                            # Calculate summary score
                            data[prefix+'combined'] = data[ [prefix+s for s in filters] ].sum(axis=1)
                            
                            # Select the chose one
                            data[prefix+'selected'] = data.loc[ data[prefix+'combined'] < gc(config,'clean','scoring_max_score', dtype=int), var]

                            print('i Scoring removed %d out of %d values.' % (
                                _len_values-data[prefix+'selected'].count(), _len_values))       

            #######

            validlinesg = len(data[smg].dropna())
            validlinesv = len(data[smv].dropna())
            (a,b) = xsplit(config['clean']['sm_range'], type=float)

            if config['clean']['invalid_data'] == 'drop':
                data = data.query("@"+smg+" > %s and @"+smg+" < %s" % (a,b))
                data = data.query("@"+smv+" > %s and @"+smv+" < %s" % (a,b))
            else:
                data.loc[(data[smg] < a) | (data[smg] > b), smg+err] = np.nan
                data.loc[(data[smg] < a) | (data[smg] > b), smg+err+'_low'] = np.nan
                data.loc[(data[smg] < a) | (data[smg] > b), smg+err+'_upp'] = np.nan

                data.loc[(data[smg] < a) | (data[smg] > b), smg] = np.nan
                data.loc[(data[smg]-data[smg+err] < a), smg+err] = data[smg]
                data.loc[(data[smg]+data[smg+err] > 1), smg+err] = 1-data[smg]
                data.loc[(data[smg]+data[smg+err+'_low'] < a), smg+err+'_low'] = -data[smg]
                data.loc[(data[smg]+data[smg+err+'_upp'] > b), smg+err+'_upp'] = 1-data[smg]

                data.loc[(data[smv] < a) | (data[smv] > b), smv+err] = np.nan
                data.loc[(data[smv] < a) | (data[smv] > b), smv+err+'_low'] = np.nan
                data.loc[(data[smv] < a) | (data[smv] > b), smv+err+'_upp'] = np.nan
                data.loc[(data[smv] < a) | (data[smv] > b), smv] = np.nan
                data.loc[(data[smv]-data[smv+err] < a), smv+err] = data[smv]
                data.loc[(data[smv]+data[smv+err] > b), smv+err] = 1-data[smv]
                data.loc[(data[smv]+data[smv+err+'_low'] < a), smv+err+'_low'] = -data[smv]
                data.loc[(data[smv]+data[smv+err+'_upp'] > b), smv+err+'_upp'] = 1-data[smv]
                #if sm_neighbors in data.columns:
                #    data.loc[(data[sm_neighbors] < a) | (data[sm_neighbors] > b), sm_neighbors] = np.nan

            data[sm] = data[smv]
            data[sm+'_%'] = data[sm] * 100

        #print('dropped %i smg values out of %i' % (validlinesg-len(data[smg].dropna()), validlinesg))
        print('i Dropped %i out of %i values (out of range) from %s.' % (validlinesv-len(data[smv].dropna()), validlinesv, smv))

        # data = data.dropna(subset=[smg,smv])
        report_SM(data.dropna(subset=[smg]), smg, units='grv.%%')
        report_SM(data.dropna(subset=[smv]), smv, units='vol.%%')
        #print('%.3f +/- %.3f -%.3f +%.3f' % (data[smv].mean(), data[smv+err].mean(), data[smv+err+'_low'].mean(), data[smv+err+'_upp'].mean()))

        J.report("sm",
             "Cleaning dropped {:.0f} unrealistic soil moisture values beyond {:.2f}—{:.2f} g/g or m³/m³.",
             validlinesv-len(data[smv].dropna()), a, b)
        J.report("sm",
             "Median soil moisture values were {:.3f} {:+.3f}{:+.3f} g/g or {:.3f} {:+.3f}{:+.3f} m³/m³.",
             data[smg].median(), data[smg+err+"_upp"].median(), data[smg+err+"_low"].median(),
             data[smv].median(), data[smv+err+"_upp"].median(), data[smv+err+"_low"].median())

        terminal_plot(data.dropna(subset=[smg,smv])[smv]*100, lines=True, y_gridlines=[50], x_gridlines=[], y_min=0, y_max=100)

        #report_mmsm(data[Nfinal])
        #report_mmsm(data[smg])
        #report_mmsm(data[smv])

        ####################################################
        ######  Depth  #####################################
        ####################################################

        data['footprint_depth'] = D86(data[smv], data.bd)
        data['footprint_depth'] = data['footprint_depth'].replace(np.inf, np.nan)
        print('i   Footprint depth  D = %3.0f +/- %2.0f cm.' % (data.footprint_depth.dropna().mean(), data.footprint_depth.dropna().std()))
        J.report("footprint",
                 "The average measurement depth of the sensor was {:.0f} ±{:.0f} cm.",
                 data.footprint_depth.dropna().mean(), data.footprint_depth.dropna().std())
        
        if gridding:
            M['footprint_depth'] = D86(M['soil_moisture'], M['bulk_density'])
            M['footprint_depth'] = M['footprint_depth'].replace(np.inf, np.nan)

        ####################################################
        ######  Footprint  #################################
        ####################################################

        if gc(config, "conversion", "estimate_footprint") == "no":
            data['footprint_radius'] = np.nan
            data['footprint_volume'] = np.nan
            if gridding:
                M['footprint_radius'] = np.nan
        else:
            footprint_data = np.loadtxt(app_file_path('corny/footprint_radius.csv'))
            # footprint_data = np.loadtxt(os.path.join(pasdy_path ,'corny/footprint_radius.csv'))
            def get_footprint(sm, h, p):
                if np.isnan(sm) or np.isnan(h) or np.isnan(p): return(np.nan)
                if sm <  0.01: sm =  0.01
                if sm >  0.49: sm =  0.49
                if h  > 30   : h  = 29
                #print(sm, h, p, int(round(100*sm)), int(round(h)))
                return(footprint_data[int(round(100*sm))][int(round(h))] * 0.4922/(0.86-np.exp(-p/1013.25)))

            data['footprint_radius'] = data.apply(lambda row: get_footprint( row[smv], row[ah], row[p] ), axis=1)
            print('i   Footprint radius R = %3.0f +/- %2.0f m.' % (data.footprint_radius.dropna().mean(), data.footprint_radius.dropna().std()))

            data['footprint_volume'] = (data['footprint_depth']+D86(data[smv], data.bd,data['footprint_radius']))*0.01*0.47*data['footprint_radius']**2*3.141 /1000
            print('i   Footprint volume V = %3.0f +/- %2.0f x10³ m³.' % (data.footprint_volume.dropna().mean(), data.footprint_volume.dropna().std()))
            if gridding:
                M['footprint_radius'] = M.apply(lambda row: get_footprint( row['soil_moisture'], row['air_humidity'], row['Pressure'] ), axis=1)

            J.report("footprint",
                 "The average footprint radius was {:.0f} ±{:.0f} m.",
                 data.footprint_radius.dropna().mean(), data.footprint_radius.dropna().std())
        
        # 0.44 (dry) ..0.5 (wet) is roughly the average D over radii

        ####################################################
        ######  Time Resolution  ###########################
        ####################################################

        tres_orig = data_orig.index.to_series().dropna().diff().median().total_seconds()
        tres_orig_str = ''
        if   tres_orig/86400 >= 1: tres_orig_str = '%.0fd' % (tres_orig/86400)
        elif tres_orig/3600  >= 1: tres_orig_str = '%.0fh' % (tres_orig/3600)
        elif tres_orig/60    >= 1: tres_orig_str = '%.0fmin' % (tres_orig/60)
        else                     : tres_orig_str = '%.0fsec' % (tres_orig)

        tres = data.index.to_series().dropna().diff().median().total_seconds()
        tres_str = ''
        if   tres/86400 >= 1: tres_str = '%.0fd' % (tres/86400)
        elif tres/3600  >= 1: tres_str = '%.0fh' % (tres/3600)
        elif tres/60    >= 1: tres_str = '%.0fmin' % (tres/60)
        else                : tres_str = '%.0fsec' % (tres)

        ####################################################
        ######  POI  #######################################
        ####################################################

        poi_data = pandas.DataFrame()
        if cc(config,'conversion','poi_table'):
            print('| Evaluating points of interest...')
            poi = '_poi'

            #POI = read_geojson(config['conversion']['poi_json']) # 'data/Selke.geojson'
            poi_list = gc(config,'conversion','poi_table', dtype='multi')

            _poi_data_columns = ['name','lat','lon','radius','utmx','utmy',Nfinal,Nfinal+err,Nfinal+std,smv,smv+err,smv+std]
            _poi_data = []
            for item in poi_list:
                _poi_data.append(xsplit(item, range=len(_poi_data_columns)))
            poi_data = pandas.DataFrame(_poi_data, columns=_poi_data_columns)
            del _poi_data

            poi_data.lat = pandas.to_numeric(poi_data.lat)
            poi_data.lon = pandas.to_numeric(poi_data.lon)
            poi_data.radius = pandas.to_numeric(poi_data.radius)
            utmx, utmy = latlon2utm(
                poi_data.lat, poi_data.lon,
                epsg = gc(config,"clean","utm_epsg", alt=31468))
            poi_data['utmy'] = utmy
            poi_data['utmx'] = utmx

            _data = data[['utmx','utmy',Nfinal,Nfinal+err,smg,'bd',smv,smv+err]].copy()

            for i, row in poi_data.iterrows():
                _data['distance2poi'] = np.sqrt((_data.utmx-row['utmx'])**2+(data.utmy-row['utmy'])**2)

                if cc(config,'conversion','poi_distance_weight','inverse'):
                    _data['distance_weight'] = 1/_data['distance2poi']
                elif cc(config,'conversion','poi_distance_weight','W_r_approx'):
                    _data['distance_weight'] = Wr_approx(_data['distance2poi'])
                else:
                    _data['distance_weight'] = 1

                _query = (_data.distance2poi < row['radius']) & (~np.isnan(_data[Nfinal]))
                if len(_data[_query])>0:
                    poi_data.loc[i, Nfinal]     = np.average(_data.loc[_query, Nfinal],     weights=_data.loc[_query, 'distance_weight'])
                    poi_data.loc[i, Nfinal+err] = np.average(_data.loc[_query, Nfinal+err], weights=_data.loc[_query, 'distance_weight']) / np.sqrt(_data.loc[_query, Nfinal].count())
                    poi_data.loc[i, Nfinal+std] =            _data.loc[_query, Nfinal].std()
                    _query = (_data.distance2poi < row['radius']) & (~np.isnan(_data[smv]))
                    if np.sum(_data.loc[_query, 'distance_weight'])>0:
                        poi_data.loc[i, smv]        = np.average(_data.loc[_query, smv],        weights=_data.loc[_query, 'distance_weight'])
                        poi_data.loc[i, smv+err]    = np.average(_data.loc[_query, smv+err],    weights=_data.loc[_query, 'distance_weight']) / np.sqrt(_data.loc[_query, smv].count())
                        poi_data.loc[i, smv+std]    =            _data.loc[_query, smv].std()
                        #poi_data.loc[i, smg]        = np.average(_data.loc[_query, smg],        weights=_data.loc[_query, 'distance_weight'])
                        poi_data.loc[i, 'bd']        = np.average(_data.loc[_query, 'bd'],        weights=_data.loc[_query, 'distance_weight'])
                        #poi_data.loc[i, smg+err]    = np.average(_data.loc[_query, smg+err],    weights=_data.loc[_query, 'distance_weight']) / np.sqrt(_data.loc[_query, smv].count())
                        #poi_data.loc[i, smg+std]    =            _data.loc[_query, smg].std()

                        print('|   POI %15s (#%2.0f >%3.0f m): N = %5.0f +/-%4.0f cph, SM = %4.1f +/-%4.1f vol.%%, bd %4.1f' % (row['name'],
                            _data.loc[_query, 'distance2poi'].count(), _data.loc[_query, 'distance2poi'].min(),
                            poi_data.loc[i, Nfinal],  poi_data.loc[i, Nfinal+std], # poi_data.loc[i, Nfinal+std],
                            poi_data.loc[i, smv]*100, poi_data.loc[i, smv+std]*100, poi_data.loc[i, 'bd'])) #, poi_data.loc[i, smv+std]*100)) # (%4.1f)
                        _data.loc[_query, smv].to_csv(config['output']['out_path'] + 'poi-%s.csv' % row['name'], index=False, float_format='%.4f')
                else:
                    print('|   POI %15s (  >%5.0f m): (not covered)' % (row['name'], _data.distance2poi.min()))

            poi_data.to_csv(output_basename+'_POI.csv',
                sep = config['output']['CSV_column_sep'].strip('\"'),
                decimal = config['output']['CSV_decimal'],
                float_format = '%' + config['output']['CSV_float_format'],
                na_rep = config['output']['CSV_NaN'])
            file_save_msg(output_basename + '_POI.csv', "POI", end="\n")

            J.report("poi",
                     "For comparison, the data was averaged near provided points of interest using {:} weighting.",
                     gc(config,'conversion','poi_distance_weight'))
        else:
            poi = ''


        ####################################################
        ######  Output  ####################################
        ####################################################
        print('[Output]')
        
        if gridding:
            timestr = data.index.mean().strftime("%Y%m%d_%H%M")
            trackstr = ""
            gridstr = "G"
            save_index = True
            
            if cc(config,'clean','grid__master_file'):
                gridstr = "MG"
                save_index = False
            elif cc(config,'clean','grid_bbox'):
                gridstr = "RG"
            
            if len(dataset) > 1:
                trackstr = '-track%02d' % track
            
            output_filename = "%s%s-%s_%s%s.dat" % (
                os.path.join(config['output']['out_path'],''),
                config['output']['out_basename'],
                gridstr, timestr, trackstr)

            M.to_csv(output_filename, index=save_index, float_format='%.4f')
            file_save_msg(output_filename, "Grid", end="\n")


        if config['output']['CSV_datetime_format']:
            CSV_datetime_format = config['output']['CSV_datetime_format']
        else:
            CSV_datetime_format = 'Y-m-d H:M:S'
        datetime_format = re.sub(r'(\w)', r'%\1', CSV_datetime_format)

        
        if yesno2bool(config['output']['make_CSV']):
            #print('| CSV data preparation... ')
            
            if cc(config,'output','CSV_columns'):
                out_columns = []
                column_names = xsplit(gc(config,'output','CSV_columns'))
                for item in column_names:
                    item = item.replace('neutrons_proc', Nfinal)
                    item = item.replace('neutrons_raw', N)
                    item = item.replace('relative_humidity', rh)
                    item = item.replace('temperature', T)
                    item = item.replace('pressure', p)
                    item = item.replace('alt', 'Alt')
                    if item in data.columns:
                        out_columns.append(item)
            else:
                out_columns  = data.columns
                column_names = data.columns

            try:
                data[out_columns].to_csv(output_basename+'.csv',
                    date_format = datetime_format, header=data[out_columns].columns,
                    sep = config['output']['CSV_column_sep'].strip('\"'),
                    decimal = config['output']['CSV_decimal'],
                    float_format = '%' + config['output']['CSV_float_format'],
                    na_rep = config['output']['CSV_NaN'])
            except Exception as e:
                print('! ERROR: Cannot write CSV file:', e)

            file_save_msg(output_basename + '.csv', "Data", end="\n")

        ### Summary

        if yesno2bool(gc(config,'output','make_summary')):

            Variables = [smv,   smv+err  , T,      ah,       inc,      p,       alt] #,   'Vbat', 'N01C', 'N02C']
            Names =     ['SWC', 'SWC_err', 'Temp', 'AbsHum', 'Incrad', 'Press', 'Alt'] #, 'Volt', 'N1',   'N2'  ]
            i = 1
            for tube_column in xsplit(config['input']['neutron_columns']):
                Variables.append(tube_column)
                Names.append('N%d' % i)
                i += 1
            
            Summary_track = pandas.DataFrame()

            if 'luse_str' in data.columns:
                luse_list = list(data.luse_str.unique())
            else:
                luse_list = []
            #for luse in ['all'] + luse_list:
            for luse in ['all', 'agriculture', 'forest', 'urban']:

                Summary_track_luse = pandas.DataFrame()

                data_luse = None
                if luse == 'all':
                    data_luse = data
                    luse = ''
                else:
                    if 'luse_str' in data.columns:
                        if luse in list(data.luse_str):
                            data_luse = data.loc[data.luse_str==luse]
                            luse = '_'+luse[0:3]
                
                if not data_luse is None:
                    for var, name in zip(Variables, Names):

                        Summary_track_luse_var = pandas.DataFrame({
                            name+luse+'_avg': data_luse[var].mean(),
                            name+luse+'_med': data_luse[var].median(),
                            name+luse+'_min': data_luse[var].min(),
                            name+luse+'_max': data_luse[var].max(),
                            name+luse+'_std': data_luse[var].std(),
                            }, index=pd.Series([track], name='Track'))

                        # Merge var columns
                        Summary_track_luse = pandas.concat([Summary_track_luse, Summary_track_luse_var], axis=1)

                    # Add counts for outliers
                    Summary_track_luse_sa7 = pandas.DataFrame({
                        smv+'_above70'+luse+'_num': int(data_luse.loc[data_luse[smv]>0.7, smv].count()) },
                        index=pd.Series([track], name='Track'))

                    # Merge luse columns
                    Summary_track = pandas.concat([Summary_track, Summary_track_luse, Summary_track_luse_sa7], axis=1)

            # Merge track rows
            Summary = pandas.concat([Summary, Summary_track], axis=0)

            try:
                Summary.to_csv(output_basename_notrack+'_summary.csv',
                    date_format = datetime_format, header=Summary.columns,
                    sep = config['output']['CSV_column_sep'].strip('\"'),
                    decimal = config['output']['CSV_decimal'],
                    float_format = '%' + config['output']['CSV_float_format'],
                    na_rep = config['output']['CSV_NaN'])
            except Exception as e:
                print('! ERROR: Cannot write summary file: ', e)

            file_save_msg(output_basename_notrack + '_summary.csv', "Summary", end="\n")

        # Figures
        if yesno2bool(config['output']['make_PDF']):

            figure_name = output_basename + '.pdf'
            if 'PDF_sm_range' in config['output']:
                smrange = xsplit(config['output']['PDF_sm_range'], range=2, type=float)
            else:
                smrange = (0,1)
            
            time_range_hours = (data.index.max()-data.index.min()).total_seconds()/60/60
            time_range_days  = time_range_hours/24
            time_range_years = time_range_days/365
            time_range       = time_range_years
            
            pdf_plots_list = xsplit(config['output']['PDF_plots'])

            # Date range in plots, minimum a full day
            if time_range_hours > 12:
                date1 = datetime(data.index.min().year, data.index.min().month, data.index.min().day, 0,0,0)
                date2 = datetime(data.index.max().year, data.index.max().month, data.index.max().day, 23,59,59)
            else:
                date1 = datetime(data.index.min().year, data.index.min().month, data.index.min().day, data.index.min().hour, 0,0)
                date2 = datetime(data.index.max().year, data.index.max().month, data.index.max().day, data.index.max().hour, 59,59)

            # New
            from corny.reports import Report

            title = config['info']['title'].encode('latin-1').decode('utf-8')
        
            figure_name2 = output_basename + '-R.pdf'

            # print("i The Journalist collected {:.0f} stories: {:}".format(
            #     len(J.story), list(J.story.keys())))

            # Make SVG plots for daily data <10 years or hourly data < 5 months, else make PNG plots
            kw_fig_buff = dict(save="buff", format="svg" if len(data)<3650 else "png")
            
            # Find best xaxis labels for the given time span
            if time_range > 2:
                kw_fig_time = dict(x_major_ticks="years", x_minor_ticks="months", x_major_fmt="%Y", x_minor_fmt="")
            elif time_range > 1:
                kw_fig_time = dict(x_major_ticks="years", x_minor_ticks="months", x_major_fmt="%b\n%Y", x_minor_fmt="%b")
            elif time_range > 4/12:
                kw_fig_time = dict(x_major_ticks="months", x_minor_ticks="weeks", x_major_fmt="%b\n%Y", x_minor_fmt="")
            elif time_range > 2/12:
                kw_fig_time = dict(x_major_ticks="months", x_minor_ticks="days", x_major_fmt="%b\n%Y", x_minor_fmt="")
            elif time_range > 1/12:
                kw_fig_time = dict(x_major_ticks="weeks", x_minor_ticks="days", x_major_fmt="%d\n%b", x_minor_fmt="")
            elif time_range > 3/365:
                kw_fig_time = dict(x_major_ticks="months", x_minor_ticks="days", x_major_fmt="%d\n%b", x_minor_fmt="%d")
            elif time_range > 6/24/365:
                kw_fig_time = dict(x_major_ticks="days", x_minor_ticks="hours", x_major_fmt="%d\n%b", x_minor_fmt="%H:%M")
            elif time_range > 2/24/365:
                kw_fig_time = dict(x_major_ticks="hours", x_major_fmt="%H")
            else:
                kw_fig_time = dict(x_major_ticks="hours", x_major_fmt="%H:%M", x_minor_ticks="minutes")
            
            with Report(figure_name2, "Data Report: %s" % title) as R:
                with Progress("| Making PDF report", replace=True) as Pr:

                    Pr.update("Cover page")
                    R.add_page()
                    R.add_title(title)
                    R.add_par(config['info']['subtitle'].encode('latin-1').decode('utf-8'))
                    R.add_par("Contact: " + config['info']['contact'].encode('latin-1').decode('utf-8'))
                    R.add_par( J.post("general", "clean") )

                    if os.path.exists(config['info']['cover']):
                        R.add_image(config['info']['cover'])
                    
                    Pr.update("Neutrons epithermal")
                    R.add_page()
                    R.add_title("Neutrons epithermal")
                    R.add_par(J.post("neutrons"))

                    if not data[N].isnull().all():
                        with Figure(size=(10,11), layout=(3,1),
                                    **kw_fig_buff, **kw_fig_time,
                                    x_ticks_last=True) as F:
                            
                            ax = F.axes[0]
                            # ax.scatter(data_orig.index, data_orig[N],
                            #            label="$N$ raw",
                            #            marker='o', s=5, lw=0, color='#DDDDDD')
                            ax.scatter(data.index, data[N],
                                    label="$N$ raw",
                                    marker='o', s=10, lw=0, color='#CCCCCC')
                            data_outliers = data[data[N+clean] != data[N]]
                            ax.scatter(data_outliers.index, data_outliers[N],
                                    label="$N$ raw (dropped)",
                                    marker='+', s=40, lw=2, color='red')

                            ax = F.axes[1]
                            ax.scatter(data.index, data[N+clean],
                                    label="$N$ cleaned",
                                    marker='o', s=10, lw=0, color='#CCCCCC')
                            ax.scatter(data.index, data[N+clean+pih],
                                    label="$N$ cleaned, atm. corrections",
                                    marker='o', s=10, lw=0, color='#999999')
                            
                            ax = F.axes[2]
                            if Nfinal != N+clean+pih:
                                ax.scatter(data.index, data[N+clean+pih],
                                    label="$N$ cleaned, atm. corrections",
                                    marker='o', s=10, lw=0, color='#999999')
                            ax.plot(data.index, data[Nfinal],
                                    label="$N$ cleaned, atm. corrections, smoothed",
                                    lw=1, color='C0') #, drawstyle="steps-post")
                            ax.fill_between(data.index,
                                            data[Nfinal]-data[Nfinal+'_err'],
                                            data[Nfinal]+data[Nfinal+'_err'],
                                            lw=0, color='C0', alpha=0.5)

                            for ax in F.axes:
                                ax.set_ylabel("Neutrons (in cph)")
                                ax.set_xlim(date1, date2)
                                ax.grid(color="k", alpha=0.1)
                                ax.legend(loc="upper left", bbox_to_anchor=(0, 1.12),
                                        ncol=3, fancybox=False, framealpha=1, edgecolor="k",
                                        shadow=True)
                                if ax != F.axes[-1]:
                                    ax.set_xticklabels([])
                        
                        buffer = F.buff
                        R.add_image(buffer)

                    if Nth:
                        Pr.update("Neutrons thermal   ")
                        R.add_page()
                        R.add_title("Neutrons thermal")
                        R.add_par(J.post("neutronsth"))

                        if not data[N+th].isnull().all():
                            with Figure(size=(10,11), layout=(3,1),
                                    **kw_fig_buff, **kw_fig_time,
                                    x_ticks_last=True) as F:
                                
                                ax = F.axes[0]
                                # ax.scatter(data_orig.index, data_orig[N],
                                #            label="$N$ raw",
                                #            marker='o', s=5, lw=0, color='#DDDDDD')
                                ax.scatter(data.index, data[N+th],
                                        label="$N_{th}$ raw",
                                        marker='o', s=10, lw=0, color='#CCCCCC')
                                data_outliers = data[data[N+th+clean] != data[N+th]]
                                ax.scatter(data_outliers.index, data_outliers[N+th],
                                        label="$N_{th}$ raw (dropped)",
                                        marker='+', s=40, lw=2, color='red')

                                ax = F.axes[1]
                                ax.scatter(data.index, data[N+th+clean],
                                        label="$N_{th}$ cleaned",
                                        marker='o', s=10, lw=0, color='#CCCCCC')
                                ax.scatter(data.index, data[N+th+clean+pih],
                                        label="$N_{th}$ cleaned, atm. corrections",
                                        marker='o', s=10, lw=0, color='#999999')
                                
                                ax = F.axes[2]
                                if Nthfinal != N+th+clean+pih:
                                    ax.scatter(data.index, data[N+th+clean+pih],
                                        label="$N_{th}$ cleaned, atm. corrections",
                                        marker='o', s=10, lw=0, color='#999999')
                                ax.plot(data.index, data[Nthfinal],
                                        label="$N_{th}$ cleaned, atm. corrections, smoothed",
                                        lw=1, color='C1') #, drawstyle="steps-post")
                                ax.fill_between(data.index,
                                                data[Nthfinal]-data[Nthfinal+'_err'],
                                                data[Nthfinal]+data[Nthfinal+'_err'],
                                                lw=0, color='C1', alpha=0.5)

                                for ax in F.axes:
                                    ax.set_ylabel("Neutrons (in cph)")
                                    ax.set_xlim(date1, date2)
                                    ax.grid(color="k", alpha=0.1)
                                    ax.legend(loc="upper left", bbox_to_anchor=(0, 1.12),
                                            ncol=3, fancybox=False, framealpha=1, edgecolor="k",
                                            shadow=True)
                                    if ax != F.axes[-1]:
                                        ax.set_xticklabels([])
                            
                            buffer = F.buff
                            R.add_image(buffer)

                        Pr.update("Neutrons epi vs. thermal")
                        R.add_page()
                        R.add_title("Neutrons epi vs. thermal")
                        R.add_par(J.post("neutronsratio"))
                        
                        if not data[N+th].isnull().all():
                            with Figure(size=(10,11), layout=(3,1),
                                    **kw_fig_buff, **kw_fig_time,
                                    x_ticks_last=True) as F:
                                
                                ax = F.axes[0]
                                ax.plot(data.index, data[Nfinal],
                                        label="$N$ epithermal",
                                        lw=1, color='C0')
                                ax.plot(data.index, data[Nthfinal],
                                        label="$N_{th}$ thermal",
                                        lw=1, color='C1')
                                ax.set_ylabel("Neutrons (in cph)")
                                ax.set_ylim(0,None)

                                ax = F.axes[1]
                                ax.plot(data.index, (data[Nfinal] - data[Nfinal].mean())/data[Nfinal].std(ddof=0),
                                        label="$N$ epithermal, normalized",
                                        lw=1, color='C0')
                                ax.plot(data.index, (data[Nthfinal] - data[Nthfinal].mean())/data[Nthfinal].std(ddof=0),
                                        label="$N_{th}$ thermal, normalized",
                                        lw=1, color='C1')
                                ax.set_ylabel("z score (--)")

                                ax = F.axes[2]
                                ax.plot(data.index, data[Nthfinal]/data[Nfinal],
                                        label="$N_{th}/N$ ratio",
                                        lw=1, color='grey', ls="--")
                                ax.set_ylabel("Neutron ratio (--)")

                                for ax in F.axes:
                                    ax.grid(color="k", alpha=0.1)
                                    ax.set_xlim(date1, date2)
                                    ax.legend(loc="upper left", bbox_to_anchor=(0, 1.12),
                                            ncol=3, fancybox=False, framealpha=1, edgecolor="k",
                                            shadow=True)
                                    if ax != F.axes[-1]:
                                        ax.set_xticklabels([])
                            
                            buffer = F.buff
                            R.add_image(buffer)

                    if Muons:
                        Pr.update("Muons             ")
                        R.add_page()
                        R.add_title("Muons")
                        R.add_par(J.post("muons"))

                        if not data[mu].isnull().all():
                            with Figure(size=(10,11), layout=(3,1),
                                    **kw_fig_buff, **kw_fig_time,
                                    x_ticks_last=True) as F:
                                
                                ax = F.axes[0]
                                # ax.scatter(data_orig.index, data_orig[N],
                                #            label="$N$ raw",
                                #            marker='o', s=5, lw=0, color='#DDDDDD')
                                ax.scatter(data.index, data[mu+"_cph"],
                                        label="Muons raw",
                                        marker='o', s=10, lw=0, color='#CCCCCC')
                                data_outliers = data[data[mu+clean] != data[mu+"_cph"]]
                                ax.scatter(data_outliers.index, data_outliers[mu+"_cph"],
                                        label="Muons raw (dropped)",
                                        marker='+', s=40, lw=2, color='red')

                                ax = F.axes[1]
                                ax.scatter(data.index, data[mu+clean],
                                        label="Muons cleaned",
                                        marker='o', s=10, lw=0, color='#CCCCCC')
                                ax.scatter(data.index, data[mu+clean+"_ph"],
                                        label="$Muons cleaned, atm. corrections",
                                        marker='o', s=10, lw=0, color='#999999')
                                
                                ax = F.axes[2]
                                ax.plot(data.index, (data[mufinal] - data[mufinal].mean())/data[mufinal].std(ddof=0),
                                        label="Muons, normalized",
                                        lw=1, color='C0')
                                ax.plot(data.index, (data["NM"] - data["NM"].mean())/data["NM"].std(ddof=0),
                                        label="Neutron Monitor, normalized",
                                        lw=1, color='C3')
                                ax.set_ylabel("z score (--)")

                                for ax in F.axes[0:2]:
                                    ax.set_ylabel("Muons (cph)")
                                for ax in F.axes:
                                    ax.set_xlim(date1, date2)
                                    ax.grid(color="k", alpha=0.1)
                                    ax.legend(loc="upper left", bbox_to_anchor=(0, 1.12),
                                            ncol=3, fancybox=False, framealpha=1, edgecolor="k",
                                            shadow=True)
                                    if ax != F.axes[-1]:
                                        ax.set_xticklabels([])
                            
                            buffer = F.buff
                            R.add_image(buffer)
                    
                    Pr.update("Atmosphere")
                    R.add_page()
                    R.add_title("Atmosphere")
                    R.add_par(J.post("atmos"))
                    
                    with Figure(size=(10,11), layout=(3,1),
                                **kw_fig_buff, **kw_fig_time,
                                x_ticks_last=True) as F:
                        
                        ax = F.axes[0]
                        if not data[p].isnull().all():
                            ax.plot(data.index, data[p],
                                    label="Air pressure",
                                    lw=1, color='lightgrey')
                            ax.set_ylabel("Air pressure (in hPa)")
                            
                        ax = F.axes[1]
                        if not data[T].isnull().all():
                            ax.plot(data.index, data[T],
                                    lw=1, color='C4', label="-")
                            ax.set_ylim(-20,40)
                            ax.axhline(0.0, color="C4", ls="--", lw=1)
                            ax.set_ylabel("Air temperature (in °C)")

                        if not data[rh].isnull().all():
                            ax2 = ax.twinx()
                            ax2.plot(data.index, data[T]-200,
                                    label="Air temperature",
                                    lw=1, color='C4')
                            ax2.plot(data.index, data[rh],
                                    label="Air relative humidity",
                                    lw=1, color='C9', alpha=0.5)
                            ax2.set_ylim(0,110)
                            ax2.axhline(100.0, color="C9", alpha=0.5, ls="--", lw=1)
                            ax2.set_ylabel("Air relative humidity (in %)")
                            ax2.legend(loc="upper left", bbox_to_anchor=(0, 1.12),
                                        ncol=3, fancybox=False, framealpha=1, edgecolor="k",
                                        shadow=True)
                            
                        ax = F.axes[2]
                        if not data[ah].isnull().all():
                            ax.plot(data.index, data[ah],
                                    label="Air absolute humidity",
                                    lw=1, color='C9')
                            ax.set_ylim(0,20)
                            ax.set_ylabel("Air absolute humidity (in g/m³)")
                        
                        for ax in F.axes:
                            ax.grid(color="k", alpha=0.1)
                            ax.set_xlim(date1, date2)
                            ax.legend(loc="upper left", bbox_to_anchor=(0, 1.12),
                                    ncol=3, fancybox=False, framealpha=1, edgecolor="k",
                                    shadow=True)
                            if ax != F.axes[-1]:
                                ax.set_xticklabels([])

                    buffer = F.buff
                    R.add_image(buffer)
                    
                    Pr.update("Corrections")
                    R.add_page()
                    R.add_title("Corrections")
                    R.add_par(J.post("corrections", "soil"))
                    
                    if not data[N].isnull().all():
                        with Figure(size=(10,9), layout=(3,1),
                                **kw_fig_buff, **kw_fig_time,
                                x_ticks_last=True) as F:
                            
                            ax = F.axes[0]
                            ax.plot(data.index, data["correct_p"],
                                    label="Air pressure",
                                    lw=1, color='lightgrey')
                            ax.plot(data.index, data["correct_h"],
                                    label="Air humidity",
                                    lw=1, color='C9')
                            ax.plot(data.index, data["correct_i"],
                                    label="Incoming neutron flux",
                                    lw=1, color='C3')
                            if urb != '':
                                ax.plot(data.index, data['correct_urban'],
                                    label="Urban area",
                                    lw=4, color='C6')
                            if bio != '':
                                ax.plot(data.index, data['correct_bio'],
                                    label="Biomass",
                                    lw=1, color='C2')
                            if road!= '':
                                ax.plot(data.index, data['correct_road'],
                                    label="Road effect",
                                    lw=1, color='black')
                            
                            ax.axhline(1.0, color="k", ls="--", lw=1)
                            ax.set_ylabel("Correction factor ($-$)")
                            ax.set_ymargin(0.12)

                            ax = F.axes[1]
                            ax.plot(data.index, data["bd"],
                                    label="Bulk density",
                                    lw=1, color='brown')
                            ax.set_ylabel("Bulk density (in kg/m³)")
                            ax.set_ylim(0,2.0)

                            ax = F.axes[2]
                            ax.plot(data.index, data["lw"],
                                    label="lattice water",
                                    lw=1, color='C8')
                            ax.plot(data.index, data["owe"],
                                    label="Organic water",
                                    lw=1, color='C9')
                            ax.set_ylim(0,0.15)
                            ax.set_ylabel("Water equivalent (in g/g)")
                            
                            for ax in F.axes:
                                ax.grid(color="k", alpha=0.1)
                                ax.set_xlim(date1, date2)
                                ax.legend(loc="upper left", bbox_to_anchor=(0, 1.12),
                                        ncol=5, fancybox=False, framealpha=1, edgecolor="k",
                                        shadow=True)
                                if ax != F.axes[-1]:
                                    ax.set_xticklabels([])
                        
                        buffer = F.buff
                        R.add_image(buffer)

                    Pr.update("Soil moisture")
                    R.add_page()
                    R.add_title("Soil moisture")
                    R.add_par(J.post("sm"))
                    
                    if not data[smv].isnull().all():
                        with Figure(size=(10,7), layout=(2,1),
                                **kw_fig_buff, **kw_fig_time,
                                x_ticks_last=False, gridspec_kw=dict(hspace=0.3)) as F:
                            
                            ax = F.axes[0]
                            ax.plot(data.index, data[smv],
                                    color='C0', drawstyle='steps-post',
                                    lw=1, label="Water content")
                            ax.fill_between(data.index, data[smv],
                                            0, color='C0', alpha=0.05,
                                            lw=0, step='post')
                            ax.set_ylim(smrange[0], smrange[1])
                            ax.set_yticks(np.arange(smrange[0],smrange[1]+0.01, (smrange[1]-smrange[0])/10))
                            ax.set_ylabel("Water content (in m³/m³)")
    
                            ax = F.axes[1]
                            _ylim = 1.1*data[smv+err+'_upp'].max()
                            # if _ylim < 0.1: _ylim = 0.1
                            ax.plot(data.index, data[smv+err+'_low'],
                                drawstyle='steps', color='C3', lw=1,
                                label="Lower error")
                            ax.plot(data.index, data[smv+err+'_upp'],
                                drawstyle='steps', color='C2', lw=1,
                                label="Upper error")
                            ax.fill_between(data.index, data[smv+err+'_low'], 0,
                                color='C3', alpha=0.2, lw=0, step='pre')
                            ax.fill_between(data.index, data[smv+err+'_upp'], 0,
                                color='C2', alpha=0.2, lw=0, step='pre')
                            ax.set_ylabel("Water content (in m³/m³)")
                            ax.set_ylim(-_ylim, _ylim)
                            
                            for ax in F.axes:
                                ax.grid(color="k", alpha=0.1)
                                ax.set_xlim(date1, date2)
                                ax.legend(loc="upper left", bbox_to_anchor=(0, 1.10),
                                        ncol=3, fancybox=False, framealpha=1, edgecolor="k",
                                        shadow=True)
                                if ax != F.axes[-1]:
                                    ax.set_xticklabels([])
                        
                        buffer = F.buff
                        R.add_image(buffer)

                        with Figure(size=(10,2), layout=(1,1), save="buff") as F:
                            
                            ax = F.axes
                            ax.hist(data[sm].dropna().values,
                                    bins=100, density=True, histtype='stepfilled',
                                    label="Probability density")
                            ax.set_xlim(smrange[0],smrange[1])
                            ax.set_xticks(np.arange(smrange[0],smrange[1],(smrange[1]-smrange[0])/10))
                            ax.set_xlabel('Water content (in m³/m³)')
                            ax.set_ylabel('Probability (a.u.)')

                            ax.grid(color="k", alpha=0.1)
                            ax.legend(loc="upper left", bbox_to_anchor=(0, 1.12),
                                    ncol=3, fancybox=False, framealpha=1, edgecolor="k",
                                    shadow=True)
                        
                        buffer = F.buff
                        R.add_image(buffer)

                    # Trends
                    if time_range < 2:
                        # R.add_par("Time range covers only {:.1f} years, which is too short for a trend analysis.".format(time_range)) 
                        pass

                    elif not data[smv].isnull().all():

                        Pr.update("Trends")
                        R.add_page()
                        R.add_title("Trends")
                        # R.add_par(J.post("sm"))

                        data['zdiff'] = data[smv].interpolate().rolling('1D').mean()
                        data['doy']   = data.index.dayofyear
                        data['year']   = data.index.year
                        data_m = data.resample('1M').mean()
                        data_y = data.resample('1Y').mean()

                        #data = data.interpolate().rolling('30D').mean()

                        piv = pandas.pivot_table(data, index=['doy'], columns=['year'], values=['zdiff'])
                        pivdf = pandas.DataFrame()
                        pivdf['doy'] = piv.index
                        pivdf['avg'] = piv.mean(axis=1)
                        pivdf['min'] = piv.min(axis=1)
                        pivdf['max'] = piv.max(axis=1)
                        pivdf['std'] = piv.std(axis=1)
                        s = (pivdf.index.to_series() / 30.5).astype(int)
                        pivdf['mmonth'] = s+1
                        for M in range(12):
                            month_filter = pivdf.mmonth==M+1
                            pivdf.loc[month_filter, 'mmmin'] = pivdf.loc[month_filter, 'avg'].mean()-pivdf.loc[month_filter, 'std'].mean()
                            pivdf.loc[month_filter, 'mmmax'] = pivdf.loc[month_filter, 'avg'].mean()+pivdf.loc[month_filter, 'std'].mean()
                        data_daily_avg = data.join(pivdf.set_index('doy'), on='doy')
                        y_max = data.zdiff.max()
                        
                        with Figure(size=(10,7), layout=(2,1), **kw_fig_buff,
                                    x_major_ticks="years", x_minor_ticks="months",
                                    x_major_fmt="%Y", x_minor_fmt="",
                                    x_ticks_last=True) as F:
                            
                            ax = F.axes[0]
                            # ax.figure.suptitle('Water content', fontsize=18)
                            # ax.figure.subplots_adjust(top=0.84)
                            
                            ax.set_title('Time series')
                            ax.plot(data.index, data[smv], marker='o', ls='', ms=2, color='#BBBBBB', label='daily')
                            ax.fill_between(data.index, data[smv], data[smv]*0, color='#EEEEEE')
                            ax.plot(data.index, data[smv].rolling(30, center=True, min_periods=5).mean(), color='k', label='monthly')
                            ax.plot(data.index, data[smv].rolling(365, center=True, min_periods=180).mean(), color='k', ls='--', label='yearly')
                            ax.grid(color='k', alpha=0.2)
                            ax.set_xlim(data.index.min(), date(date.today().year,12,31)) #data.index.max())
                            ax.set_ylim(0,y_max)
                            ax.set_ylabel('Water content (m³/m³)')
                            ax.legend(loc='upper right', framealpha=1,
                                edgecolor='w', borderpad=0.1, borderaxespad=0.1,
                                bbox_to_anchor=(1.14, 1))
                            
                            ax = F.axes[1]
                            ax.set_title('Extremes indentification') #, pad=20)
                            # ax.xaxis.set_major_locator(YearLocator())
                            # ax.xaxis.set_minor_locator(MonthLocator())
                            ax.grid(color='k', alpha=0.2)
                            ax.scatter(data.index, data.zdiff, s=10, marker='.', fc='none', color='k', alpha=0.1, label='Daily data')
                            ax.plot(data_m.index, data_m.zdiff, color='k', drawstyle='steps', label='Monthly mean')
                            #ax.fill_between(data_daily_avg.index, data_daily_avg['mmmin'], data_daily_avg['mmmax'], color='C0', step='pre', alpha=0.3,
                            #               label='Quantiles 25%/75%')
                            ax.fill_between(data_daily_avg.index, data_daily_avg['mmmin'], data_daily_avg['mmmax']*0, color='C3', step='pre', alpha=0.2, label='Quantile <25%')
                            ax.fill_between(data_daily_avg.index, data_daily_avg['mmmin']*0+y_max*1.02, data_daily_avg['mmmax'], color='C0', step='pre', alpha=0.2, label='Quantile >75%')

                            for i, row in data_y.drop_duplicates(['year']).iterrows():
                                if i.year > data.index.min().year:
                                    ax.text(date(i.year, 7, 1), y_max*0.945, 'Ø %.0f' % (row.zdiff*100), ha='center') #1.035
                            ax.set_ylim(0, y_max*1.02)
                            ax.set_ylabel('Water content (a.u.)')
                            ax.set_xlim(data.index.min(), date(date.today().year,12,31))
                            ax.legend(loc='upper right', framealpha=1,
                                edgecolor='w', borderpad=0.1, borderaxespad=0.1,
                                bbox_to_anchor=(1.20, 1))
                            # fig = ax.figure

                            for ax in F.axes:
                                # ax.grid(color="k", alpha=0.1)
                                # ax.legend(loc="upper left", bbox_to_anchor=(0, 1.05),
                                #         ncol=3, fancybox=False, framealpha=1, edgecolor="k",
                                #         shadow=True)
                                if ax != F.axes[-1]:
                                    ax.set_xticklabels([])
                        
                        R.add_image(F.buff)

                        with Figure(size=(10,6), layout=(2,1), save="buff") as F:
                            
                            ax = F.axes[0]
                            ax.set_title('Yearly comparison')
                            piv.plot(ax=ax, drawstyle='steps', cmap='Spectral_r', lw=2)
                            ax.set_xticks(np.cumsum(np.array([0,31,28,31,30,31,30,31,31,30,31,30])))
                            ax.set_xticklabels(['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'])
                            ax.set_xlabel('')
                            ax.set_ylabel('Water content (a.u.)')
                            ax.set_xlim(0,366)
                            ax.set_ylim(0, y_max*1.02)
                            ax.grid(axis='y', color='k', alpha=0.2)
                            ax.axvline(181, color='k', alpha=0.2, lw=1)
                            han, lab = ax.get_legend_handles_labels()
                            ax.legend(han, ['%s (Ø %.0f)' % (x, y) for x,y in zip([x[1] for x in piv.columns], data_y.zdiff.values*100)],
                                    ncol=1, loc='upper right', framealpha=1,
                                    edgecolor='w', borderpad=0.1, borderaxespad=0.1,
                                    bbox_to_anchor=(1.185, 1))

                            ax = F.axes[1]
                            ax.set_title('Yearly cumulative comparison')
                            piv.cumsum().plot(ax=ax, drawstyle='steps', cmap='Spectral_r', lw=2)
                            ax.set_xticks(np.cumsum(np.array([0,31,28,31,30,31,30,31,31,30,31,30])))
                            ax.set_xticklabels(['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'])
                            ax.set_xlabel('')
                            ax.set_ylabel('Cumulative water content (a.u.)')
                            ax.set_xlim(0,366)
                            #ax.set_ylim(0, y_max*1.02)
                            ax.grid(axis='y', color='k', alpha=0.2)
                            ax.axvline(181, color='k', alpha=0.2, lw=1)
                            han, lab = ax.get_legend_handles_labels()
                            ax.legend(han, ['%s (Ø %.0f)' % (x, y) for x,y in zip([x[1] for x in piv.columns], data_y.zdiff.values*100)],
                                    ncol=1, loc='upper right', framealpha=1,
                                    edgecolor='w', borderpad=0.1, borderaxespad=0.1,
                                    bbox_to_anchor=(1.185, 1))
                            
                            for ax in F.axes:
                                # ax.grid(color="k", alpha=0.1)
                                # ax.legend(loc="upper left", bbox_to_anchor=(0, 1.05),
                                #         ncol=3, fancybox=False, framealpha=1, edgecolor="k",
                                #         shadow=True)
                                if ax != F.axes[-1]:
                                    ax.set_xticklabels([])

                        R.add_image(F.buff)

                    if "footprint" in pdf_plots_list:
                        Pr.update("Footprint")
                        R.add_page()
                        R.add_title("Footprint")
                        R.add_par(J.post("footprint"))
                        
                        if not data["footprint_radius"].isnull().all():
                            with Figure(size=(10,11), layout=(3,1),
                                    **kw_fig_buff, **kw_fig_time,
                                    x_ticks_last=True) as F:
                                
                                ax = F.axes[0]
                                var = 'footprint_radius'
                                ax.fill_between(data.index, data[var], 0,
                                                color='C2', alpha=0.3, lw=0, step='post')
                                ax.plot(data.index, data[var],
                                        color='C2', drawstyle='steps-post', lw=2,
                                        label="Footprint radius")
                                ax.set_ylim(0,350)
                                ax.set_ylabel("Radius (in m)")

                                ax = F.axes[1]
                                var = 'footprint_depth'
                                ymin = np.min([-80,-data[var].max()])
                                ax.fill_between(data.index, -data[var], 0,
                                                color='sienna', alpha=0.3, lw=0, step='post')
                                ax.plot(data.index, -data[var],
                                        color='sienna', drawstyle='steps-post', lw=2,
                                        label="Footprint depth")
                                ax.set_ylim(ymin,0)
                                ax.set_ylabel("Depth (in cm)")

                                ax = F.axes[2]
                                var = 'footprint_volume'
                                ax.fill_between(data.index, data[var], 0,
                                                color='C0', alpha=0.3, lw=0, step='post')
                                ax.plot(data.index, data[var],
                                        color='C0', drawstyle='steps-post', lw=2,
                                        label="Footprint volume")
                                ax.set_ylim(0,120)
                                ax.set_ylabel("Volume (in 10³ m³)")

                                for ax in F.axes:
                                    ax.grid(color="k", alpha=0.1)
                                    ax.set_xlim(date1, date2)
                                    ax.legend(loc="upper left", bbox_to_anchor=(0, 1.12),
                                            ncol=3, fancybox=False, framealpha=1, edgecolor="k",
                                            shadow=True)
                                    if ax != F.axes[-1]:
                                        ax.set_xticklabels([])
                        
                            R.add_image(F.buff)

                    if "diurnal" in pdf_plots_list:
                        Pr.update("Diurnal cycles")
                        R.add_page()
                        R.add_title("Diurnal cycles")

                        if data.index.to_series().dropna().diff().median().total_seconds() > 3600:
                            print('! Diurnal plots are only possible if temporal resolution is < 1 hour.')
                            R.add_par("Diurnal plots are only possible if temporal resolution is < 1 hour.")
                        
                        elif not data[smv].isnull().all():
                            data_diurnal = data.copy()
                            data_diurnal['Time'] = data_diurnal.index.map(lambda x: x.strftime("%H:%M"))
                            data_diurnal = data_diurnal.groupby('Time').describe().unstack()

                            with Figure(size=(10,11), layout=(3,1), save="buff") as F:
                                
                                ax = F.axes[0]
                                ax.plot(data_diurnal[Nfinal]['mean'].index,
                                        data_diurnal[Nfinal]['mean'],
                                        color='C0', drawstyle='steps-post',
                                        label='Diurnal cycle of neutrons epithermal')
                                ax.fill_between(data_diurnal[Nfinal]['mean'].index,
                                                data_diurnal[Nfinal]['25%'],
                                                data_diurnal[Nfinal]['75%'],
                                                color='C0', alpha=0.2, lw=0,
                                                step='post', label='Quantiles 25%--75%')
                                if Nth:
                                    bias = (data_diurnal[Nfinal]['mean'] - data_diurnal[Nthfinal]['mean']).mean()
                                    ax.plot(data_diurnal[Nthfinal]['mean'].index,
                                        data_diurnal[Nthfinal]['mean'] + bias,
                                        color='C1', drawstyle='steps-post',
                                        label="thermal ({:+.0f} cph)".format(bias))
                                    ax.fill_between(data_diurnal[Nthfinal]['mean'].index,
                                                    data_diurnal[Nthfinal]['25%'] + bias,
                                                    data_diurnal[Nthfinal]['75%'] + bias,
                                                    color='C1', alpha=0.2, lw=0,
                                                    step='post')
                                ax.set_ylabel("Neutron counts (in cph)")

                                ax = F.axes[1]
                                ax.plot(data_diurnal[smv]['mean'].index,
                                        data_diurnal[smv]['mean'],
                                        color='C0', drawstyle='steps-post',
                                        label='Diurnal cycle of water content')
                                ax.fill_between(data_diurnal[smv]['mean'].index,
                                                data_diurnal[smv]['25%'],
                                                data_diurnal[smv]['75%'],
                                                color='C0', alpha=0.2, lw=0,
                                                step='post', label='Quantiles 25%--75%')
                                ax.set_ylabel("Water content (in m³/m³)")

                                ax = F.axes[2]
                                ax.plot(data_diurnal[ah]['mean'].index,
                                        data_diurnal[ah]['mean'],
                                        color='C9', drawstyle='steps-post',
                                        label='Diurnal cycle of air humidity')
                                ax.fill_between(data_diurnal[ah]['mean'].index,
                                                data_diurnal[ah]['25%'],
                                                data_diurnal[ah]['75%'],
                                                color='C9', alpha=0.2, lw=0,
                                                step='post', label='Quantiles 25%--75%')
                                ax.set_ylabel("Air humidity (in g/m³)")
                                ax.set_xlabel("Hour of the day")

                                for ax in F.axes:
                                    ax.set_xticks(range(0,24))
                                    ax.set_xticklabels(range(0,24))
                                    ax.set_xlim(0,23)
                                    ax.grid(color="k", alpha=0.1)
                                    ax.legend(loc="upper left", bbox_to_anchor=(0, 1.07),
                                            ncol=3, fancybox=False, framealpha=1, edgecolor="k",
                                            shadow=True)
                                    if ax != F.axes[-1]:
                                        ax.set_xticklabels([])
                            
                            R.add_image(F.buff)

                    if "tubes" in pdf_plots_list:
                        Pr.update("Tubes")
                        R.add_page()
                        R.add_title("Tubes")
                        R.add_par("Data shown for each active tube. Red dots are raw data, grey dots are cleaned data. Black line is smoothed, red line is interpolated.")
                        
                        N_tubes = xsplit(config['input']['neutron_columns'])
                        if cc(config, 'input', 'ignore_neutron_columns'):
                            N_tubes_ignored = xsplit(config['input']['ignore_neutron_columns'])
                            N_tubes = [item for item in N_tubes if item not in N_tubes_ignored]
                        
                        mysize = np.ceil(np.sqrt(len(N_tubes)))
                        if len(N_tubes) == 1:
                            mylayout = (2,1)
                        elif len(N_tubes) == 2:
                            mylayout = (2,1)
                        else:
                            mylayout = (int(mysize),int(mysize))
                        ymax = data[N_tubes[0]].mean()*5
                        
                        smooth_window = 6*15
                        for N_col in N_tubes:
                            data[N_col+'_mov'] = mavg(data[N_col+'_clean'], smooth_window)

                        with Figure(size=(10,10), layout=mylayout,
                                save="buff", format="png",
                                **kw_fig_time,
                                # x_major_ticks="hours", x_minor_ticks=None, x_major_fmt="%H", x_minor_fmt=""
                                ) as F:
                            
                            i = -1
                            for var in N_tubes:
                                i += 1
                                ax = F.axesflat[i]
                                
                                ax.plot(data.index, data[var], color='red', ls='', marker='o', ms=2, markeredgewidth=0)
                                ax.plot(data.index, data[var+'_clean'], color='silver', ls='', marker='o', ms=2.1, markeredgewidth=0,
                                        label=var)

                                if cc(config,'clean','smooth'):
                                    rollobj = data[var+'_clean'].interpolate(limit=gc(config,'clean','interpolate_neutrons_gapsize',dtype=int)).rolling(int(config['clean']['smooth']), center=True)
                                    ax.plot(data.index, rollobj.mean(), color='red', ls='-', lw=1)
                                    rollobj = data[var+'_clean'].rolling(int(config['clean']['smooth']), center=True)
                                    ax.plot(data.index, rollobj.mean(), color='black', ls='-', lw=1)
                                
                                ax.grid(color="k", alpha=0.1)
                                ax.set_ylim(0,ymax)
                                ax.set_xlim(date1, date2)
                                ax.legend(loc="upper left", bbox_to_anchor=(0, 1.12),
                                            ncol=3, fancybox=False, framealpha=1, edgecolor="k",
                                            shadow=True)
                                if i==0:
                                    ax.set_ylabel('raw counts')

                            for j in range(i+1, len(F.axesflat)):
                                    F.axesflat[j].set_axis_off()

                        R.add_image(F.buff)


                    if "map_xy"   in pdf_plots_list or \
                       "map_grid" in pdf_plots_list:
                        
                        var = sm

                        Pr.update("Map xy")
                        R.add_page()
                        R.add_title("Map (xy) of %s" % var)
                        
                        if not data[var].isnull().all() and lat != '' and lat in data.columns:

                            from pyproj import Transformer

                            datax = data[[lon,lat,var]].dropna()
                            if len(datax)==0:
                                raise ValueError('No records with valid sm left after processing, stop printing')

                            ys, xs = Transformer.from_crs("epsg:4326", "epsg:31468").transform(
                                datax[lat].values, datax[lon].values)
                            datax['x'] = xs
                            datax['y'] = ys
                            
                            # relative values
                            x = datax.x.values - datax.x.min()
                            y = datax.y.values - datax.y.min()
                            z = datax[var].values

                            # config
                            xmax = np.nanmax(x[np.isfinite(x)])
                            xmin = np.nanmin(x[np.isfinite(x)])
                            ymax = np.nanmax(x[np.isfinite(x)])
                            ymin = np.nanmin(x[np.isfinite(x)])
                            xrange = xmax - xmin
                            yrange = ymax - ymin
                            if xrange==0: xrange=1
                            if yrange==0: yrange=1
                            xpad = 0.05*xrange
                            ypad = 0.10*yrange
                            mysize = (10,10/xrange*yrange)
                            if mysize[1] > 20:
                                mysize = (10*xrange/yrange,10)
                            resolution = xrange / 15.0
                            contour_levels = np.arange(0.05,0.525,0.025) #20
                            
                            with Figure(size=mysize, save="buff", format="png",) as F:
                                
                                ax = F.axes
                                ax.set_xlabel('Easting (in m)')
                                ax.set_ylabel('Northing (in m)')

                                # Points
                                ax.plot(x, y,
                                        color="k", lw=1, ls=":", alpha=0.2, zorder=1)
                                q = ax.scatter(x, y,
                                               c=z, cmap='Spectral', vmin=smrange[0], vmax=smrange[1],
                                               zorder=2)
                                ax.set_aspect(1)
                                ax.set_xlim( xmin - xpad, xmax + xpad)
                                ax.set_ylim( ymin - ypad, ymax + ypad)
                                add_colorbar(ax, points=q, label="vol. soil moisture (in m³/m³)")
                                ax.grid(color='black', alpha=0.1)

                                if cc(config,'conversion','poi_plot','yes') and len(poi_data)>0:
                                    for i, poi in poi_data.iterrows():
                                        ax.scatter(poi.utmx - datax.x.min(),
                                                   poi.utmy - datax.y.min(),
                                                   c='k', marker='+', s=100)
                                        add_circle(ax,
                                            poi.utmx- datax.x.min(),
                                            poi.utmy- datax.y.min(),
                                            poi.radius, "none", "k", "--")


                            R.add_image(F.buff)

                    if "map_tiles" in pdf_plots_list:
                        
                        var = sm

                        Pr.update("Map tiles")
                        R.add_page()
                        R.add_title("Map (tiles) of %s" % var)
                        
                        if not data[var].isnull().all() and lat != '' and lat in data.columns:

                            config_zoom = gc(config,'output','satellite_zoom', 8, dtype=int)
                            config_map = 'satellite'
                            config_markersize = 15

                            from pyproj import Transformer

                            datax = data[[lon,lat,var]].dropna()
                            if len(datax)==0:
                                raise ValueError('No records with valid sm left after processing, stop printing')

                            ys, xs = Transformer.from_crs("epsg:4326", "epsg:31468").transform(
                                datax[lat].values, datax[lon].values)
                            datax['x'] = xs
                            datax['y'] = ys
                            
                            # relative values
                            x = datax.x.values - datax.x.min()
                            
                            datall = data.dropna(subset=[sm]).copy()

                            # if (x.max()-x.min() > -25000*config_zoom+401000) or (y.max()-y.min() > -25000*config_zoom+401000):
                            #     print('! WARNING: map zoom level too high for the given spatial extent of the data, downloading tiles might take a long time.') 
                            lonpad = 0.1*(datall[lon].max()-datall[lon].min())
                            latpad = 0.1*(datall[lat].max()-datall[lat].min())

                            # data coordinates and values
                            x = datax.x.values - datax.x.min()
                            y = datax.y.values - datax.y.min()
                            z = datax[var].values

                            xrange = x.max()-x.min()
                            yrange = y.max()-y.min()
                            if xrange==0: xrange=1
                            if yrange==0: yrange=1
                            xpad = 0.05*xrange
                            ypad = 0.10*yrange

                            mysize = (10,10/xrange*yrange)
                            if mysize[1] > 20:
                                mysize = (10*xrange/yrange,10)

                            zoom = config_zoom #18-int(15*(data[lon].max()-data[lon].min())) # 0->18, 0.5->16, 1->14
                            # Do not choose >=17 for more than a small field site! Python will crash.
                            #print("(zoom %i) " % zoom, end='')

                            extent = [datall[lon].min()-lonpad, datall[lon].max()+lonpad, datall[lat].min()-latpad, datall[lat].max()+latpad]

                            with Figure(size=mysize, save="buff", format="png",
                                        projection="flat", zoom=config_zoom,
                                        tiles=gc(config, "output", "tiles", alt="satellite-ms"),
                                        ) as F:
                                
                                ax = F.axes
                                ax.set_xlabel('Longitude')
                                ax.set_ylabel('Latitude')

                                ax.scatter( datall[lon], datall[lat],
                                           c='black', transform=ccrs.PlateCarree(),
                                           s=config_markersize*2)
                                q = ax.scatter( datall[lon], datall[lat],
                                               c=datall[smv], cmap='Spectral',
                                               vmin=smrange[0], vmax=smrange[1],
                                               s=config_markersize,
                                               transform=ccrs.PlateCarree())
                                
                                add_colorbar(ax, points=q, label="vol. soil moisture (in m³/m³)")

                                if cc(config,'conversion','poi_plot','yes') and len(poi_data)>0:
                                    for i, poi in poi_data.iterrows():
                                        ax.scatter( poi.lon, poi.lat, c='black', marker='+', s=100, transform=ccrs.PlateCarree())

                            R.add_image(F.buff)
                    

                    if "map_interpolation" in pdf_plots_list:
                        
                        var = sm

                        Pr.update("Map interpolation")
                        if not data[var].isnull().all() and lat != '' and lat in data.columns:

                            from corny.figures import make_bbox, Kriging
                            from corny.exdata.openstreetmap import OsmGraph

                            # Reduce data to the relevant pieces
                            data_x = data[[lat,lon,var]].dropna()

                            # Config
                            resolution   = gc(config, "output", "interpolation_ll_resolution", alt=0.01, dtype=float)
                            z_resolution = gc(config, "output", "interpolation_sm_resolution", alt=0.02, dtype=float)
                            variogram    = gc(config,"output","variogram", alt="spherical")

                            # Setup up Kriging instance
                            OKI = Kriging(
                                data_x[lon], data_x[lat],
                                data_x[var],
                                resolution = resolution,
                                z_min=smrange[0], z_max=smrange[1],
                                z_resolution=z_resolution)

                            # Run Kriging operator, creates OKI.z_grid
                            OKI.execute(variogram=variogram)

                            # Export as ESRI ASCII grid file
                            if cc(config, "output", "interpolation_export","yes"):
                                OKI.export_asc(output_basename + '.asc')
                                file_save_msg(
                                    output_basename + '.asc',
                                    name="ASCII Grid",
                                    end="\n")

                            # Make bounding box from data
                            bbox = make_bbox(
                                data_x[lon].values,
                                data_x[lat].values,
                                pad=resolution)

                            # Download map features
                            plot_rivers = cc(config, "output", "interpolation_plot_rivers","yes")
                            plot_roads  = cc(config, "output", "interpolation_plot_roads","yes")
                            if plot_rivers:
                                Rivers = OsmGraph("rivers", bbox).get()
                            if plot_roads:
                                Roads  = OsmGraph("roads",  bbox).get()

                            R.add_page()
                            R.add_title("Map (interpolated) of %s" % var)
                            R.add_par("Kriging map of water content created with Ordinary Kriging and a %s variogram model. The resolution of the interpolated grid has been set to %.4f° (~%.1f meters)." \
                                      % (variogram, OKI.resolution, OKI.resolution_m))

                            # Create the plot
                            with Figure(
                                size   = (9,9),
                                layout = (1,1),
                                projection = "flat",
                                extent = bbox,
                                save="buff", format="png"
                                ) as F:

                                ax = F.axes
                                # Plot interpolated soil moisture values
                                OKI.plot_values(ax)
                                # Plot map features
                                if plot_rivers: Rivers.plot(ax)
                                if plot_roads:  Roads.plot(ax)
                            
                            R.add_image(F.buff)
                            R.add_page()
                            R.add_title("Map (interpolated) of Variance")
                            R.add_par("Quality of the Kriging interpolation expressed as variance. The variance is lowest near the data points and increases with distance, indicating the increasing uncertainty of the interpolation map with distance from the actual data.") 

                            with Figure(
                                size   = (9,9),
                                layout = (1,1),
                                projection = "flat",
                                extent = bbox,
                                save="buff", format="png"
                                ) as F:

                                ax = F.axes
                                # Plot variance map (quality of interpolation)
                                OKI.plot_variance(ax)
                                # Plot map features
                                if plot_rivers: Rivers.plot(ax)
                                if plot_roads:  Roads.plot(ax)
                            
                            R.add_image(F.buff)


                    if "roads" in pdf_plots_list:
                        
                        Pr.update("Roads")
                        if not data[var].isnull().all() and lat != '' and lat in data.columns:

                            from corny.figures import make_bbox
                            from corny.exdata.openstreetmap import OsmGraph

                            # Reduce data to the relevant pieces
                            data_x = data[[lat,lon]].dropna()
                            if "road" in data.columns:
                                data_x["road"] = data["road"].fillna("none")

                                # Set colors for road types
                                data_x["road_color"] = [OsmGraph.road_colors[r] if r in OsmGraph.road_colors else "none" for r in data_x["road"]]
                            else:
                                data_x["road_color"] = "black"

                            # Config
                            resolution   = gc(config, "output", "interpolation_ll_resolution", alt=0.001, dtype=float)
                            
                            # Make bounding box from data
                            bbox = make_bbox(
                                data_x[lon].values,
                                data_x[lat].values,
                                pad=resolution)
                            
                            Roads  = OsmGraph("roads", bbox).get()

                            R.add_page()
                            R.add_title("Map of roads")
                            R.add_par("The road network and road types have been downloaded from OpenStreetMap. Data within ~7 m received the road type attribute.")

                            # Create the plot
                            with Figure(
                                size   = (9,9),
                                # projection = "flat",
                                extent = bbox,
                                save="buff", #format="png"
                                ) as F:

                                ax = F.axes
                                Roads.plot_road_types(ax=ax)

                                data_x0 = data_x[data_x.road == "none"]
                                data_x1 = data_x[data_x.road != "none"]
                                ax.scatter(
                                    data_x0[lon], data_x0[lat],
                                    c=data_x0["road_color"], edgecolor="grey", lw=0.5, s=30,
                                    zorder=2)
                                ax.scatter(
                                    data_x1[lon], data_x1[lat],
                                    c=data_x1["road_color"], s=30,
                                    zorder=3)
                            
                            R.add_image(F.buff)


                    for pdf_plots_var in pdf_plots_list:

                        if pdf_plots_var in ["footprint", "tubes", "diurnal"]:
                            continue
                        
                        if not pdf_plots_var in data.columns or data[pdf_plots_var].isnull().all():
                            continue
                        
                        R.add_page()
                        R.add_title(pdf_plots_var)
                        with Figure(size=(10,11), layout=(1,1),
                                    **kw_fig_buff, **kw_fig_time) as F:
                            
                            ax = F.axes
                            ax.set_ylabel(pdf_plots_var)
                            ax.grid(color="k", alpha=0.1)
                            ax.set_xlim(date1, date2)

                            # Line styles
                            myls = gc(config, "output", "PDF_line_style")
                            if myls == "lines":
                                styles_dict = dict(ls='-')
                            elif myls == "steps":
                                styles_dict = dict(ls='-', drawstyle='steps')
                            else: # points
                                styles_dict = dict(ls='', marker='o', ms=5, markeredgewidth=0)

                            # Plot
                            ax.plot(data.index, data[pdf_plots_var], **styles_dict)
                        
                        R.add_image(F.buff)
                    
                    R.add_page()
                    R.add_title("Notes")

                    # Todo: Include this if a copy of the config was requested
                    # R.add_page()
                    # R.add_title("Configuration used in this report")
                    
                    # for section in config:
                    #     R.add_title("%s" % section, style="")
                    #     config_section_table = pandas.DataFrame([], columns=["parameter","value"])
                    #     for key in config[section]:
                    #         row = pandas.DataFrame([[key, config[section][key]]],
                    #                                columns=["parameter","value"])
                    #         config_section_table = pandas.concat([config_section_table, row])
                    #     R.add_table(config_section_table.astype(str), align="LEFT")
                    
                    # R.add_page()
                    Pr.update("Save file")

            file_save_msg(figure_name2, end="\n")


        if cc(config,'output','make_PNG','yes'):
            if not data[smv].isnull().all():
                print('| Creating PNG for %s... ' % smv)

                if 'map_tiles_flyer' in xsplit(config['output']['PNG_plots']):

                    var = sm
                    if 'PDF_sm_range' in config['output']:
                        smrange = xsplit(config['output']['PDF_sm_range'], range=2, type=float)
                    else:
                        smrange = (0,1)

                    # TODO Those params should go to the config file
                    todo_config_zoom = gc(config,'output','satellite_zoom', 8, dtype=int)
                    todo_config_map = 'satellite'
                    todo_config_markersize = 15

                    print('Latlon plot with tiles...')
                    datall = data.dropna(subset=[sm]).copy()

                    if todo_config_map == 'satellite':
                        cimgt.QuadtreeTiles.get_image = image_spoof # reformat web request for street map spoofing
                        osm_img = cimgt.QuadtreeTiles() # spoofed, downloaded street map
                    elif todo_config_map == 'OSM':
                        cimgt.OSM.get_image = image_spoof # reformat web request for street map spoofing
                        osm_img = cimgt.OSM() # spoofed, downloaded street map
                    else:
                        todo_config_map = None

                    lonpad = 0.1*(datall[lon].max()-datall[lon].min())
                    latpad = 0.3*(datall[lat].max()-datall[lat].min())

                    datax = data[[lon,lat,var]].dropna()
                    if len(datax)==0:
                        raise ValueError('No records with valid sm left after processing, stop printing')

                    ## Deprecated:
                    # import pyproj
                    # xs, ys = pyproj.transform(pyproj.Proj("epsg:4326"), pyproj.Proj("epsg:31468"), datax[lat].values, datax[lon].values)
                    ## New pyproj2 syntax:
                    from pyproj import Transformer
                    xs, ys = Transformer.from_crs("epsg:4326", "epsg:31468").transform(datax[lat].values, datax[lon].values)

                    datax['y'] = xs
                    datax['x'] = ys

                    # data coordinates and values
                    x = datax.x.values - datax.x.min()
                    y = datax.y.values - datax.y.min()
                    z = datax[var].values

                    #smrange = xsplit(config['output']['PDF_sm_range'], range=2, type=float)

                    xrange = x.max()-x.min()
                    yrange = y.max()-y.min()
                    if xrange==0: xrange=1
                    if yrange==0: yrange=1
                    xpad = 0.05*xrange
                    ypad = 0.10*yrange

                    mysize = (10,10/xrange*yrange)
                    if mysize[1] > 10:
                        mysize = (10*xrange/yrange,10)

                    zoom = todo_config_zoom #18-int(15*(data[lon].max()-data[lon].min())) # 0->18, 0.5->16, 1->14
                    # Do not choose >=17 for more than a small field site! Python will crash.
                    #print("(zoom %i) " % zoom, end='')

                    filename = 'pics/RR1-%s.png' % data.index[1].strftime('%Y%m%d')
                    #Neutrons-On-Rails: CRNS/RR1 Blankenburg--Rübeland (HVLE)
                    with Fig(title='', xlabel='lon', ylabel='lat', size=mysize, time_series=False,
                        proj=ccrs.PlateCarree(), savefile=filename) as ax:
                        ax.scatter( datall[lon], datall[lat], c='black', transform=ccrs.PlateCarree(), s=todo_config_markersize*2)
                        q = ax.scatter( datall[lon], datall[lat], c=datall[sm], transform=ccrs.PlateCarree(),
                                        s=todo_config_markersize, cmap='Spectral', vmin=smrange[0], vmax=smrange[1])
                        ax.set_xlim(bbox[0], bbox[1])
                        ax.set_ylim(bbox[2], bbox[3])
                        if not todo_config_map is None:
                            ax.add_image(osm_img, zoom)

                        #cb = plt.colorbar(q, ax=ax, shrink=0.4, pad=0.03, aspect=30)
                        #cb.set_label("vol. soil moisture (in m$^3$/m$^3$)", rotation=270, labelpad=20, fontsize=7)

                        #ax.scatter( datall[lon][-1], datall[lat][-1], c='white', marker='+', s=200, transform=ccrs.PlateCarree())
                        #ax.text( datall[lon][-1], datall[lat][-1]+0.01, data.index[-1].strftime('%H:%M'), c='white', transform=ccrs.PlateCarree(),
                        #        fontsize=9, ha='center') #, bbox=dict(facecolor='white', edgecolor='black')) #, boxstyle='round'
                        from matplotlib import cm
                        cmap = cm.get_cmap('Spectral')
                        c = datall[sm].mean()/smrange[1]
                        ax.text( 0.98*bbox[0]+0.02*bbox[1], bbox[3]-(bbox[3]-bbox[2])*0.1,
                                'Average Soil Water on %s' % (data.index[1].strftime('%A, %b %d, %Y')),
                                c='white', transform=ccrs.PlateCarree(), fontsize=9, ha='left', bbox=dict(facecolor='none', edgecolor='none'))
                        ax.text( 0.98*bbox[0]+0.02*bbox[1], bbox[3]-(bbox[3]-bbox[2])*0.29,
                                'Footprint width: %.0f m, depth: 0 to %.0f cm' % (2*datall['footprint_radius'].mean(), datall['footprint_depth'].mean()),
                                c='white', transform=ccrs.PlateCarree(), fontsize=9, ha='left', bbox=dict(facecolor='none', edgecolor='none')) #, boxstyle='round'
                        ax.text( bbox[0]+(bbox[1]-bbox[0])*0.02, bbox[3]-(bbox[3]-bbox[2])*0.19,
                                '$%2.0f\,$%% (agriculture)' % (100*datall.loc[datall.luse_str=='agriculture',sm].mean()),
                                c='black', transform=ccrs.PlateCarree(), fontsize=9, ha='left',
                                bbox=dict(facecolor=cmap(datall.loc[datall.luse_str=='agriculture',sm].mean()/smrange[1]), edgecolor='none',boxstyle='square,pad=0.1')) #, boxstyle='round'
                        ax.text( bbox[0]+(bbox[1]-bbox[0])*0.255, bbox[3]-(bbox[3]-bbox[2])*0.19,
                                '$%2.0f\,$%% (forest)' % (100*datall.loc[datall.luse_str=='forest',sm].mean()),
                                c='black', transform=ccrs.PlateCarree(), fontsize=9, ha='left',
                                bbox=dict(facecolor=cmap(datall.loc[datall.luse_str=='forest',sm].mean()/smrange[1]), edgecolor='none',boxstyle='square,pad=0.1')) #, boxstyle='round'
                        ax.text( bbox[0]+(bbox[1]-bbox[0])*0.425, bbox[3]-(bbox[3]-bbox[2])*0.19,
                                '$%2.0f\,$%% (urban)' % (100*datall.loc[datall.luse_str=='urban',sm].mean()),
                                c='black', transform=ccrs.PlateCarree(), fontsize=9, ha='left',
                                bbox=dict(facecolor=cmap(datall.loc[datall.luse_str=='urban',sm].mean()/smrange[1]), edgecolor='none',boxstyle='square,pad=0.1')) #, boxstyle='round'
                        ax.text( bbox[1]-(bbox[1]-bbox[0])*0.5, bbox[2]+(bbox[3]-bbox[2])*0.05,
                                'Cosmogenic Neutrons on Rails (UFZ, Uni Potsdam, HVLE)',
                                c='white', transform=ccrs.PlateCarree(), fontsize=6, ha='left', bbox=dict(facecolor='none', edgecolor='none'))
                    file_save_msg(filename, 'PNG', end="\n")

                    if cc(config,'output','PNG_upload_ftp_path'):

                        import ftplib
                        remote = ftplib.FTP(gc(config,'update','FTP_server'))
                        print('| FTP connection OK', end='')
                        remote.login(gc(config,'update','FTP_user'), gc(config,'update','FTP_pswd'))
                        print(', login OK', end='')
                        local_file = open(filename, "rb")
                        remote_file = gc(config,'output','PNG_upload_ftp_path') + os.path.basename(filename)
                        print(', store %s' % remote_file, end='')
                        remote.storbinary("STOR %s" % remote_file, local_file)
                        print(' OK (%.0f B), chmod 0644' % remote.size(remote_file), end='')
                        xcmd = remote.sendcmd('SITE CHMOD 644 %s' % remote_file)
                        print(' OK (%s).' % xcmd)
                        remote.close()




        # Make KML if lat/lon is provided
        if lat in data.columns and lon in data.columns:

            if yesno2bool(config['output']['make_KML_neutrons']):
                if not data[Nfinal].isnull().all():
                    print('| Creating_KML for %s... ' % Nfinal)
                    collim = xsplit(config['output']['KML_neutrons_range'])
                    if len(collim)<=1: collim = None #alt=alt, 
                    save_KML(data, Nfinal, lat, lon, file=output_basename+'-'+config['correction']['new_neutron_column'], format_str='%i', cmap='Spectral', collim=collim)#(3000,9000)) # long track: 5000,11000
                    file_save_msg(output_basename+'-'+config['correction']['new_neutron_column']+'.kml', 'KML', end="\n")


    #            if Nth:
    #                print("\n"+'Creating_KML... ', end='')
    #                save_KML(data, Nth, lat, lon, file=output_basename+'-'+config['correction']['new_neutron_column']+'_thermal', format_str='', cmap='Spectral') #, collim=(3000,9000)) # long track: 5000,11000
    #                file_save_msg(output_basename+'-'+config['correction']['new_neutron_column']+'_thermal.kml', '', end='')

            if yesno2bool(config['output']['make_KML_sm']):
                if not data[sm].isnull().all():
                    print('| Creating_KML for %s... ' % sm)
                    collim = xsplit(config['output']['KML_sm_range'])
                    if len(collim)<=1:
                        collim = None
                    else:
                        collim[0] *= 100
                        collim[1] *= 100 #alt=alt, 
                    save_KML(data, sm+'_%', lat, lon, file=output_basename+'-'+sm, format_str='%.0f%%', collim=collim)#(5,80))
                    file_save_msg(output_basename+'-'+sm+'.kml', 'KML', end="\n")

            if yesno2bool(config['output']['make_KML_other']):
                other = config['output']['KML_other_column']
                if other in data.columns:
                    if not data[other].isnull().all():
                        print('| Creating_KML for %s... ' % other)
                        collim = xsplit(config['output']['KML_other_range'])
                        if len(collim)<=1: collim = None
                        if yesno2bool(config['output']['KML_other_reverse']):
                            cmap = 'Spectral_r'
                        else:
                            cmap = 'Spectral'
                        format_str = '%'+config['output']['KML_other_format'] if config['output']['KML_other_format'] else ''
                        save_KML(data, other, lat, lon, alt=alt, file=output_basename+'-'+other, format_str=format_str, cmap=cmap, collim=collim)#(5,80))
                        file_save_msg(output_basename+'-'+other+'.kml', 'KML', end="\n")
                else:
                    print('Cannot plot additional KML variable: %s is not in [%s]!' %  (other, ', '.join(data.columns)))
                        # save also ID file
                        #save_KML(data, 'RecordNum', lat, lon, alt='Alt', file=output_basename+'-ID', format_str='%i')

         # Create the footprint geometries
        if cc(config,'output','make_footprint_polygons','yes'):
            print('| Creating footprint polygons...')
            #footprint_radius= gc(config, 'input','footprint_radius',dtype=float)
            #footprint_type=gc(config, 'input','footprint_type')
            #if footprint_type=='polygon':
            footprint.retrieve_footprints(data, footprint_radius = data.footprint_radius.mean(), lon=lon,lat=lat,
                                          output=os.path.join(output_basename+'_footprints.geojson'))
            file_save_msg(os.path.join(output_basename+'_footprints.geojson'), 'Polygons', end="\n")

        plt.close('all')
        Nfinal = Nfinal_before_loop

    # end loop datasets

    if True:
        # Backup Config
        try:
            import shutil
            shutil.copyfile(configfile_abspath, output_basename+'.cfg')
            file_save_msg(output_basename + '.cfg', "Copy of config", end="\n")
        except Exception as e:
            print('! Error: Cannot backup the config file:', e)
    

    if yesno2bool(gc(config,'output','make_summary')):
        Summary.loc['mean']   = Summary.mean()
        Summary.loc['median'] = Summary.median()
        Summary.loc['stdev']  = Summary.std()
        try:
            Summary.to_csv(output_basename_notrack+'_summary.csv',
                date_format = datetime_format, header=Summary.columns,
                sep = config['output']['CSV_column_sep'].strip('\"'),
                decimal = config['output']['CSV_decimal'],
                float_format = '%' + config['output']['CSV_float_format'],
                na_rep = config['output']['CSV_NaN'])
        except Exception as e:
            print('! ERROR: Cannot write, probably permission denied.', e)

        file_save_msg(output_basename_notrack + '_summary.csv', "Summary", end="\n")


    print("= Done.")
    return(data)

# Direct call (instead of import)

if __name__ == "__main__":
    param2 = ''
    if len(sys.argv) > 1:
        cfg_file = sys.argv.pop(1)
        if len(sys.argv) > 1:
            cfg_prefix = sys.argv.pop(1)
            main(cfg_file, cfg_prefix)
        else:
            main(cfg_file)
        if len(sys.argv) > 1:
            param2 = sys.argv.pop(1)
    else:
        main()

    #if param2 != '--nonstop':
    #    print("Press any key to close.", end='')
    #    input()
