# Correction for Desilets and McJannet, doi:10.1029/2022WR033889
import numpy as np

def local_gravity(lon, lat, alt):
    """
    local gravity at coordinate 
    """
    dens_rock = 2670
    g1 = 9.780327*(1+0.0053024*np.sin(lat/180*3.141)**2-0.0000058*np.sin(2*lat/180*3.141)**2)
    g2 = -3.086*10**-6*alt
    g3 = 4.194*10**-10 *dens_rock*alt
    g = g1 + g2 + g3
    
    return(g)

def local_air_pressure(lon, lat, alt):
    """
    local air pressure at coordinate 
    """
    g = local_gravity(lon, lat, alt)
    p_ref = 1013.25*(1+(-0.0065/288.15)*alt)**((-g*0.0289644)/(8.31432*-0.0065))
    
    return(p_ref)

def Tau(lat, lon, alt, Rc):
    """
    Correction factor Tau
    Example:
        Tau(51,180,0,1)
    """
    c0 = -0.0009
    c1 = 1.7699
    c2 = 0.0064
    c3 = 1.8855
    c4 = 0.000013
    c5 = -1.2237
    eps = 1
    
    Rc_JUNG = 4.49
    atm_depth_JUNG = 665.18
    tau_JUNG = eps*1*(c0*atm_depth_JUNG+c1)*(1-np.exp(-(c2*atm_depth_JUNG+c3)*Rc_JUNG**(c4*atm_depth_JUNG+c5)))
    K_JUNG = 1/tau_JUNG

    g = local_gravity(lon, lat, alt)
    p_ref = local_air_pressure(lon, lat, alt)
    atm_depth = 10*p_ref/g
    tau = eps*K_JUNG*(c0*atm_depth+c1)*(1-np.exp(-(c2*atm_depth+c3)*Rc**(c4*atm_depth+c5)))
    return(tau)
