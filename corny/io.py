import os
from os.path import join, abspath, dirname, realpath
import sys
from .basics import Progress

# def file_save_msg(file, name='File', end=''):
#     size = os.stat(file).st_size
#     if   size < 1024:    sizestr = "%.0f Bytes" % (size)
#     elif size < 1024**2: sizestr = "%.0f KB"    % (size/1024)
#     else:                sizestr = "%.1f MB"    % (size/1024/1024)
#     r = "< %s saved to %s (%s)" % (name, file, sizestr)
#     #rc = cprintf(r, 'green')
#     print(r, end=end)
#     #return(rc)
    
def make_dir(dir):
    if not os.path.exists(dir):
        with Progress('Creating folder %s' % dir):
            os.makedirs(dir)

def format_path(dir):
    if os.path.isdir(dir):
        # add trailing slash
        return(os.path.join(dir, ''))
    else:
        return(dir)

def get_app_path():

    if getattr(sys, 'frozen', False):
        # application_path = abspath(dirname(dirname(realpath(sys.executable))))
        path_of_script = dirname(realpath(sys.executable)) #sys._MEIPASS
    elif __file__:
        path_of_script = dirname(__file__)
        # application_path = abspath(dirname(dirname(__file__)))
    
    return(abspath(dirname(path_of_script)))

def app_file_path(file):

    path_of_script = get_app_path()
    file_path = join(path_of_script, file)
    return(file_path)
