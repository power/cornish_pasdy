"""
CoRNy Smoothing
    Support spatial and temporal averaging and aggregation
"""

# %%
import numpy as np
import pandas
from .corn import Wr_approx
#from corny.corn import Wr_approx

# %%

nearby_avg_rs = np.linspace(0.1,200,2000)
nearby_avg_ws = Wr_approx(nearby_avg_rs)
nearby_avg_ws /= nearby_avg_ws.sum()

# %%
def filter_nearby(data, rstr='r', xstr='x', ystr='y',
    max_radius=20, max_points=None, or_nearest=False):
    """
    Given distance r or (x,y) for every row in a DataFrame,
    cut all data that is within a given max_radius.
    Optionally, limit it to max_points.
    If the resulting dataset is empty, optionally take the nearest distance.
    Then, make sure that for every r there is only one value (i.e., average else).
    Usage:
        data = filter_nearby(data, max_radius=200)
    """
    
    if rstr is None:
        rstr = 'r'
        data[rstr] = np.sqrt(data[xstr]**2 + data[ystr]**2)
    #print(vstr, rstr, max_radius, rstr in data.columns, data[rstr].median())
    # cut to max_radius
    data = data[data[rstr] <= max_radius].copy()
    
    # Take only nearest x points
    if not max_points is None:
        data = data.sort_values([rstr])[:int(max_points)] 

    if or_nearest:
        if len(data) == 0:
            data = data.loc[ data[rstr]==data[rstr].min() ].copy()
    #print(data[rstr].median(), len(data[rstr]))
    # replace duplicate r by their average
    data = data.groupby(rstr, as_index=False).mean()

    return(data)

# %%
# Example

#import pandas
#D = pandas.DataFrame()
#D['x'] = [3.1,1,3,4,5,3,3]
#D['y'] = [1,3.2,5,2,6,2,1]
#D['v'] = [1.3,3,4,6,7,3,2]
#D = filter_nearby(D, rstr=None, max_radius=6, max_points=23, or_nearest=False)
#D

# %%
def nearby_avg(data, rstr='r', vstr='t', xstr='x', ystr='y',
                max_radius=20, method='W_r_approx', func='mean',
                err_sqrt=False):
    """
    Average all points of a dataset
    """
    # if not r is given, calculate distance from x and y

    # if just one value left, return just that
    if len(data) == 0:
        return(np.nan)
    if len(data) == 1:
        return(data[vstr].values)

    # 
    # interpolate
    from scipy import interpolate
    f = interpolate.interp1d(data[rstr], data[vstr], kind='linear',
        fill_value=(data[vstr].values[0], data[vstr].values[-1]), bounds_error=False)

    df = pandas.DataFrame()
    df['r'] = nearby_avg_rs
    df['w'] = nearby_avg_ws
    df['v'] = f(nearby_avg_rs)
    df = df[df.r <= max_radius]
    # renormalize weights based on new r
    df['w'] = df.w/df.w.sum()
    
    # weight
    if method == 'W_r_approx':
        wavg = np.nansum(df.v * df.w)
    else:
        if func=='mean':
            wavg = np.nansum(df.v * 1/len(df.v))
        elif func=='median':
            wavg = np.nanmedian(df.v)

    if err_sqrt:
        wavg /= np.sqrt(len(data))
    
    if np.isnan(wavg):
        print(data[vstr])
    return(wavg)

# %% Example

# import pandas
# D = pandas.DataFrame()
# D['x'] = [3.1,1,3,4,5,3,3]
# D['y'] = [1,3.2,5,2,6,2,1]
# D['v'] = [1.3,3,4,6,7,3,2]
# D_nearby = filter_nearby(D, rstr=None, max_radius=6, max_points=6, or_nearest=False)
# D_avg_1 = nearby_avg(D_nearby, vstr='v', max_radius=20, method='equal')
# D_avg_2 = nearby_avg(D_nearby, vstr='v', max_radius=20, method='W_r_approx')
# from corny.figures import Figure
# with Figure(size=(5,5)) as ax:
#     #ax.plot(df.r, df.v, label='interpolation')
#     ax.scatter(D_nearby.r, D_nearby.v, label='data')
#     ax.set_xlim(0,10)
#     ax.axhline(D_avg_2, color='C0', ls='--', label='avg Wr_approx')
#     ax.axhline(D_avg_1, color='C1', ls='--', label='avg equal')
#     ax.axhline(D_nearby.v.mean(),   color='C2', ls='--', label='avg data')
#     ax.axhline(D_nearby.v.median(), color='C3', ls='--', label='median data')
#     ax.legend()


def scoring(data=None, var='moisture_vol',
            var_type='moisture',
            filters=[], scope=12, std_factor=1.5,
            max_score=2):

    S = var
    prefix = S+'_score_'
    D = data
    _len_values = D[S].count()

    # Introduce shifts for more performant temporal averaging
    shifts = np.concatenate((np.arange(-scope,0), np.arange(1,scope+1)))
    for s in shifts:
        D[S+'%+d'%s] = D[S].shift(s)

    scope_all = [S+'%+d' % s for s in shifts]
    scope_vor = [S+'%+d' % s for s in np.arange(-scope,0) ]
    scope_zur = [S+'%+d' % s for s in np.arange(1,scope+1)]

    D[prefix+'mean+-']        =   D.loc[:, scope_all].mean(axis = 1)
    D[prefix+'mean-']         =   D.loc[:, scope_vor].mean(axis = 1)
    D[prefix+'mean+']         =   D.loc[:, scope_zur].mean(axis = 1)
    D[prefix+'stdev+-']       =  (D.loc[:, scope_all].std(axis = 1) *std_factor)**2
    D[prefix+'median+-']      =   D.loc[:, scope_all].median(axis = 1)
    #1
    D[prefix+'minus_mean']    =   D.loc[:,S]  -  D.loc[:,prefix+'mean+-']   >0
    #2
    D[prefix+'minus_median']  =   D.loc[:,S]  -  D.loc[:,prefix+'median+-'] >0
    #7
    D[prefix+'follow_SD']     = ((D.loc[:,S]) - (D.loc[:,S+'+1']))**2  > D.loc[:,prefix+'stdev+-']
    #8
    D[prefix+'before_SD']     = ((D.loc[:,S]) - (D.loc[:,S+'-1']))**2  > D.loc[:,prefix+'stdev+-']
    #5
    if var_type=='moisture':
        D[prefix+'wetter_before_and_after'] = (((D.loc[:,S]) - (D.loc[:,S+'-1']))  > 0) & (((D.loc[:,S]) - (D.loc[:,S+'+1']))  > 0)
    elif var_type=='neutrons':
        D[prefix+'wetter_before_and_after'] = (((D.loc[:,S]) - (D.loc[:,S+'-1']))  < 0) & (((D.loc[:,S]) - (D.loc[:,S+'+1']))  < 0)
    D[prefix+'minus_before']  = ((D.loc[:,S]) - (D.loc[:,prefix+'mean+']))**2  > D.loc[:,prefix+'stdev+-']
    D[prefix+'minus_follow']  = ((D.loc[:,S]) - (D.loc[:,prefix+'mean-']))**2  > D.loc[:,prefix+'stdev+-']
    D[prefix+'minus_mean']    = ((D.loc[:,S]) - (D.loc[:,prefix+'mean+-']))**2 > D.loc[:,prefix+'stdev+-']

    # Remove shifts
    for s in shifts:
        D.drop(S+'%+d'%s, axis=1, inplace=True)

    # Calculate summary score
    D[prefix+'combined'] = D[ [prefix+s for s in filters] ].sum(axis=1)
    
    # Select the chose one
    D[prefix+'selected'] = D.loc[ D[prefix+'combined'] < max_score, S]

    print('i Scoring removed %d out of %d values.' % (
        _len_values-D[prefix+'selected'].count(), _len_values))
    
    return(D)

# Gridding

class RegularGrid:
    def __init__(self, bbox, resolution=1):
        x0, x1, y0, y1 = bbox
        self.x0 = x0
        self.x1 = x1
        self.y0 = y0
        self.y1 = y1
        self.resolution = resolution


    def find_nearest_node_x(self, x):
        res = self.resolution
        node_x = np.floor((x + res / 2 - self.x0) / res).astype(int)
        return node_x

    def find_nearest_node_y(self, y):
        res = self.resolution
        node_y = np.floor((y + res / 2 - self.y0) / res).astype(int)
        return node_y

    def find_nearest_node(self, x, y):
        node_x = self.find_nearest_node_x(x)
        node_y = self.find_nearest_node_y(y)
        return node_x, node_y
    
    def find_nearest_node_from_pandas_xy(self, data):
        """
        Assumes that data consists of two columns with x and y values
        """
        data["node_x"] = self.find_nearest_node_x(data.iloc[:,0])
        data["node_y"] = self.find_nearest_node_y(data.iloc[:,1])
        return data[["node_x","node_y"]]

    def get_node_id_from_pandas_xy(self, data, method="Dega"):
        
        # Convert x,y to node_x, node_y
        data = self.find_nearest_node_from_pandas_xy(data)
        
        if method == "Dega":
            data["node_id"] = 1000 * data.iloc[:,0] + data.iloc[:,1]
        else:
            w = np.floor((self.x1 - self.x0) / self.resolution)
            h = np.floor((self.y1 - self.y0) / self.resolution)
            M = np.arange(w*h).reshape((h,w))
            data["node_id"] = M[data.iloc[:,0], data.iloc[:,1]]

        return(data["node_id"])

        