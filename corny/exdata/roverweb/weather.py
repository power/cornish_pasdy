
"""
Very Important Notice, in the dwdweather2 library I experienced some bugs
regarding the communication with the sql database
I fixed three main things:
    1) solved problem of vanishing category properties
    - class DwdWeather in core.py:
        # a fixed categories property
        self.categories_fixed = self.resolve_categories(category_names)
        ...
        def import_measures(self, station_id, latest=True, historic=False):
            ...
            # Download and import data.
            for category in self.categories_fixed:
    2) Solved issue that sql query fails to fetch results in core.py
        def query(self, station_id, timestamp, recursion=0):
        if recursion < 2:
            sql = "SELECT * FROM %s WHERE station_id=? AND datetime=?" % self.get_measurement_table()
            c = self.db.cursor()
            c.execute("SELECT * FROM " + self.get_measurement_table() +" WHERE station_id= ? AND datetime= ? ",(int(station_id),str(timestamp.strftime(self.get_timestamp_format())),))
    3) Solved issue of geeting incomplete list of stations
        wrote an own tool to get station list uptodate

WARNING: CURRENTLY ONLY ONE CATEGORY ACCEPTED, FOR MULTIPLE CATEGORIES
PLEASE RUN THE FUNCTION MULTIPLE TIMES

"""
import urllib
from ftplib import FTP
from datetime import datetime,timedelta,date
import numpy as np
import geopandas as gpd
import pandas as pd
import math
import time
from shapely.ops import nearest_points
import re
import os
import json
import xarray as xr
from zipfile import ZipFile
import io
import sys
from pdb import set_trace as debug
from corny.basics import col_is_unique, progressbar, cprintf
import warnings
warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)
#######

def findkeys(node, kv):
    """
    from https://stackoverflow.com/questions/9807634/find-all-occurrences-of-a-key-in-nested-dictionaries-and-lists
    """
    if isinstance(node, list):
        for i in node:
            for x in findkeys(i, kv):
               yield x
    elif isinstance(node, dict):
        if kv in node:
            yield node[kv]
        for j in node.values():
            for x in findkeys(j, kv):
                yield x

def connect_ftp(server = 'opendata.dwd.de',connected=False):
    while not connected:
        try:
            ftp = FTP(server)
            ftp.login()
            connected = True

        except:
            time.sleep(5)
            print('Reconnect to Server')
            pass
    return ftp
def update_stationlist(time_res='hourly',dbase_dir='dbase',data_category='air_tremperature'):
    """
    small function to get updated dwd station list

    """
    stations_old=None
    #dwd categories
    dwd_abbr = {'air_temperature': 'TU',
                  'cloud_type': 'CS',
                  'cloudiness': 'N',
                  'dew_point' : 'TD',
                  'extreme_temperature': 'TX',
                  'extreme_wind': 'FX',
                  'precipitation': 'RR',
                  'pressure': 'P0',
                  'soil_temperature': 'EB',
                  'solar': 'ST',
                  'sun': 'SD',
                  'visibility': 'VV',
                  'wind': 'FF',
                  'wind_synop': 'F',
                  'kl':'KL',
                  'more_precip':'RR',
                  'soil_temperature':'EB',
                  'water_equiv':'Wa',
                  'weather_phenomena':'wetter'
                  }

    # lets start
    print('|     Updating list of DWD network stations.. ', end='')

    #load metadata table
    dwd_datasets_meta=json.load(open(dbase_dir+"\\dwd_station_meta.txt"))

    # create output directory if not existing

    if not os.path.exists(dbase_dir):
        os.makedirs(dbase_dir)

    #check whether we have an up-to-date-station-list-already
    try:
        stations_network_old=[s for s in os.listdir(dbase_dir) if 'dwd_station_network_'+data_category+'_'+time_res in s][0]
        datetime_network=datetime.date(datetime.strptime(re.findall('\d+',stations_network_old)[0],'%Y%m%d'))
        #update if more than 24hours
        dt_today=datetime.date(datetime.now())
        if (dt_today-datetime_network)<timedelta(days=1):
            print('already up to date.')
            filename_stations=dbase_dir+'\\'+stations_network_old
            time_res_dbase=time_res
            return (filename_stations, time_res_dbase)
        print('update needed.')
        os.remove(dbase_dir+'\\'+stations_network_old)
    except:
        print('update needed.')
        pass
    #check also w


    # header
    stations_network=pd.DataFrame()

    # connect to ftp server and go to the folder

    # Connect to the Server
    server='opendata.dwd.de'
    ftp=connect_ftp(server = server,connected = False)
    #change to subfolder
    ftp.cwd('/climate_environment/CDC/observations_germany/climate/' + time_res +'/')

    #if data is not daily, we have to check categorywise the availability of information
    #get dwd categories
    dwd_categories=ftp.nlst()

    if data_category in dwd_categories:
        time_res_dbase=time_res
    elif time_res=='daily':
            #check hourly database
            time_res_dbase='hourly'
            ftp.cwd('/climate_environment/CDC/observations_germany/climate/' + time_res_dbase +'/')
            dwd_categories=ftp.nlst()
            if data_category in dwd_categories:
                print(data_category,' is not provided at the required resolution, daily_mean of hourly data used instead')
            else:
                time_res_dbase='10_minutes'
                if data_category in dwd_categories:
                    ftp.cwd('/climate_environment/CDC/observations_germany/climate/' + time_res_dbase +'/')
                    dwd_categories=ftp.nlst()
                if data_category in dwd_categories:
                    print(data_category,' is not provided at the required resolution, daily_mean of 10_minutes data used instead')
                else:
                    print(data_category, 'not available')
                    sys.exit(1)
    elif time_res=='hourly':
        time_res_dbase='10_minutes'
        if data_category in dwd_categories:
            ftp.cwd('/climate_environment/CDC/observations_germany/climate/' + time_res_dbase +'/')
            dwd_categories=ftp.nlst()
        if data_category in dwd_categories:
            print(data_category,' is not provided at the required resolution, daily_mean of 10_minutes data used instead')
        else:
            print(data_category, 'not available')
            sys.exit(1)

    #loop through the subfolders  to get the station lists

    for category in [data_category]:
        #retreive station_list
        print('retrieve stationlist for', category)
        #try to get historical data
        try:
            dir_path='/climate_environment/CDC/observations_germany/climate/' + time_res_dbase +'/'+category+'/historical/'
            ftp.cwd(dir_path)
        except Exception as e:
            print(e, 'try to download category', category, 'from other folder')
            try:
                dir_path='/climate_environment/CDC/observations_germany/climate/' + time_res_dbase +'/'+category+'/'
                ftp.cwd(dir_path)
            except:
                print('Category', category, 'could not have been downloaded')
                pass
        #get the filename of the station list
        if time_res_dbase=='hourly':
            filename=dwd_abbr[category]+'_Stundenwerte_Beschreibung_Stationen.txt'
        if time_res_dbase=='daily':
            if dwd_abbr[category] =='wetter':
                filename=dwd_abbr[category]+'_tageswerte_Beschreibung_Stationen.txt'
            else:
                filename=dwd_abbr[category]+'_Tageswerte_Beschreibung_Stationen.txt'
        if time_res_dbase=='10_minutes':
            filename='zehn_min_'+dwd_abbr[category].lower()+'_Beschreibung_Stationen.txt'
        #retrieve the stationlist
        stationlist = list()
        # try to retrieve file
        print('| Read FTP data ', end='')
        retrieved=False
        max_trys = 10
        while max_trys > 0:
            try:
                url='https://opendata.dwd.de//'+dir_path+'//'+filename
                file = urllib.request.urlopen(url)
                [stationlist.append(line.decode('latin-1')) for line in file]
                retrieved = True
                max_trys=-1
            except:
                max_trys -= 1
                print('.', end='')
                ftp=connect_ftp(server = server,connected = False)
                ftp.cwd(dir_path)
        if retrieved:
            print('OK')
        else:
            print('FAILED.')
            return()

        #remove first two lines
        stationlist=stationlist[2:]
        #delete uncessary blanks
        stationlist=[re.sub(' +', ' ', station.rstrip()) for station in stationlist]
        #split the list
        stationlist=[station.split(" ")[:7] for station in stationlist]
        #read as dataframe
        static_rows=['date_start','date_end','height','geo_lat','geo_lon','name']
        dfstations=pd.DataFrame(stationlist,columns=['STATIONS_ID','date_start','date_end','height','geo_lat','geo_lon','name'])
        #add true information to category
        dfstations[category]=True

        stations_network = pd.concat(
            [ stations_network, dfstations ],
            sort=False, ignore_index=True)
        #A=[sub.split(" ") for sub in stationlist]

    #replace all Na by False
    stations_network.loc[stations_network[data_category].isna(),data_category]=0
    #aggregate
    stations_network=stations_network.groupby(['STATIONS_ID'],as_index=False).agg('max')
    #replace zero by False in order to have pure boolean data
    stations_network.replace(0,False,inplace=True)
    #fix the error with station 14138 and 05614 and 07325, which does not have pressure cord
    if 'pressure' in stations_network:
        stations_network.loc[stations_network.STATIONS_ID=='14138','pressure']=False
        stations_network.loc[stations_network.STATIONS_ID=='05614','pressure']=False
        stations_network.loc[stations_network.STATIONS_ID=='07325','pressure']=False
        stations_network.loc[stations_network.STATIONS_ID=='01572','pressure']=False
    #for temperature the same
    if 'air_temperature' in stations_network:
        stations_network.loc[stations_network.STATIONS_ID=='14138','air_temperature']=False
    #save to database writing the time as well
    filename_stations=dbase_dir+'\\dwd_station_network_' + category +'_'+time_res_dbase +'_' + datetime.now().strftime('%Y%m%d')+'.csv'
    #merge with existing data
    stations_network.to_csv(filename_stations,index=False)

    print('|     Updating station list...finished')

    return  (filename_stations, time_res_dbase)


def nearest_direct(row, gdf1, gdf2, src_column=None):
    """Find the nearest point and return the corresponding value from specified column.
    df 1 is the origin
    df 2 is the destination
    inspired by https://automating-gis-processes.github.io/2017/lessons/L3/nearest-neighbour.html
    """
    #create a unary union
    unary_union = gdf2.unary_union
    # Find the geometry that is closest
    nearest = gdf2['centroid'] == nearest_points(row['centroid'], unary_union)[1]
    # Get the corresponding value from df2 (matching is based on the geometry)
    value = gdf2[nearest][src_column].get_values()[0]
    return value

#inpt_df=pd.read_csv('Mueglitz-20190708_selection.csv')
def nearest_loop(row, gdf2,geometry_cols=['geo_lon','geo_lat'],src_column=None,surrounding=False):
    """
    takes longer, seems to be more precise and allows multiple locations
    surrounding: int: m around the measurement point, default is false
    """
    def haversine_distance(origin, destination):
        lon1, lat1 = origin
        lon2, lat2 = destination
        radius = 6371000 # meters

        dlat = math.radians(lat2-lat1)
        dlon = math.radians(lon2-lon1)
        a = math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(lat1)) \
            * math.cos(math.radians(lat2)) * math.sin(dlon/2) * math.sin(dlon/2)
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
        d = radius * c
        return d

    # start the main iteration
    if row.geometry.type == 'Polygon':
        point_xy = np.array((row.geometry.centroid.x,
                     row.geometry.centroid.y))
    if row.geometry.type in ['Point', 'LineString']:
        point_xy = np.array((row.geometry.x, row.geometry.y))
    # Select most current stations datasets.
    closest = None
    closest_distance = 99999999999
    for _, station in gdf2.iterrows():
        d = haversine_distance((point_xy[0], point_xy[1]),
            (station[geometry_cols[0]], station[geometry_cols[1]]))
        if d < closest_distance:
            closest = station
            closest_distance = d
    # if surroung
    if surrounding:
        closest1 = []
        closest_distance = closest_distance+surrounding
        i = 0
        for _, station in gdf2.iterrows():
            d = haversine_distance((point_xy[0], point_xy[1]),
                                   (station[geometry_cols[0]], station[geometry_cols[1]]))
            if d < closest_distance:
                closest1.append(station)
                i += 1
            closest = closest1
    return closest[src_column]

def dwd_age_test(date):
    """
    

    Parameters
    ----------
    date : a list containing the earliest and the latest date of the investigation period
        DESCRIPTION.

    Returns
    -------
    timeranges : TYPE
        DESCRIPTION.

    """
    # Compute timerange labels / subfolder names.

    days_smallest = (datetime.utcnow() - date[-1]).total_seconds() / 86400
    days_largest = (datetime.utcnow() - date[0]).total_seconds() / 86400
    age=np.array((days_smallest,days_largest))
    #if all entries are more than 500 days away we use historic data
    if age.min() > 500:
        timeranges=['historical']
    #if all are younger 500 days we can use recent data only
    elif age.max() < 500:
        timeranges=['recent']
    else:
        timeranges=['recent','historical']
    print('Timeranges ',timeranges, 'will be downloaded from dwd server')

    return timeranges

def archive_in_time(archive_name,campaign_time,timerange):
    """
    This function checks whether the archive covers the requested time range
    ONLY valid for historical time range

    Parameters
    ----------
    archive_name : TYPE
        DESCRIPTION.
    campaign_time : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    if 'historical' not in timerange:
        return True
    #read the time range from the archive name
    start=datetime.strptime(archive_name.split('_')[3],'%Y%m%d').date()
    end=datetime.strptime(archive_name.split('_')[4],'%Y%m%d').date()
    #check data range
    if start <= campaign_time[0].date() <= end or start <= campaign_time[1].date() <= end:
        print(archive_name,' is in time')
        return True
    else:
        return False


#%% We start to connect to dwd server and download
def import_stations(time_res='hourly',time_format='%Y%m%d%H',
                    campaign_time=[datetime(2018,12,9), datetime(2018,12,12)],
                    data_category='air_temperature', parameter='air_temperature_at_2m_height',dwd_stations=pd.DataFrame(),
                    dbase_dir='dbase', table_dir='tables',Output=False,
                    memory_save=True):
    """
    Imports stations to the existing netcdf database
    Warning: Currently, the only way to update the database for existing
    stations is to delete the entire database
    WARNING: Import does not seems to be perfect, sometimes double import
    """
    #%% First we create the database directory if it does not exist
    os.makedirs(dbase_dir,exist_ok=True)
    #define time range options
    timeranges=dwd_age_test(campaign_time)
    #%%load the datasets available at each timestep
    dwd_datasets_meta=dwd_datasets_meta=json.load(open(table_dir+"\\dwd_station_meta.txt"))
    #try to get a variable from the category, otherwise use interpolation of higher frequency data
    resample_frequency=None
    time_res_dbase=time_res
    try:
        dwd_datasets_meta[time_res][data_category]
    except Exception:
        if time_res=='daily':
            try:
              dwd_datasets_meta['hourly'][data_category]
              print(data_category,' is not provided at the required resolution, daily_mean of hourly data used instead')
              resample_frequency='D'
              time_res_dbase='hourly'
            except Exception:
                try:
                    dwd_datasets_meta['10_minutes'][data_category]
                    print(data_category,' is not provided at the required resolution, daily_mean of 10_minutes data used instead')
                    resample_frequency='D'
                    time_res_dbase='10_minutes'
                except Exception:
                    print(data_category, 'not available')
                    sys.exit(1)
        if time_res=='hourly':
            try:
                dwd_datasets_meta['10_minutes'][data_category]
                print(data_category,' is not provided at the required resolution, hourly_mean of 10_minutes data used instead')
                resample_frequency='H'
                time_res_dbase='10_minutes'
            except Exception:
                print(data_category, 'not available')
                sys.exit(1)


    #%% download from dwd if necessary
    #connect to server
    server='opendata.dwd.de'
    ftp=connect_ftp(server = server,connected = False)
    #get the mean time of the campaign
    date_mean=campaign_time[0]+(campaign_time[1]-campaign_time[0])/2
    # load the inititial ds
    dbase_path=dbase_dir+'\\db_stations_'+time_res+'_'+data_category+'.nc'
    print(cprintf('>     Reading %s ..' % dbase_path,'yellow'), end='')
    if os.path.exists(dbase_path):
        with xr.open_dataset(dbase_path) as dwd_dbase:
            dwd_dbase.load()
            print(cprintf('OK','yellow'))
         #get the non_nans stations covering the  entire campaign
        current_stations_0=np.array(dwd_dbase[parameter].sel(time=campaign_time[0],method='nearest').dropna('STATIONS_ID').coords['STATIONS_ID'])
        current_stations_1=np.array(dwd_dbase[parameter].sel(time=campaign_time[1],method='nearest').dropna('STATIONS_ID').coords['STATIONS_ID'])
    else:
        #print(dbase_path, 'does not exist, we create a new netcdf_file')
        print('created.')
        dwd_dbase=xr.Dataset()
        current_stations_0=np.array((-9999)).reshape(1)
        current_stations_1=np.array((-9999)).reshape(1)
    #change directory on server
    for timerange in timeranges:
        archive_url='/climate_environment/CDC/observations_germany/climate/'+time_res_dbase+'/'+data_category+'/'+timerange
        ftp.cwd(archive_url)
        #get the archivestation_ids
        for station_id in dwd_stations['STATIONS_ID']:
            #we check whether the station is in the database with this parameter already
            if int(station_id) in current_stations_0 and int(station_id) in current_stations_1:
                print('Station', station_id, 'with category', data_category,'in ',timerange,'dbase already')
                continue
            try:
                archive_names=[s for s in ftp.nlst() if station_id in s]
            except:
                print('No ',timerange,'data for station',station_id)
                continue
            for archive_name in archive_names:
                #check whether the archive are in valid time range
                if not archive_in_time(archive_name,campaign_time,timerange):
                    continue
                print('Retrieving {}...'.format(archive_name))
                retrieved = False
                archive = io.BytesIO()
                # try to retrieve file
                while not retrieved:
                    try:
                        ftp.retrbinary("RETR " + archive_name, archive.write)
                        retrieved = True
                    except:
                        ftp=connect_ftp(server = server,connected = False)
                        ftp.cwd(archive_url)
                archive.seek(0)
                with ZipFile(archive) as myzip:
                    for f in myzip.infolist():
                        # This is the data file
                        #print('zip content:', f.filename)
                        if f.filename.startswith('produkt_'):
                            product = io.StringIO(str(myzip.read(f.filename),'utf-8'))
                #get dataframe from product
                dwd_product=pd.read_csv(product,sep=';',skipinitialspace=True)
                #get datetime
                dwd_product['time']=pd.to_datetime(dwd_product['MESS_DATUM'],format=time_format)
                dwd_product=dwd_product.rename(columns=dwd_datasets_meta[time_res_dbase][data_category])
                dwd_product=dwd_product.reset_index()
                dwd_product=dwd_product.set_index(['time','STATIONS_ID'])
                cols_to_drop=['MESS_DATUM','quality_level_of_next_columns','end_of_record','index']
                for col_to_drop in cols_to_drop:
                    try:
                        dwd_product=dwd_product.drop(columns=[col_to_drop])
                    except Exception as e:
                        print(e)
                        pass
                # if we are at 10 minutes and 'air_temp'
                if data_category=='air_temperature' and time_res=='10_minutes':
                    print('recalculate pressure at sea level from station')
                    alt=float(dwd_stations[dwd_stations['STATIONS_ID']==station_id].height)
                    T=dwd_product['air_temperature_at_2m_height']
                    dwd_product['air_pressure_nn']= dwd_product['pressure_at_station_height'] * ( 1 - (0.0065*alt) / ( T + 0.0065*alt + 273.15 ) )**-5.257
                #append to database
                dwd_xr=dwd_product.to_xarray()
                #replace all values equal to -999 to nan
                for data_var in dwd_xr.data_vars:
                    dwd_xr[data_var]=dwd_xr[data_var].where(dwd_xr[data_var]>-99)
                if station_id=='05009':
                    print('ok')
                #only add relevant dates if available memoryis rather small
    
                if memory_save and timerange=='historical':
                    dwd_xr=dwd_xr.sel(time=slice(campaign_time[0]-timedelta(days=1),campaign_time[1]+timedelta(days=1)))
                    #dwd_xr=dwd_xr.squeeze()
    
                try:
                    dwd_dbase=xr.merge([dwd_dbase,dwd_xr])
                except Exception as e:
                    print(e)
                    print('try merging with compat=override')
                    dwd_dbase=xr.merge([dwd_dbase,dwd_xr],compat='override')
                print(archive_name,' added to database')
    #upscale to required temporal resolution
    if resample_frequency is not None:
        dwd_dbase=dwd_dbase.resample(time=resample_frequency).mean(skipna=True)
        print('DWD data upscaled to',time_res,'averages')
    if Output==True:
        print(cprintf('<     Storing %s ..' % dbase_path,'green'), end='')
        dwd_dbase.to_netcdf(dbase_path)
        print(cprintf('OK','green'))
    return dwd_dbase

#%% Start the main function
def Find_nearest_dwd_stations(inpt_data,
                         date_start='20051201',
                         date_end='20201231',
                         data_category='air_temperature',
                         parameters='air_temperature_at_2m_height',
                         temp_resolution='hourly',
                         no_of_nearest_stations=4,
                         memory_save=True,
                         Output='True'):
    """
    The Main function which is written from skretch and uses an netcdf database
    Run for each category individually
    """
    if isinstance(data_category,list):
        if len(list(data_category)) > 1:
            print(
                'Currently only one dwd category allowed, please run function multiple times for each category'
            )
            return None
    #define dwd time format for each temporal_resolution
    dwd_time_formats={'hourly':'%Y%m%d%H',
                      'daily':'%Y%m%d',
                      '10_minutes':'%Y%m%d%H%M'}

    dwd_time_format=dwd_time_formats[temp_resolution]

    #convert time to datetime
    dt_start=datetime.strptime(date_start,'%Y%m%d')
    dt_end=datetime.strptime(date_end,'%Y%m%d')
    #print('Start quering data from DWD')
    #define the database folder
    pypath = os.path.dirname(os.path.abspath(__file__))
    table_dir = pypath + '\\' + 'tables'
    dbase_dir = pypath + '\\' + 'dbase'
    #%% we check all available stations and create a valid list
    filename_stations,time_res_dbase=update_stationlist(time_res=temp_resolution,dbase_dir=table_dir,data_category=data_category)

    #update time format
    dwd_time_format=dwd_time_formats[time_res_dbase]

    # read station list
    stations_all=pd.read_csv(filename_stations, dtype={'STATIONS_ID': object})

    # delete all stations which do not cover the category
    dwd_stations=stations_all[stations_all[data_category]==True].copy()

    #correct to datetime
    dwd_stations['date_end']=pd.to_datetime(stations_all.date_end,format='%Y%m%d')
    dwd_stations['date_start']=pd.to_datetime(stations_all.date_start,format='%Y%m%d')

    # clean to stations which cover the campaign time #dt_low <= dt <= dt_high:
    dwd_stations=dwd_stations[(dwd_stations.date_start<=dt_start) & (dwd_stations.date_end>=dt_end)]

    # convert to geodataframe
    dwd_stations=gpd.GeoDataFrame(dwd_stations,geometry=gpd.points_from_xy(dwd_stations.geo_lon, dwd_stations.geo_lat))

    if not len(dwd_stations)>0:
        return(inpt_data, None)

    #loop through all rows to get the n closest points
    distances=pd.DataFrame()

    # If all coordinates in data are the same, use just one row
    all_same_coords = col_is_unique(inpt_data.geometry)
    if all_same_coords:
        inpt_data_x = inpt_data.iloc[[0]]
    else:
        inpt_data_x = inpt_data

    print('|     Distance to nearest DWD station:')
    i = 0
    for _, station in dwd_stations.iterrows():
        distances[station.STATIONS_ID] = inpt_data_x.distance(station.geometry)
        i += 1
        progressbar(i-1, len(dwd_stations), title='    Calculating', prefix='Station %s' % station.STATIONS_ID)

    progressbar(i-1, len(dwd_stations), title='    Calculating', prefix=str(station.STATIONS_ID), end=True)

    if all_same_coords:
        # repeat the result for all time steps
        distances = pd.concat([distances]*len(inpt_data))
        distances.index = inpt_data.index

    # get the n stations with smallest distance and update database
    print('|     Find nearest %d stations...' % no_of_nearest_stations)
    id_nearest_stations=distances.apply(lambda s: s.nsmallest(no_of_nearest_stations).index.tolist(), axis=1).values.tolist() #station ids
    #get them as unique values by sum a list of lists https://bit.ly/353iZQB
    id_dwd_stations=list(set(sum(id_nearest_stations,[])))
    #create a subset of the entire dwd_stations
    dwd_stations_to_import=dwd_stations[dwd_stations['STATIONS_ID'].isin(id_dwd_stations)]

    #update the database
    db_dwd_stations=import_stations(time_res=temp_resolution,
                                    time_format=dwd_time_format,
                                    campaign_time=[dt_start,dt_end],
                                    data_category=data_category,
                                    dwd_stations=dwd_stations_to_import,
                                    parameter=parameters[0],
                                    dbase_dir=dbase_dir,
                                    Output=Output,
                                    table_dir=table_dir,
                                    memory_save=memory_save)

    #distance of nearest stattions
    dist_nearest_stations=pd.DataFrame(np.sort(distances.values)[:,:no_of_nearest_stations]).values.tolist() #distances themself
    #create new columns in the input data
    station_col_nm=list()
    for i in range(0,no_of_nearest_stations):
        station_col_nm.append(data_category+'_station_'+str(i))
    for i in range(0,no_of_nearest_stations):
        station_col_nm.append(data_category+'_distance_'+str(i))

    #create new dataframe: [ id, distance ]
    distance_data=pd.concat( [ pd.DataFrame(id_nearest_stations).astype(int),
                               pd.DataFrame(dist_nearest_stations)], axis=1)
    distance_data.columns=station_col_nm

    #add to main dataset
    inpt_data=pd.concat([inpt_data, distance_data.set_index(inpt_data.index)], axis=1)

    return(inpt_data,db_dwd_stations)

def Apnd_dwd_data(inpt_data,dwd_dbase,
                         time_col='Date Time(UTC)',
                         data_time_format='%Y-%m-%d %H:%M:%S',
                         data_category='air_temperature',
                         parameters=['2m_air_temperature','2m_relative_humidity'],
                         no_of_nearest_stations=3,
                         idw_exponent=1):
    print('|     Query data from DWD and use IDW algorithm for parameter interpolation...', end='')
    # convert input time
    df_times = pd.to_datetime(inpt_data[time_col], format=data_time_format)
    #correct if input parameter is str but not list
    if isinstance(parameters,str):
        parameters=[parameters]
    #add additional columns to the inpt data
    inverse_dist=np.zeros((len(inpt_data),no_of_nearest_stations))
    for i in range (0,no_of_nearest_stations):
        inverse_dist[:,i]=(1/inpt_data[data_category+'_distance_'+str(i)]**2)
    for parameter in parameters:
        inpt_data[parameter]=-9999
        #calculate the result for each matrix
        result_matrix=np.zeros((no_of_nearest_stations,len(inpt_data)))
        #loop over all the available stations
        for i in range (0,no_of_nearest_stations):
            #add station data colummn-wise
            station_data=dwd_dbase[parameter].sel(STATIONS_ID=xr.DataArray(inpt_data[data_category+'_station_'+str(i)]), time=xr.DataArray(df_times),method='nearest').values
            #http://xarray.pydata.org/en/stable/indexing.html
            #check for nan values
            station_data[np.where(station_data==-999)]=np.nan
            #replace distance to 100000 if value is nan
            inpt_data.loc[np.isnan(station_data),data_category+'_distance_'+str(i)]=10000000
            #replace if distance is zero (can happen if device is at weather station the value with a value one digit smaller then GPS precision
            inpt_data.loc[inpt_data[data_category+'_distance_'+str(i)]==0,data_category+'_distance_'+str(i)]=0.00000001
            #add inverse distances in dependence on the data
            inverse_dist[:,i]=(1/inpt_data[data_category+'_distance_'+str(i)]**idw_exponent)
            #depending whether there is data or not,
            result_matrix[i,:]=np.array(station_data*(1/inpt_data[data_category+'_distance_'+str(i)]**idw_exponent))
        #add to new columns
        inpt_data[parameter]=np.nansum(result_matrix,axis=0)/inverse_dist.sum(axis=1)
    print('OK')
    return inpt_data
