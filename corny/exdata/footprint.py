"""
Calls the geometry in roverweb to create footprints
"""
import geopandas as gpd
from copy import deepcopy
from .roverweb import geometry as rovergeo
def retrieve_footprints(data,footprint_radius=50,lon='LongDec',lat='LatDec',
                        output='output.geojson',unify=False):
    #first we form a geodataframe with points from our data
    if isinstance(lat, float):
        data['LatDec'] = deepcopy(lat)
        lat='LatDec'
    if isinstance(lon, float):
        data['LongDec']=deepcopy(lon)
        lon = 'LongDec'

    #remove rows with no geographic coordinate
    data = data[data[lon].notna()]
    data = data[data[lat].notna()]

    #create geodataframe
    data_gdf=gpd.GeoDataFrame(data, geometry=gpd.points_from_xy(data[lon], data[lat]),crs='epsg:4326')
    data_gdf_out=rovergeo.points_to_footprint(data_gdf,footprint_radius,crs_src='epsg:4326',
                                              crs_dst='epsg:4326')
    #delete tuple

    try:
        data_gdf_out=data_gdf_out.drop(columns='ll')
    except:
        print('No column', 'll', 'to delete')
    if output is not None:
        data_gdf_out.to_file(output,driver='GeoJSON')
    if unify:
        boundary_polygon=data_gdf_out.dissolve().geometry.iloc[0]
        return boundary_polygon
