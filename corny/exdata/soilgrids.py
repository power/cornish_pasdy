"""
CoRNy exdata.NM
    Get Data from NMDB
"""

from corny import *

from .roverweb import soilgrid
import geopandas as gpd
from copy import deepcopy
def retrieve_soilgrid_data(data, lon='LongDec',lat='LatDec',                                         
                                soilgridlrs=['bdod','clay','soc'],
                                soil_layerdepths=['0-5cm','5-15cm','15-30cm','30-60cm'],
                                raster_res=(250, 250),
                                statistical_metric=['mean'],
                                all_touched=True,
                                zero_to_nan=True):
    """
    A function to call the roverweb soilgrids module to get SoilGrids paramaters for 
    the desired location and time
    Warning: EPSG: 4326 is silently assumed
    Parameters
    ----------
    data : dataframe
        DESCRIPTION.
    lon : str, optional
        data_subset Column name for longitude coordinate. The default is 'LongDec'.
    lat : str, optional
        data_subset Column name for latitude coordinate . The default is 'LatDec'.
    soilgridlrs : list, optional
        Parameter Category on WCS Service
    soil_layerdepths : list, optional
        Requested depth layers.
    all_touched : boolean, optional
    statistical_metric: list, optional
        Default is mean
    zero_to_nan :boolean, optional
        replace 0 in dataset by np.nan

    Returns
    -------
    data : dataframe
        Output dataframe with new column

    """
    #if lon and lat is a float, we add this as extra columns
    if isinstance(lat,float) and isinstance(lon,float):
        data['LatDec']=deepcopy(lat)
        data['LongDec']=deepcopy(lon)
        lat='LatDec'
        lon='LongDec'        
    #define a dictionary with names and conversion factors
    soil_metadata={'bdod':{'crr_name' : 'bd','conversion_factor':100},
                 'clay':{'crr_name' : 'clay','conversion_factor':1000},
                 'soc':{'crr_name' : 'org','conversion_factor':1000}}
    
    #remove rows with no geographic coordinate
    data = data[data[lon].notna()]
    data = data[data[lon].notna()]
    
    #create geodataframe
    data=gpd.GeoDataFrame(data,geometry=gpd.points_from_xy(data[lon],data[lat]),crs='epsg:4326')
    #remember all old columns
    col_nm_old=data.columns.values.tolist()
    #call the main functionality in roverweb
    data = soilgrid.apnd_from_wcs(data,soilgridlrs=soilgridlrs,
        soil_layerdepths=soil_layerdepths,
        raster_res=raster_res,
        statistical_metric=statistical_metric,
        all_touched=all_touched,
        output=None)
    #get the subset all all retrieved layers
    new_columns=list(set(data.columns.values.tolist()) - set(col_nm_old))
    soil_data=data[new_columns].copy()
    #replace 0 values with nan
    if zero_to_nan:
        print('replace zero entry by nan')
        soil_data.replace({0:np.nan},inplace=True)
    #rename and average columns
    if len(soil_layerdepths)>0:
        print('soil depth layers will be averaged')
        for soil_prop in soil_metadata:
            soil_prop_cols=[col for col in new_columns if soil_prop +'_' in col]
            #get mean of these columns as new column and divide by conversion factor
            soil_data[soil_metadata[soil_prop]['crr_name']] = soil_data[soil_prop_cols].mean(axis=1)/soil_metadata[soil_prop]['conversion_factor']
    
    #postprocessing, remove datasets and add
    #drop temporary datasets
    soil_data.drop(columns=new_columns,inplace=True)
    #add to original dataset
        
    
    print('Retrieval and processing from soilgrids successful')                        

    return pd.DataFrame(soil_data)


