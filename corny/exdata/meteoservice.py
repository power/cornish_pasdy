"""
CoRNy exdata.NM
    Get Data from NMDB
"""

from corny import *

#######################################################
##### Automatic Meteo lookup
##### by Erik Nixdorf
##### (merge 1 commit 5eb8070f)
#######################################################

#import necessaries for roverweb library
from .roverweb import weather
import geopandas as gpd
from copy import deepcopy

def retrieve_dwd_data(data_subset,
                                  lon='LongDec',
                                  lat='LatDec',
                                  dwd_category='pressure',
                                  dwd_parameters=['air_pressure_local'],
                                  no_of_nearest_stations=4,
                                  temporal_resolution='daily',
                                  local_storage_file='data_from_DWD.csv'):
    """
    A function to call the roverweb weather module to get dwd paramaters for
    the desired location and time
    Warning: EPSG: 4326 is silently assumed
    Parameters
    ----------
    data_subset : dataframe
        DESCRIPTION.
    lon : str, optional
        data_subset Column name for longitude coordinate. The default is 'LongDec'.
    lat : str, optional
        data_subset Column name for latitude coordinate . The default is 'LatDec'.
    dwd_category : str, optional
        Parameter Category on FTP Server. The default is 'pressure'.
    dwd_parameters : list, optional
        Requested parameters for each category. The default is ['air_pressure_local'].
    no_of_nearest_stations : int, optional
        Number of nearest dwd stations for each record location. The default is 4.

    Returns
    -------
    data_subset : dataframe
        Output dataframe with new column

    """
    #get the output directory from subset and target variable
    #output_path=data_subset.attrs['dwd_path']
    #output_variable=data_subset.attrs['target_var']
    #if lon and lat is a float, we add this as extra columns
    if isinstance(lat, float):
        data_subset['LatDec'] = deepcopy(lat) # TODO: why deepcopy?
        lat='LatDec'
    if isinstance(lon, float):
        data_subset['LongDec']=deepcopy(lon)
        lon = 'LongDec'

    #remove rows with no geographic coordinate
    data_subset = data_subset[data_subset[lon].notna()]
    data_subset = data_subset[data_subset[lat].notna()]

    #create geodataframe
    data_subset=gpd.GeoDataFrame(data_subset, geometry=gpd.points_from_xy(data_subset[lon], data_subset[lat]))

    # Find nearest station
    #print('| Download %s data on hourly base from nearest %d stations...' % (dwd_category, no_of_nearest_stations))
    data_subset,dwd_base=weather.Find_nearest_dwd_stations(
        data_subset.copy(),
        date_start=data_subset.index.min().date().strftime('%Y%m%d'),
        date_end=data_subset.index.max().date().strftime('%Y%m%d'),
        data_category=dwd_category,
        parameters=dwd_parameters,
        temp_resolution=temporal_resolution,
        no_of_nearest_stations=no_of_nearest_stations,
        memory_save=False,
        Output=True)

    if dwd_base is None:
        data_subset[dwd_parameters] = np.nan
        return(data_subset)

    # add data to rover record
    print('|     Calculate DWD parameter value by idw method...')
    data_subset=weather.Apnd_dwd_data(data_subset.reset_index(),
                                  dwd_base,
                                  time_col='Date Time(UTC)',
                                  data_time_format='%Y-%m-%d %H:%M:%S',
                                  data_category=dwd_category,
                                  parameters=dwd_parameters,
                                  no_of_nearest_stations=no_of_nearest_stations,
                                  idw_exponent=2
                                  )
    #restore index
    data_subset=data_subset.set_index('Date Time(UTC)')
    #create temporary columns from dwd data extraction
    dwd_temporary_columns=[dwd_category+'_station_'+str(i) for i in range(0,no_of_nearest_stations)]
    dwd_temporary_columns.extend([dwd_category+'_distance_'+str(i) for i in range(0,no_of_nearest_stations)])
    #drop them
    data_subset.drop(columns=dwd_temporary_columns,inplace=True)
    #remove geometry
    data_subset.drop(columns='geometry',inplace=True)
    print('i     Retrieved DWD data to correct', str(len(data_subset)), 'data records')
    #write out
    #data_subset.to_csv(os.path.join(output_path,'dwd_'+output_variable+'.csv'))
    data_subset.to_csv(local_storage_file)

    return pd.DataFrame(data_subset)
