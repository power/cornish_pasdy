#!python 

import os
from pathlib import Path

this_path = Path(os.path.realpath(__file__))
corn_path = this_path.parents[2]
corny_path = corn_path.joinpath('instantPASDy.py')
corny_gui_path = corn_path.joinpath('cornyApp.py')

print('Creating folder           example_work_folder/')
os.makedirs('example_work_folder/input')

print('Creating shortcut         example_work_folder/Corny.bat')
with open('example_work_folder/Corny.bat', 'w') as f:
    f.write('python %s my_station.cfg' % corny_path)

print('Creating shortcut         example_work_folder/Corny-GUI.bat')    
with open('example_work_folder/Corny-GUI.bat', 'w') as f:
    f.write('python %s my_station.cfg' % corny_gui_path)

print('Copying example config to example_work_folder/my_station.cfg')
with open(corn_path.joinpath('examples/station.cfg'), 'rb') as src, open('example_work_folder/my_station.cfg', 'wb') as dst: dst.write(src.read())

print('Copying example data to   example_work_folder/input/station-data.zip')
with open(corn_path.joinpath('examples/input/station-data.zip'), 'rb') as src, open('example_work_folder/input/station-data.zip', 'wb') as dst: dst.write(src.read())

print('Finished. Copy "example_work_folder" anywhere on your disk and start playing with COSMOS data :)')
