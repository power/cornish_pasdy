@echo off

echo "Please make sure that you have Conda with Python 3.10.* installed first."
set /p yesno="Continue?"

echo "Installing via conda"
conda.exe install --file python_packages/requirements-conda.txt

echo "Installing via pip..."
python -m pip install --upgrade pip
pip install -r python_packages/requirements-conda-pip.txt

echo "Preparing example folder..."
python scripts/example_folder.py && pause

echo "Installing Kivy-Garden packages..."
garden install matplotlib

echo "Done."

