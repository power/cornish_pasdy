@echo off

echo "Please make sure that you have Python 3.10.* installed first."
set /p yesno="Continue?"

echo "Upgrading pip..."
python -m pip install --upgrade pip

echo "Installing precompiled python packages..."
pip install -r python_packages/requirements-windows-1.txt

echo "Installing more friendly python packages..."
pip install -r python_packages/requirements-windows-2.txt

echo "Installing Kivy-Garden packages..."
garden install matplotlib
REM python scripts/bugfix_kivy_garden.py

echo "Preparing example folder..."
python scripts/example_folder.py && pause

echo "Done."
