from timeit import default_timer as timer
#print('--- %5.2f GO!' % timer())

import subprocess
from threading import Thread
from multiprocessing import Process

# Adding matplotlib Agg before kivy and corny solves the threading bug, see: https://stackoverflow.com/questions/52839758/matplotlib-and-runtimeerror-main-thread-is-not-in-main-loop
import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
#plt.rcParams['axes.facecolor'] = 'none'

#import os
#os.environ["KIVY_NO_CONSOLELOG"] = "1"

from kivy.garden.matplotlib.backend_kivyagg import FigureCanvasKivyAgg

from kivy.config import Config
#Config.set('kivy','default_font', ['Consolas','fonts/consola.ttf','fonts/consolai.ttf','fonts/consolab.ttf','fonts/consolaz.ttf'])
#Config.set('kivy','default_font', ['Segeo UI','fonts/segoeui.ttf','fonts/segoeuii.ttf','fonts/segoeuib.ttf','fonts/segoeuiz.ttf'])
#Config.set('kivy','default_font', ['consola', 'fonts/consola.ttf', 'fonts/consola.ttf', 'fonts/consola.ttf', 'fonts/consola.ttf'])
#Config.set('input', 'mouse', 'mouse, disable_multitouch')# disable_multitouch, disable_hover 'mouse,multitouch_on_demand')
Config.set('kivy','window_icon', 'app/_ui/corny-icon-corn.png')
#Config.set('kivy', 'log_level', 'error')
#Config.set('kivy', 'log_name', "anything_you_want_%y-%m-%d_%_.log")
#Config.set('kivy', 'log_maxfiles', 1000)
            
from kivymd.app import MDApp as App

from kivymd.uix.floatlayout import MDFloatLayout

from kivymd.uix.button import MDFillRoundFlatIconButton, MDRectangleFlatButton
from kivymd.uix.expansionpanel import MDExpansionPanel, MDExpansionPanelOneLine
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.textfield import MDTextField
#from kivymd.uix.floatlayout import MDFloatLayout
from kivymd.uix.tab import MDTabsBase, MDTabs
from kivymd.uix.chip import MDChip
from kivymd.uix.label import MDLabel
from kivymd.uix.dialog import MDDialog

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.stacklayout import StackLayout
from kivymd.uix.stacklayout import MDStackLayout
#from kivy.uix.accordion import Accordion, AccordionItem
from kivy.uix.splitter import Splitter
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivymd.uix.gridlayout import MDGridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.image import Image
from kivy.uix.scrollview import ScrollView
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.switch import Switch
from kivy.uix.spinner import Spinner
from kivy.core.window import Window
from kivy.properties import ListProperty
from kivy.uix.checkbox import CheckBox
from kivy.uix.widget import Widget
from kivy.uix.recycleview import RecycleView
from kivy.uix.popup import Popup
from kivy.uix.behaviors import ButtonBehavior
from kivy.factory import Factory
from kivy.uix.popup import Popup
from kivy.lang import Builder
from kivy.uix.relativelayout import RelativeLayout
#from kivymd.app import MDApp
from kivy.event import EventDispatcher
from kivy.properties import ObjectProperty, ListProperty, StringProperty, NumericProperty, Clock, partial
from kivy.utils import rgba
from kivy.clock import mainthread, Clock, _default_time as deftime  # ok, no better way to use the same clock as kivy, hmm

from shutil import copyfile
import time
import sys

from kivymd.uix.list import OneLineListItem
from kivymd.uix.menu import MDDropdownMenu
from kivymd.uix.dropdownitem import MDDropDownItem
#from kivy.garden.matplotlib.backend_kivyagg import FigureCanvasKivyAgg
from kivy.uix.dropdown import DropDown

import pandas
import os
import re
from functools import partial
import configparser
from ast import literal_eval as make_tuple

# pos where instanpasdy was imported 
_orig_print = print
_corny_print = print

#print('(%5.2f) Finished imports.' % timer())

re_file_out = re.compile('^< (.+) saved to (.+\.\w+) \((.+)\)')

@mainthread
def print(*args, **kwargs):
    if 'flush' in kwargs:
        _corny_print(*args, **kwargs)
    else:
        _corny_print(*args, flush=True, **kwargs)
    app = App.get_running_app()
    #if hasattr(Root, 'logwin'):
    if hasattr(app, 'data'):
        if 'end' in kwargs:
            end = kwargs['end']
        else:
            end = "\n"
        
        text = " ".join(map(str, args))
        #text = str(args[0])
        text = re.sub(r'^.*\[\d+m', '(skipped colored input)', text)
        if text.startswith(r'\\033'):
            #if text.index('[3')>0:
            text = text[1+text.index('m'):]
        #Root.logwin.insert_text(text + end)
        #Root.logwin.reset_undo()
        #Root.logwin.do_undo()
        if end=='\r':
            app.update_log(text, whole_line=True)
        else:
            if app.log_line_hold:
                app.update_log(text)
            else:
                app.add_log(text)
            
            if len(text)>0: # and app.root.update_progressbar_on_print:
                if text[0] == '[':
                    #app.make_plot(3)
                    #Clock.schedule_once(lambda x: app.make_plot(4))
                    pass
                    
                elif text[0] == '=':
                    Root.update_progressbar(app.root, 0)
                    #app.make_plot(4)
                    
                elif text[0] == '<':
                    match = re_file_out.search(text)
                    if match:
                        Exec_button = ExecuteFileButton(text='%s (%s)' % (match.group(1), match.group(3)))
                        Exec_button.file_source = match.group(2)
                        if Exec_button.file_source.endswith('csv'):
                            Exec_button.icon = 'microsoft-excel' # file-delimited-outline
                            
                            if match.group(1) == 'Data':
                                #app.make_plot(3.51) #Clock.schedule_once(lambda x: )
                                # Make plot
                                Root.output_csv_df = pandas.read_csv(match.group(2),
                                    parse_dates=True, index_col=0, skipinitialspace = True)
                                Root.fill_dd_items(app.root)
                                Root.set_dd_item(app.root, 'moisture_vol')
                                #app.make_plot(3.52)
                               
                        elif Exec_button.file_source.endswith('pdf'):
                            Exec_button.icon = 'file-chart-outline'
                            #app.make_plot(3.6)
    
                        elif Exec_button.file_source.endswith('kml'):
                            Exec_button.icon = 'file-marker-outline'
                            #app.make_plot(3.7)
                            Root.create_map(app.root, match.group(2))
                            
                        elif Exec_button.file_source.endswith('cfg'):
                            Exec_button.icon = 'file-cog-outline'
                        elif Exec_button.file_source.endswith('dat'):
                            Exec_button.icon = 'file-table-outline'
                        elif Exec_button.file_source.endswith('geojson'):
                            pass
                            #app.make_plot(3.9)
                        
                        app.root.Tab_export_box.add_widget(Exec_button)
                    Root.update_progressbar(app.root, 0, diff=1)
                else:
                    Root.update_progressbar(app.root, 0, diff=1)
                
        if end=='':
            app.log_line_hold = True
        else:
            app.log_line_hold = False
        
        #print(app.root.update_progressbar_on_print)
        
#     if args and not args[0].startswith(' '):
#         #global progress
#         self.progress += 1
#         if self.progress >= 100: self.progress = 99
#         self.Pb.value = self.progress
#         self.Pblbl.text = args[0].strip().split(' ', 1)[0]

redirect = dict(print=print) # set to ever function call

def RunCorny(*args):
    #import contextlib, io
    #f = io.StringIO()
    #with io.StringIO() as buf, contextlib.redirect_stdout(buf):
    #with contextlib.redirect_stdout(f):
    
    import instantPASDy
    _corny_print = instantPASDy.print
    instantPASDy.print = print # pipes print hereto
    #instantPASDy.corny.io = print # pipes print hereto

    instantPASDy.main(*args)
        #print(buf.getvalue())
    #output = f.getvalue()
    #print(output)


class ConsoleLabel(Label):
    """
    # Based on Tshirtman's answer
    https://stackoverflow.com/questions/43666381/wrapping-the-text-of-a-kivy-label
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        
        self.bind(
            # Width
            width=lambda *x:
            self.setter('text_size')(self, (self.width, None)),
            texture_size=lambda *x: self.setter('height')(self, self.texture_size[1]))

class FixedRecycleView(RecycleView):
    """
    Log view based on <https://github.com/kivy/kivy/blob/master/examples/widgets/recycleview/infinite_scrolling.py>
    """
    distance_to_top = NumericProperty()
    scrollable_distance = NumericProperty()

    def on_scrollable_distance(self, *args):
        """This method maintains the position in scroll, by using the saved
        distance_to_top property to adjust the scroll_y property. Only if we
        are currently scrolled back.
        """
        if self.scroll_y > 0:
            self.scroll_y = (
                (self.scrollable_distance - self.distance_to_top)
                / self.scrollable_distance
            )

    def on_scroll_y(self, *args):
        """Save the distance_to_top everytime we scroll.
        """
        self.distance_to_top = (1 - self.scroll_y) * self.scrollable_distance

class WrappedLabel(ButtonBehavior, Label):
    """
    # Based on Tshirtman's answer
    https://stackoverflow.com/questions/43666381/wrapping-the-text-of-a-kivy-label
    """
    def __init__(self, **kwargs):
    
        if 'subbox' in kwargs:
            print(kwargs['subbox'])
            self._subbox = kwargs['subbox']
            del kwargs['subbox']
    
        super().__init__(**kwargs)
        
        
        self.bind(
            # Width
            width        = lambda *x: self.setter('text_size')(self, (self.width, None)),
            texture_size = lambda *x: self.setter('height')(self, self.texture_size[1]),
            )
            # Toggle
            #on_press = self.Toggle)
        
        
    def Toggle(self, something):
        print('hello')
        Root.isShownMenu = not Root.isShownMenu
        self._subbox.size_hint = (0, 0)
        self._subbox.opacity = 0

class LoadDialog(FloatLayout):
    load = ObjectProperty(None)
    cancel = ObjectProperty(None)


class SaveDialog(FloatLayout):
    save = ObjectProperty(None)
    text_input = ObjectProperty(None)
    cancel = ObjectProperty(None)

#class Separator(Widget):
#    sep_color = ListProperty([1,0,0,1])

script_path = os.path.dirname(os.path.abspath( __file__ ))+'/'
working_dir = os.getcwd()

class TextInputMD(MDTextField):
    def __init__(self, label='', units='', **kwargs):
        if units != '':
            units = ' (%s)' % units
        kwargs['hint_text'] = label + units 
        #kwargs['helper_text'] = '(%s)' % units
        super().__init__(**kwargs)

###################################
########################### App ###
###################################
class CornyApp(App):
    script_path = script_path #os.path.dirname(os.path.abspath( __file__ ))+'/'
    working_dir = working_dir
    data = ListProperty()
    log_line_hold = False
    
    def build(self):
        
        '''Theme'''
        self.theme = dict()
        theme_file      = script_path + 'app/default.styles'
        theme_file_user = script_path + 'app/user.styles'
        if os.path.exists(theme_file_user):
            theme_file = theme_file_user
            
        with open(theme_file, 'r') as f:
             for line in f:
                 if line.startswith('#'):
                     continue
                 else:
                     line = line.strip()
                     elements = line.split('=')
                     if len(elements) == 2:
                         key = elements[0].strip()
                         value = elements[1].strip()
                         self.theme[key] = make_tuple(value)
        #return Root()
        #self.theme_cls.theme_style = "Dark"
        #self.theme_cls.primary_palette = "Indigo"
        #self.theme_cls.accent_palette = "Red"
        #print('(%5.2f) Building...' % timer())
        self.i = 0
        '''KV'''
        return Builder.load_file(self.script_path + 'app/corny.kv') #os.path.join(dirname(__file__), '../view/app1.kv')

    def update_size(self, index, size):
        """Maintain the size data for a log entry, so recycleview can adjust
        the size computation.
        As a log entry needs to be displayed to compute its size, it's by
        default considered to be (0, 0) which is a good enough approximation
        for such a small widget, but you might want do give a better default
        value if that doesn't fit your needs.
        """
        self.data[index]['cached_size'] = size

    def add_log(self, dt):
        """Produce random text to append in the log, with the date, we don't
        want to forget when we babbled incoherently.
        """
        bgcolor = rgba("#EDEDE7")
        color = 'black'
        font_name = script_path + 'app/_ui/fonts/consola.ttf'

        if len(dt)>0:
            if dt[0:2] == '> ': color = '#C19C00'
            if dt[0:2] == '< ': color = 'green'
            if dt[0:2] == '# ': color = 'purple'
            if dt[0:2] == 'i ': color = '#3A96DD'
            if dt[0:2] == '! ': color = 'red'
            if dt[0] == '[': font_name = script_path + 'app/_ui/fonts/consolab.ttf'
            if re.match('^\s*\w+Error:', dt):
                color = 'red'

        self.data.append({
            'index': len(self.data),
            'text': dt,
            'cached_size': (0, 17),
            'color': color,
            'font_name': font_name
            #'bgcolor': bgcolor
             })

    def update_log(self, dt, whole_line=False):
        """Produce random text to append in the log, with the date, we don't
        want to forget when we babbled incoherently.
        """
        if len(self.data)>0:
            if whole_line:
                self.data[-1]['text'] = dt
            else:
                self.data[-1]['text'] += dt
        else:
            self.add_log(dt)
    
    """
    def make_plot(self, var=None):
        print('Trying to plot', var)
        self.get_running_app().root.plot_canvas = FigureCanvasKivyAgg(plt.gcf())
        self.get_running_app().root.Tab_plots_box.add_widget(self.get_running_app().root.plot_canvas)
        
        plt.cla()
        #ax, fig = plt.subplots(1,1, facecolor=(1, 0.5, 1))
        if var is None:
            plt.scatter([3,4,5], [0,0,0])
        else:
            plt.scatter([3,4,5], [var,var,var])
        plt.grid(color='black', alpha=0.2)
        plt.tight_layout()
        #plt.gcf().patch.set_facecolor('green') # does not work
        self.get_running_app().root.plot_canvas.draw_idle()
        return()
    
    def on_start(self, **kwargs):
        #self.get_running_app().root.load_config()
        #for expansion_panel in expansion_panels: 
            #print(expansion_panel)
        #    expansion_panel.open_panel(expansion_panel, True)
        #    expansion_panel.dispatch("on_open")
            #expansion_panel._anim_playing = False
            #expansion_panel.close_panel(expansion_panel, False)
            #expansion_panel.dispatch("on_close")
        print('GUI load time: %.2f seconds.' % timer())
    """    

    def execute(self, file):
        import subprocess, platform
        file = os.path.abspath(file)
        if platform.system() == 'Darwin':       # macOS
            subprocess.call(('open', file))
        elif platform.system() == 'Windows':    # Windows
            os.startfile(file)
        else:                                   # linux variants
            subprocess.call(('xdg-open', file))
        

    def weblink(self, value): #instance, 
        import webbrowser
        webbrowser.open(value)



class ImageButton(ButtonBehavior, Image):
    pass


#class Tab(ScrollView, MDTabsBase):
class Tab(RecycleView, MDTabsBase):
    '''Class implementing content for a tab.'''

class TabFloat(MDFloatLayout, MDTabsBase):
    '''Class implementing content for a tab.'''


expansion_panels = []

class Subsection(MDExpansionPanel):
    def __init__(self, label='', content=None, **kwargs):
        kwargs['panel_cls'] = MDExpansionPanelOneLine(text=label)
        kwargs['content'] = content
        super().__init__(**kwargs)
        expansion_panels.append(self)
    

class SelectChip(MDChip):
    _no_ripple_effect = True

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def on_long_touch(self, *args):
        # MDChips behave on long touch by default, use this to deactivate it:
        pass

    def on_press(self, *args):
        # Overwrite MDChips on_press() to make it behave like it should.
        self.active = True if not self.active else False

    def on_active(self, instance_check, active_value: bool):
        pass

class SelectChip_no(SelectChip):
    pass
    
class SelectChip_or(SelectChip):
    def on_press(self, *args):
        # Overwrite MDChips on_press() to make it behave like it should.
        self.active = True if not self.active else False
        if self.active:
            for chip in self.parent.children:
                if chip.active and chip != self:
                    chip.active = False

class SelectChip_or_no(SelectChip_or):
    pass
    
    
class SelectChip_xor(SelectChip):
    def on_press(self, *args):
        # Overwrite MDChips on_press() to make it behave like it should.
        if not self.active:
            self.active = True
            for chip in self.parent.children:
                if chip.active and chip != self:
                    chip.active = False

class SelectChip_xor_no(SelectChip_xor):
    pass

class FillSpaceWidget(RecycleView):
    pass

class ExecuteFileButton(MDFillRoundFlatIconButton):
    file_source = None

class DDButton(MDRectangleFlatButton):
    pass
    
re_section = re.compile(r'^\[(.+)\]')
re_commenthead = re.compile(r'^\#\# (.+)')
re_comment = re.compile(r'^\# (.+)')
re_keyval  = re.compile(r'^(\w+)\s*=(.*)')
re_multiline = re.compile(r'^\s+(\w.+)')

re_table     = re.compile(r'\(table\)')
re_selectone = re.compile(r'\(select one\)')
re_selectany = re.compile(r'\(select any\)')
re_selectitems = re.compile(r'^\# - (.+) \# (.+)')
re_units = re.compile(r'^\# Units: (.+)')
re_yesno     = re.compile(r'\(yes\/no\)')

##################################
##################################
##################################            
class Root(BoxLayout):

    box = BoxLayout()
    isShownMenu = True
    update_progressbar_on_print = False

    def __init__(self, **kwargs):
        
        super(Root, self).__init__(**kwargs)
        
        self.theme = App.get_running_app().theme
        
        self.is_loading = False
        self.orientation='vertical'
        
        Window.maximize()
        Window.clearcolor = self.theme['background_color'] #(0.93, 0.93, 0.91, 1)
        self.progress = 0
        self.working_dir = os.getcwd()
        if len(sys.argv) > 1:
            self.config_filename = sys.argv.pop(1)
        else:
            self.config_filename = script_path + '/examples/rover.cfg'

        self.__lines = []
        self.__clock_event = None
        self.list_exp_panels = []

        info_text = 'An open-source community product to make cosmic-ray neutron related data analysis accessible for everyone! Developed by Martin Schrön, Erik Nixdorf, David Schäfer, Jannis Jakobi, Carmen Zengerle, Rafael Rosolem, and Steffen Zacharias. Made possible by the Helmholtz Centre for Environmental Research (UFZ), Leipzig, Germany.'
        #[ref=https://git.ufz.de/CRNS/cornish_pasdy][color=8888ff]git.ufz.de/CRNS/cornish_pasdy[/ref]
        #info_label = Label(text=info_text, markup=True)
        #info_label.bind(on_ref_press=weblink)

        #self.info_popup = Popup(title='CoRNy info', content=info_label, size_hint=(0.5, 0.5))
        self.info_popup = MDDialog(
            type = 'simple',
            #radius = [20, 7, 20, 7],
            title = 'CoRNy -- Comic-Ray Neutron processing toolbox for python',
            text = info_text,
            md_bg_color = self.theme['background_color'],
            #content_cls = info_label,
            buttons=[
                MDFillRoundFlatIconButton(text="Visit GitLab repo", icon='gitlab', icon_size='32sp',
                    md_bg_color=self.theme['active1_color'], icon_color=self.theme['text1_color'], text_color=self.theme['text1_color'],
                    on_press=lambda x: App.get_running_app().weblink('https://git.ufz.de/CRNS/cornish_pasdy'))
            ]
        )
        
        P_top = GridLayout(cols=5, height=62, size_hint_y=None, padding=5, spacing=(5,0))
        #P_top.add_widget(Label(text='CoRNy', color='black', size_hint_x=0.05))
        corny_logo = ImageButton(source='app/_ui/corny-logo.png', size_hint_x=0.14)
        corny_logo.bind(on_press=self.info_popup.open)
        P_top.add_widget(corny_logo)


        #P_top.add_widget(Label(text='Config:', color='black', size_hint_x=0.05, halign='right', valign='middle'))
        self.Ti_config = TextInputMD(label='Configuration file', units='full file path and name *.cfg',
                                    text=self.config_filename, multiline=False,
                                    size_hint_x=0.50, size_hint_y=None, height=30,
                                    font_name= 'RobotoMono-Regular')
        self.Ti_config.bind(on_text_validate=self.load)
        P_top.add_widget(self.Ti_config)
        #B_open = Button(text ="Load", background_color =(0.5, 0.49, 0.43, 0.5), background_normal='', color='black', size_hint_x=0.05)
        
        B_browse = MDFillRoundFlatIconButton(text ="Browse", icon='file-search-outline', icon_size='32sp',
            md_bg_color =self.theme['active3_color'], icon_color=self.theme['text1_color'], text_color=self.theme['text1_color'], size_hint_x=0.08)
        B_open = MDFillRoundFlatIconButton(text ="Load", icon='file-download-outline', icon_size='32sp',
            md_bg_color =self.theme['active2_color'], icon_color=self.theme['text1_color'], text_color=self.theme['text1_color'], size_hint_x=0.08)
        #B_open = MDFillRoundFlatIconButton(text ="Load", icon='run-fast', md_bg_color =(0.5, 0.49, 0.43, 0.5), text_color='black', size_hint_x=0.05)
        B_browse.bind(on_release = self.show_load) #lambda x: App.get_running_app().make_plot(5)
        B_open.bind(on_release = self.thread_load_config)
        P_top.add_widget(B_browse)
        P_top.add_widget(B_open)
        #B_run = Button(text ="Save & Run", background_color=(0.93, 0.83, 0.42, 1), background_normal='', color='black', size_hint_x=0.12)
        B_run = MDFillRoundFlatIconButton(text ="Save & Run", icon='magic-staff', icon_size='32sp',
            md_bg_color =self.theme['active1_color'], icon_color=self.theme['text1_color'], text_color=self.theme['text1_color'], size_hint_x=0.15)
        
        B_run.bind(on_press = self.save_run)
        P_top.add_widget(B_run)

        #P_Pb = BoxLayout(orientation='vertical', size_hint_x=0.17)
        #self.Pblbl = Label(text='Ready', color='black', size_hint_y=0.7, valign='top')
        #self.Pb = ProgressBar(max=100, size_hint_y=0.3)
        #P_Pb.add_widget(self.Pblbl)
        #P_Pb.add_widget(self.Pb)
        #P_top.add_widget(P_Pb)


        P_tabs = GridLayout(cols=2)
        #Panel_tabs.height = self.height - 50
        
        from kivymd.uix.progressbar import MDProgressBar 
        self.add_widget(P_top)
        self.Progressbar = MDProgressBar()
        self.add_widget(self.Progressbar)
        self.add_widget(P_tabs)
        
        self.Tb_config = MDTabs() #TabbedPanel(do_default_tab=False, background_color=(0.93, 0.93, 0.91, 1), background_image='', tab_width=None)
        self.Tb_output = MDTabs() #TabbedPanel(do_default_tab=False, background_color=(0.93, 0.93, 0.91, 1), background_image='')
        
        
        #Tb_config_info = TabbedPanelHeader(text='Info', background_color=(1, 1, 1, 1))
#        self.lpane = ScrollView(size_hint=(1, None), size=(Window.width, Window.height), scroll_type=['bars'], bar_pos_y='left', bar_width=10)
#        Tb_config_info.content = self.lpane
#        Tb_config.add_widget(Tb_config_info)

#        Tb_config_input = TabbedPanelHeader(text='Input', background_color=(1, 1, 1, 1))
#        self.lpane2 = ScrollView(size_hint=(1, None), size=(Window.width, Window.height), scroll_type=['bars'], bar_pos_y='left', bar_width=10)
#        Tb_config_input.content = self.lpane2
#        Tb_config.add_widget(Tb_config_input)

        #P_tabs.add_widget(self.Tb_config)
        
        splitter = Splitter(sizable_from = 'right')
        splitter.add_widget(self.Tb_config)
        splitter.min_size = 200
        splitter.strip_size = '7pt'
        P_tabs.add_widget(splitter)
        #P_tabs.add_widget(self.Tb_config)
        
        P_tabs.add_widget(self.Tb_output)

#        self.lpanelayout = BoxLayout(orientation='vertical', size_hint_y=None, height=0, spacing=20)  # GridLayout(cols=1, spacing=1, size_hint_y=None)
#        self.lpanelayout.do_scroll_y=True
#        self.lpanelayout.bind(minimum_height=self.lpanelayout.setter('height'))

        Tb_output_log = Tab(title='Log') #TabbedPanelHeader(text='Log', width=20+9*len('Log'), size_hint_x=None)
        #self.rpane3 = GridLayout(cols=1)
        sv_l = RecycleView(size_hint=(1, 1), scroll_type=['bars'], bar_pos_y='right',
            bar_width=10, do_scroll_x=False, do_scroll_y=True, scroll_y=0)
        sv_lcontent = BoxLayout(orientation='vertical', size_hint_y=None, height=0, spacing=1, padding=40)
        Tb_output_log.add_widget(sv_l) #Tb_output_log.content = sv_l
        ###self.logwin = TextInput(allow_copy=True, multiline=True)
        ###self.logwin.bind(minimum_height=self.logwin.setter('height'))
        ###sv_l.add_widget(self.logwin)
        #self.Tb_output.add_widget(Tb_output_log)

        Tab_Log = Tab(title='Log') #TabbedPanelHeader(text='Log')
        Tab_Log_grd = GridLayout(cols=1, size_hint_y=1, padding=(0,5))
        Tab_Log_grd.add_widget(FixedRecycleView())
        #self.rpane.add_widget(WrappedLabel(text='This feature is not yet active.', halign='left', valign='middle', color='grey', size_hint=(1, None)))
        #self.rpane.add_widget(FixedRecycleView()) #size_hint=(1, None)))
        Tab_Log.add_widget(Tab_Log_grd) #Tb_output_plot.content = self.rpane
        self.Tb_output.add_widget(Tab_Log)
        
        #self.Tb_output.add_widget(Tb_output_diag)
        
        
        Tab_export = Tab(title='Files') # TabbedPanelHeader(text='Export')
        self.Tab_export_box = MDStackLayout(adaptive_height=True, spacing=5, padding=(30,30,30,30))
        self.Tab_export_box.add_widget(WrappedLabel(text='Directly open the generated output files from here.',
            halign='left', valign='middle', color=self.theme['text3_color'], size_hint=(1, None)))
        Tab_export.add_widget(self.Tab_export_box)
        self.Tb_output.add_widget(Tab_export)
        #self.rpane4 = GridLayout(cols=1)
        
        Tab_plots = Tab(title='Plots') # TabbedPanelHeader(text='Export'
        self.Tab_plots_box = MDBoxLayout(orientation='vertical', adaptive_height=True, size_hint=(1, 1))
        self.Tab_plots_box_row = MDBoxLayout(orientation='horizontal', adaptive_height=True,
            height=56, padding=5, size_hint_x=0.6)
        
        self.Tab_plots_box_row_label = WrappedLabel(text='Variable:',
            color=self.theme['text3_color'], valign='middle', size_hint_x=0.15)
        self.Tab_plots_box_row.add_widget(self.Tab_plots_box_row_label)
        
        self.Tab_plots_ddi = DropDown()
        self.Tab_plots_ddi_main = DDButton(text='', size_hint=(None, None))
        self.Tab_plots_ddi_main.bind(on_release=self.Tab_plots_ddi.open)
        self.Tab_plots_ddi.bind(on_select=lambda instance, x: self.set_dd_item(x))
        self.Tab_plots_box_row.add_widget(self.Tab_plots_ddi_main)
        self.Tab_plots_box.add_widget(self.Tab_plots_box_row)
        
        #import matplotlib
        #matplotlib.use('module://kivy.garden.matplotlib.backend_kivy')
        #fig, ax = plt.subplots(1,1)
        #self.plot_canvas = FigureCanvas(fig) #fig.canvas
        #self.plot_canvas = FigureCanvasKivyAgg(plt.gcf())
        #global mycanvas
        #mycanvas = self.plot_canvas
        #self.Tab_plots_box.add_widget(self.plot_canvas)
        self.Tab_plots_box.add_widget(FillSpaceWidget())
        
        #self.make_plot('x')
        
        
        Tab_plots.add_widget(self.Tab_plots_box)
        self.Tb_output.add_widget(Tab_plots)
        
        self.Tab_map = TabFloat(title='Map')
        
        self.Tab_map_box = MDBoxLayout(orientation='vertical', adaptive_height=True, size_hint=(1, 1))
        self.Tab_map_box_row = MDBoxLayout(orientation='horizontal', adaptive_height=True,
            height=20, padding=5, size_hint_x=0.6)
        self.Tab_map_box_row.add_widget(Image(source=script_path+'app/_ui/colorscale.png', height=20, size_hint=(1, None)))
        self.Tab_map_box.add_widget(self.Tab_map_box_row)
        self.Tab_map_box.add_widget(FillSpaceWidget())
        self.Tab_map.add_widget(self.Tab_map_box)
        
        self.Tb_output.add_widget(self.Tab_map)
        #testboxlayout.add_widget(export_label)
        
        #testacc = Accordion(orientation='vertical', size_hint=(1, 1))
        
        #testacc_item = AccordionItem(title='test1')
        #testacc.add_widget(testacc_item)
        
        #testacc_item_sv = ScrollView(size_hint=(1, 1), scroll_type=['bars'], bar_pos_y='left', bar_width=10, do_scroll_x=False, do_scroll_y=True)
        #testacc_item.add_widget(testacc_item_sv)
        
        #testacc_item_sv_box = BoxLayout(orientation='vertical', size_hint_y=None, height=0, spacing=1, padding=10)
        #testacc_item_sv.add_widget(testacc_item_sv_box)
        
        #box_items = []
        #box_items.append(testacc_item_sv_box)
        #box_items[0].bind(minimum_height=box_items[0].setter('height'))
                
        #self.Ac_subsection[s].add_widget(testacc_item)
        #gr = GridLayout(cols=2, size_hint=(1, None), height=38, padding=5, spacing=5) #self.width/2
        #gr.bind(width=box_items[0].setter('width'))
        #gr.label2 = Label(text='wuff', halign='right', valign='top', color='black', size_hint=(0.4, 1), font_name='app/_ui/fonts/consola.ttf', padding_y=6)
        #gr.label2.bind(size=gr.label2.setter('text_size'))
        #gr.add_widget(gr.label2)
        #gr.file_input = TextInput(multiline=False, text='waff', size_hint=(0.6, 1))
        #gr.add_widget(gr.file_input)
                    
                    
        #box_items[0].add_widget(gr)
        #box_items[0].height += gr.height
        
        #testacc.add_widget(AccordionItem(title='test2'))
        #testboxlayout.add_widget(testacc)
        #self.rpane4.add_widget(testboxlayout)

        #self.add_widget(self.rpane)
        #row = GridLayout(cols=2)
        #self.rpane.add_widget(self.btn)
        #self.box.add_widget(FigureCanvasKivyAgg(plt.gcf()))
        #self.rpane.add_widget(self.box)
        ###self.add_widget(row)
        # Using readlines()
    
    #@mainthread
    def set_dd_item(self, var):
        self.Tab_plots_ddi_main.text = var
        Clock.schedule_once(lambda x: self.make_plot(var))
    
    #@mainthread
    def fill_dd_items(self):
        for dditem in self.Tab_plots_ddi.children[0].children:
            self.Tab_plots_ddi.remove_widget(dditem)
            
        for var in self.output_csv_df.columns:
            btn = DDButton(text=var, size_hint_y=None, height=44)
            btn.bind(on_release=lambda btn: self.Tab_plots_ddi.select(btn.text))
            self.Tab_plots_ddi.add_widget(btn)
    
    #@mainthread
    def make_plot(self, var):
        if len(self.Tab_plots_box.children) > 1:
            self.Tab_plots_box.remove_widget(self.Tab_plots_box.children[0])
        for child in self.Tab_plots_box.children:
            if isinstance(child, FigureCanvasKivyAgg) or isinstance(child, FillSpaceWidget):
                self.Tab_plots_box.remove_widget(child)
        
        fig, ax = plt.subplots(1,1)
        self.plot_canvas = FigureCanvasKivyAgg(fig) #plt.gcf())
        self.Tab_plots_box.add_widget(self.plot_canvas)
        
        #plt.cla()
        self.output_csv_df[var].plot()
        plt.grid(color='black', alpha=0.2)
        plt.xlabel('')
        #plt.gcf()
        fig.patch.set_facecolor(self.theme['background_color'])
        plt.tight_layout()
        self.plot_canvas.draw_idle()
        return()
        
        
    #@mainthread
    def create_map(self, kml_file):
        import geopandas as gpd
        import fiona 
        #gpd.io.file.fiona.drvsupport.supported_drivers['KML'] = 'rw'
        fiona.drvsupport.supported_drivers['KML'] = 'rw'

        geo_df = gpd.read_file(kml_file, driver='KML')
        import pandas as pd
        df= pd.DataFrame(geo_df)
        df['lat'] = df.geometry.apply(lambda p: p.y)
        df['lon'] = df.geometry.apply(lambda p: p.x)

        from kivy_garden.mapview import MapView, MapMarker, MapMarkerPopup
        map = MapView(zoom=13, lat=float(df['lat'].median()), lon=float(df['lon'].median()))
        map.map_source = "osm"
        for index, row in df.iterrows():
            sm = float(row['Name'][0:-1])
            if sm<5: cs = '05'
            elif sm<10: cs = '10'
            elif sm<20: cs = '20'
            elif sm<30: cs = '30'
            elif sm<40: cs = '40'
            elif sm<50: cs = '50'
            else: cs = '60'
            marker_i = MapMarkerPopup(lat=float(row['lat']), lon=float(row['lon']),
                source=script_path+'app/_ui/marker_%s.png' % cs, anchor_y=0.5)
            map.add_marker(marker_i)

        RL = RelativeLayout()
        #if len(self.Tab_map.children) > 0:
        #    self.Tab_map.remove_widget(self.Tab_map.children[0])
        if self.Tab_map_box:
            if len(self.Tab_map_box.children) > 1:
                self.Tab_map_box.remove_widget(self.Tab_map_box.children[0])
            
        #self.Tab_map_box = MDBoxLayout(orientation='vertical', adaptive_height=True, size_hint=(1, 1))
        #self.Tab_map_box.add_widget(Image(source=script_path+'app/_ui/colorscale.png', height=20, size_hint=(1, None)))
        self.Tab_map_box.add_widget(map)
        #RL.add_widget(self.Tab_map_box)
        #self.Tab_map.add_widget(RL)
    
    def create_map_station(self, lat, lon):
        from kivy_garden.mapview import MapView, MapMarker, MapMarkerPopup
        map = MapView(zoom=15, lat=lat, lon=lon)
        map.map_source = "osm"
        marker_1 = MapMarkerPopup(lat=lat, lon=lon,
            source=script_path+'app/_ui/marker_05.png', anchor_y=0.5)
        map.add_marker(marker_1)
        
        RL = RelativeLayout()
        #if len(self.Tab_map.children) > 0:
        #    self.Tab_map.remove_widget = self.Tab_map.children[0]
        #RL.add_widget(map)
        #self.Tab_map.add_widget(RL)
        if self.Tab_map_box:
            if len(self.Tab_map_box.children) > 1:
                self.Tab_map_box.remove_widget(self.Tab_map_box.children[0])
            
        #self.Tab_map_box = MDBoxLayout(orientation='vertical', adaptive_height=True, size_hint=(1, 1))
        #self.Tab_map_box.add_widget(Image(source=script_path+'app/_ui/colorscale.png', height=20, size_hint=(1, None)))
        self.Tab_map_box.add_widget(map)
        #RL.add_widget(self.Tab_map_box)
        #self.Tab_map.add_widget(RL)
    
    def thread_load_config(self, *args):
        self.is_loading = True
        # Clear existing Tabs
        for tab in self.Tb_config.get_tab_list():
            self.Tb_config.remove_widget(tab)
        
        # Get substantial config from file:
        self.config_filename = self.Ti_config.text
        print('> Reading %s ...' % self.config_filename)
        
        self.__config = configparser.ConfigParser(interpolation=None)
        temp = self.__config.read(self.config_filename, encoding='utf-8') 
        
        # Default config
        self.config_default = script_path + 'default.cfg'
        
        #Thr = Thread(target=self.load_config) #, args=(self.get_running_app().root))
        #Thr.setDaemon(True)
        #Thr.start()
        ###self.logwin.text = ''
        ###Root.logwin = self.logwin
        self.__clock_event = Clock.schedule_interval(self.generate_from_line, 0)
        self.load_config_read()
        

    @mainthread
    def update_progressbar(self, value, diff=0):
        if self:
            if diff == 0:
                self.Progressbar.value = value 
            else:
                self.Progressbar.value = self.Progressbar.value + diff
        
    @mainthread
    def make_widget(self, w, **kwargs):
        #if w == 'WrappedLabel': widget = WrappedLabel
        #elif w == 'Tab': widget = Tab
        wobj = globals()[w]
        return( wobj(**kwargs) )
    
    def load_config_read(self, *args):
        
        #self.fig = MDTabs()
        #self.fig.clear_widgets()
        #self.fig.clear_tabs()
        file_config = open(self.config_default, 'r', encoding="utf-8")

        self.config_lines = []
        self.config_keys = dict()

        self.__section = ''
        self.__subsection = ''
        self.__sv_l = None
        self.__sv_llayout = []

        self.__item_type = ''
        self.__units = ''
        self.__selectitems = []
        self.__selectitemc = []
        self.__multiline_attach = False
        self.__current_section = None
        self.__current_subsection = None
        self.Ac_subsection = []
        self.__l = 0
        self.__s = -1
        self.__ss = -1

        lines = file_config.readlines()
        self.__num_lines = len(lines)

        self.__lines = []
        for line in lines:
            #self.__l += 1
            self.__lines.append(line)
    
    def generate_from_line(self, *args):

        if not self.__lines and not self.__clock_event is None:
            self.__clock_event.cancel()
            #for expansion_panel in expansion_panels: 
            #    #print(expansion_panel)
            #    expansion_panel.open_panel(expansion_panel, True)
            #    expansion_panel.dispatch("on_open") 
            #    expansion_panel.close_panel(expansion_panel, True)
            #    expansion_panel.dispatch("on_close") 
            self.update_progressbar(0)
            print('= Finished interface generation. You can now start configuring :-)')
            self.is_loading = False
            #for panel in self.list_exp_panels:
            #    Clock.schedule_once(lambda x: panel.open_panel())
            #for panel in self.list_exp_panels:
                #panel.close_panel(panel, False)
                
            #return()

        while self.__lines and deftime() < (Clock.get_time() + 1/60):
            line = self.__lines.pop(0)
            self.__l += 1
            self.update_progressbar(int(self.__l/self.__num_lines*100))
                
            if line.strip() == '':
                self.config_lines.append(line)

            match = re_section.search(line)
            if match:
                """
                [section] -> Tab
                """
                self.config_lines.append(line)
                self.__section = match.group(1)
                self.__current_section = self.__section
                self.__s += 1
                self.__ss = -1
                print('| Section %s ...' % self.__section)
                # make new tab
                Tb_config_l = Tab(title=self.__section.capitalize()) #TabbedPanelHeader(text=section.capitalize(), size_hint_x=None, width=20+9*len(section)) #background_color=(0.81, 0.8, 0.73, 1), background_image='',
                sv_lcontent = MDBoxLayout(orientation='vertical', adaptive_height=True, spacing=1, padding=(20,20,5,20)) #ltrb #, size_hint_y=None, height=0, spacing=1, padding=40)
                Tb_config_l.add_widget(sv_lcontent)
                self.__sv_llayout.append(sv_lcontent)  # GridLayout(cols=1, spacing=1, size_hint_y=None)
                self.Tb_config.add_widget(Tb_config_l)
                # Clear item type as no more multiline data is expected
                self.__multiline_attach = False
                # ss
                self.__ss_sv_l = None
                self.__ss_sv_llayout = []
                
            match = re_commenthead.search(line)
            if match:
                """
                ## Comment head -> Expansion Panel
                """
                self.config_lines.append(line)
                commenthead = match.group(1).strip()
                self.__ss += 1
                
                ss_sv_lcontent = MDBoxLayout(orientation='vertical', adaptive_height=True, 
                    padding=(0,15,0,10), spacing=15) #, md_bg_color=(0.5, 0.49, 0.43, 0.05))
                
                ss_Ac_item = Subsection(commenthead.capitalize(), ss_sv_lcontent)
                
                self.__sv_llayout[self.__s].add_widget(ss_Ac_item)
                
                #ss_Ac_item.check_open_panel(ss_Ac_item)
                self.__ss_sv_llayout.append(ss_sv_lcontent)
                # Clear item type as no more multiline data is expected
                self.__multiline_attach = False
                
                self.list_exp_panels.append(ss_Ac_item)
                
                #ss_Ac_item.close_panel(ss_Ac_item, True)

            match = re_comment.search(line)
            if match:
                """
                # Comment -> Label
                """
                self.config_lines.append(line)
                match2 = re_selectitems.search(line)
                match3 = re_units.search(line)
                if match2:
                    # Items for selection (# - ... # ...)
                    self.__selectitems.append(match2.group(1).strip())
                    self.__selectitemc.append(match2.group(2).strip())
                elif match3:
                    # Units comment (# Units: ... .)
                    self.__units = match3.group(1)
                else:
                    comment = match.group(1)
                    # make new label
                    lbl = WrappedLabel(text=comment, halign='left', valign='middle', color=self.theme['text3_color'], size_hint=(1, None))
                    
                    if self.__section != '':
                        if self.__ss < 0:
                            lbl.padding = (15,5)
                            self.__sv_llayout[self.__s].add_widget(lbl)
                        else:
                            self.__ss_sv_llayout[self.__ss].add_widget(lbl)
                    else:
                        # print to log
                        pass
                # Clear item type as no more multiline data is expected
                self.__multiline_attach = False

            match = re_table.search(line)
            if match: self.__item_type = 'multiline'
            match = re_yesno.search(line)
            if match: self.__item_type = 'yesno'
            match = re_selectone.search(line)
            if match: self.__item_type = 'selectone'
            match = re_selectany.search(line)
            if match: self.__item_type = 'selectany'
            match = re_multiline.search(line)
            if match and self.__multiline_attach:
                if line.strip() != '':
                    self.__gr.file_input.text += line.strip()# + "\n"

            match = re_keyval.search(line)
            if match:
                """
                key = value -> ...
                """
                key = match.group(1).strip()
                val = match.group(2).strip()
                self.config_lines.append(key)

                # make new grid
                self.__gr = MDGridLayout(cols=2, size_hint=(1, None), height=38, padding=5, spacing=5) #self.width/2
                if self.__ss < 0:
                    self.__gr.bind(width=self.__sv_llayout[self.__s].setter('width'))
                else:
                    self.__gr.bind(width=self.__ss_sv_llayout[self.__ss].setter('width'))
                self.__gr.label2 = Label(text=key, halign='right', valign='top', color=self.theme['text1_color'], size_hint=(0.4, 1), font_name='app/_ui/fonts/consola.ttf', padding_y=6)
                self.__gr.label2.bind(size=self.__gr.label2.setter('text_size'))
                self.__gr.add_widget(self.__gr.label2)
                if self.__item_type == 'multiline':
                    """
                    (multiline) -> TextField multiline
                    """
                    self.__gr.cols=1
                    self.__gr.label2.size_hint = (1, 0.2)
                    self.__gr.label2.halign='left'
                    self.__gr.file_input = TextInputMD(label=key, units='', multiline=True, text=val, size_hint=(1, 0.8))
                    if key in self.__config[self.__current_section]:
                        self.__gr.file_input.text = self.__config[self.__current_section][key]
                    self.__gr.height = 150
                    self.__gr.add_widget(self.__gr.file_input)
                    self.__multiline_attach = True
                    self.config_keys[key] = self.__gr.file_input

                elif self.__item_type == 'yesno':
                    """
                    (yes/no) -> Chips_xor
                    """
                    if key in self.__config[self.__current_section]:
                        val = self.__config[self.__current_section][key]
                    
                    self.__gr.file_input =  SelectChip_xor(text='yes', active=True if val=='yes' else False)
                    self.__gr.file_input2 = SelectChip_xor_no(text='no',  active=True if (val=='no' or val=='') else False)
                    
                    self.__gr.container = MDStackLayout(size_hint=(0.6, 1))
                    self.__gr.container.add_widget(self.__gr.file_input)
                    self.__gr.container.add_widget(self.__gr.file_input2)
                    self.__gr.add_widget(self.__gr.container)
                    self.config_keys[key] = self.__gr.file_input

                elif self.__item_type == 'selectone':
                    """
                    (select one) -> Chips_or
                    """
                    if key in self.__config[self.__current_section]:
                        val = self.__config[self.__current_section][key]
                        
                    self.__gr.select_container = MDStackLayout(size_hint=(0.6, 1)) #size_hint=(1, None))
                    for i in range(len(self.__selectitems)):
                        #print(i, self.__selectitems[i])
                        self.__gr.file_input = SelectChip_or(text=self.__selectitems[i], active=True if val==self.__selectitems[i] else False)
                        #print(i, self.__gr.file_input)
                        self.__gr.select_container.add_widget(self.__gr.file_input)
                        if val==self.__selectitems[i]:
                            self.config_keys[key] = self.__gr.file_input
                    
                    if not key in self.config_keys:
                        # If none is selected, just take a random chip. 
                        # In the evaluation, will loop through .parent.children anyway.
                        #print(key, self.__selectitems)
                        #print('XXX %s' % key)
                        #self.config_keys[key] = self.__gr.file_input
                        pass
                        
                    self.__gr.add_widget(self.__gr.select_container)
                    
                elif self.__item_type == 'selectanyx':
                    """
                    (select any) -> Chips
                    self.__gr.container = BoxLayout(orientation='vertical', size_hint=(0.6, 1))
                    self.__gr.container_row = BoxLayout(size_hint=(1, 1))
                    current_container_row = self.__gr.container_row
                    text_length = 0
                    for i in range(len(self.__selectitems)):
                        self.__gr.file_input =  ToggleButton(text=self.__selectitems[i], font_name='fonts/consola.ttf')
                        self.__gr.file_input.width = self.__gr.file_input.texture_size[0]
                        current_container_row.add_widget(self.__gr.file_input)
                        text_length += len(self.__selectitems[i])
                        if text_length > 30 and i < len(self.__selectitems):
                            self.__gr.container.add_widget(current_container_row)
                            self.__gr.container_row = BoxLayout(size_hint=(1, 1))
                            current_container_row = self.__gr.container_row
                            self.__gr.height += 30
                    
                    if not key in self.config_keys:
                        # If none is selected, just take a random chip. 
                        # In the evaluation, will loop through .parent.children anyway.
                        self.config_keys[key] = self.__gr.file_input

                    self.__gr.container.add_widget(current_container_row)
                    self.__gr.add_widget(self.__gr.container)
                    # store binding
                    self.config_keys[key] = self.__gr.file_input
                    """
                    pass

                else:
                    """
                    ... -> TextField
                    """
                    if key in self.__config[self.__current_section]:
                        val = self.__config[self.__current_section][key]
                        
                    self.__gr = TextInputMD(label=key, units=self.__units, multiline=False, text=val, size_hint=(1, None))
                    self.config_keys[key] = self.__gr #.file_input
                    self.__units = '' # reset units

                if self.__ss < 0:
                    self.__sv_llayout[self.__s].add_widget(self.__gr)
                    
                else:
                    self.__ss_sv_llayout[self.__ss].add_widget(self.__gr)
                    
                self.__selectitems = []
                self.__item_type = ''

    def change_toggle(self, togglebutton):
        pass
        #tb = togglebutton #.get_widgets('group')
        #if tb.state == 'down':
        #    print('Group %s, Down is: %s' % (togglebutton.group, tb.text))

    def save_run(self, event):
        
        if self.is_loading:
            return()
            
        # Save
        self.update_progressbar_on_print = True 
        for button in self.Tab_export_box.children:
            if isinstance(button,  ExecuteFileButton):
                button.md_bg_color = self.theme['active3_color'] #(0.5, 0.49, 0.43, 0.3)
        
        old_config_filename = self.config_filename
        new_config_filename = self.Ti_config.text #'config-saved.cfg'
        if old_config_filename == new_config_filename:
            # make a backup
            copyfile(old_config_filename, '%s.backup-%i' % (old_config_filename, time.time()))
        print('Let me save the config first...', end='')
        with open(new_config_filename,'w', encoding = 'utf-8') as file:
            for i in range(len(self.config_lines)):
                k = self.config_lines[i]
                if k in self.config_keys:
                    text = ''
                    obj = self.config_keys[k]
                    
                    no_linebreak = False
                    
                    if isinstance(obj, TextInputMD):
                        text = obj.text
                        if obj.multiline:
                            text = "\n  " + text.replace("\n", "\n  ").strip()
                            no_linebreak = True
                            #if "\n" in text:
                            #text = "\n"+text
                        #text = text.replace("\n", "\n  ")
                        if k == 'coords':
                            try:
                                latlonalt = [float(y) for y in text.strip().split(',')]
                                self.create_map_station(latlonalt[0], latlonalt[1])
                            except:
                                pass

                    #elif isinstance(obj, ToggleButton):
                        # Depreciated
                    #    for tb in obj.get_widgets(k):
                    #        if tb.state=='down':
                    #            text = tb.text
                    #            break
                    elif isinstance(obj, SelectChip_or) or isinstance(obj, SelectChip_xor): # 
                        for chip in obj.parent.children:
                            if chip.active:
                                text = chip.text
                                break
                    else:
                        print('Unknown', k, type(obj))
                        continue
                    
                    if no_linebreak: 
                        file.write('%s = %s' % (k, text))
                    else:
                        file.write('%s = %s\n' % (k, text))
                else:
                    file.write(k)
        print(' OK.') #'' %s.' % new_config_filename)
        # Run
        #self.progress = 0
        #self.Pb.value = 0
        #self.Pblbl.text = 'Ready'
        #App.get_running_app().make_plot(2)
        self.update_progressbar(0)
        #for view_button in ['view_CSV', 'view_PDF', 'view_KML_neutrons', 'view_KML_sm', 'view_KML_any']:
        #    window.Element(view_button).Update(disabled=True)
        #print(self.Ti_config.text)
        # create the thread to invoke other_func with arguments (2, 5)
        Thr = Thread(target=RunCorny, args=(self.Ti_config.text,))
        # set daemon to true so the thread dies when app is closed
        Thr.setDaemon(True) #Thr.daemon = True
        # start the thread
        
        Thr.start()
        
        #plt.switch_backend('agg')#Thr.quit()

        #execute(output_basename +'.csv')

        #self.Pb.value = 100
        #self.Pblbl.text = 'Done.'
        #for view in ['CSV', 'PDF', 'KML_neutrons', 'KML_sm', 'KML_other']:
        #    if values['make_'+view]: window.Element('view_'+view).Update(disabled=False)

        #self.box.clear_widgets()
        #plt.plot([2, 13, 3, 5])
        #self.box.add_widget(FigureCanvasKivyAgg(plt.gcf()))
        #self.Pb.value += 10
        #self.Pblbl.text = str(self.Pb.value)+'%'
        self.update_progressbar_on_print = False
        
        

    # does not work
    
    loadfile = ObjectProperty(None)
    savefile = ObjectProperty(None)
    text_input = ObjectProperty(None)

    def dismiss_popup(self):
        self._popup.dismiss()

    def show_load(self, event):
        app = App.get_running_app()
        #app.add_log('blubb')

        content = LoadDialog(load=self.load, cancel=self.dismiss_popup)
        self._popup = Popup(title="Load a cornish *.cfg file", content=content, size_hint=(0.5, 0.8))
        self._popup.open()

    def show_save(self):
        content = SaveDialog(save=self.save, cancel=self.dismiss_popup)
        self._popup = Popup(title="Save file", content=content,
                            size_hint=(0.9, 0.9))
        self._popup.open()

    def load(self, path, filename):
        self.Ti_config.text = os.path.join(path, filename[0])
        self.dismiss_popup()
        self.thread_load_config()
        #with open(os.path.join(path, filename[0])) as stream:
        #    self.text_input.text = stream.read()

    def save(self, path, filename):
        with open(os.path.join(path, filename), 'w') as stream:
            stream.write(self.text_input.text)

        self.dismiss_popup()

# from https://gist.github.com/aron-bordin/8ed0d0e977e6af7a788b
def threaded(fn):
    def wrapper(*args, **kwargs):
        Thread(target=fn, args=args, kwargs=kwargs).start()
    return wrapper



Factory.register('Root', cls=Root)
Factory.register('LoadDialog', cls=LoadDialog)
Factory.register('SaveDialog', cls=SaveDialog)
#isShownMenu = BooleanProperty(True)
CornyApp().run()
