import os
import subprocess
from glob import glob
from datetime import date


subprocess.call(["pyinstaller", "-wF", "../instantPASDy-GUI.py"])
# -w creates a windowed GUI
# -F is onefile

today = date.today().strftime("%Y%m%d")

for f in glob("dist/*"):
    b = os.path.basename(f)
    os.rename(f, b.replace('.', '-%s.' % today))
    
os.rmdir("dist")

print("Done.")
input()